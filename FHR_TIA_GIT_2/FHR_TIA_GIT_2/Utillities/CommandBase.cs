﻿// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT\Utillities\CommandBase.cs </file>
///
/// <copyright file="CommandBase.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the command base class. </summary>

using System;
using System.Windows.Input;

namespace FHR_TIA_GIT_2.Utillities
{
    /// <summary>   A command base. </summary>
    ///
    /// <seealso cref="System.Windows.Input.ICommand"/>

    public class CommandBase : ICommand
    {
        /// <summary>   Implementation of the interface members. </summary>
        public event EventHandler CanExecuteChanged;
        /// <summary>   (Immutable) the execute. </summary>
        private readonly Action<object> _execute;
        /// <summary>   (Immutable) the can execute. </summary>
        private readonly Predicate<object> _canExecute;

        /// <summary>   Initializes a new instance of the <see cref="CommandBase"/> class. </summary>
        ///
        /// <param name="executed">     The executed. </param>
        /// <param name="canExecute">   (Optional) The can execute. </param>

        public CommandBase(Action<object> executed, Predicate<object> canExecute = null)
        {
            _canExecute = canExecute;
            _execute = executed;
        }

        /// <summary>
        ///     Definiert die Methode, die bestimmt, ob der Befehl im aktuellen Zustand ausgeführt werden
        ///     kann.
        /// </summary>
        ///
        /// <param name="parameter">    Vom Befehl verwendete Daten.  Wenn der Befehl keine Datenübergabe
        ///                             erfordert, kann das Objekt auf <see langword="null" /> festgelegt
        ///                             werden. </param>
        ///
        /// <returns>
        ///     <see langword="true" />, wenn der Befehl ausgeführt werden kann, andernfalls
        ///     <see langword="false" />.
        /// </returns>

        public bool CanExecute(object parameter)
        {
            return _canExecute == null || _canExecute(parameter);
        }

        /// <summary>
        ///     Definiert die Methode, die aufgerufen wird, wenn der Befehl aufgerufen wird.
        /// </summary>
        ///
        /// <param name="parameter">    Vom Befehl verwendete Daten.  Wenn der Befehl keine Datenübergabe
        ///                             erfordert, kann das Objekt auf <see langword="null" /> festgelegt
        ///                             werden. </param>

        public void Execute(object parameter)
        {
            _execute?.Invoke(parameter);
            UpdateCanExecuteState();
        }
        
        /// <summary>   Updates the can execute state. </summary>
        public void UpdateCanExecuteState()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
            CommandManager.InvalidateRequerySuggested();
        }
    }
}