﻿
#pragma warning disable 1587
/// <file>  FHR_TIA_GIT\Utillities\Logger\Log4Net.cs </file>
///
/// <copyright file="Log4Net.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the log 4 net class. </summary>
#pragma warning restore 1587

/// <file>  FHR_TIA_GIT_2\Utillities\Logger\Log4Net.cs </file>
///
/// <copyright file="Log4Net.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the log 4 net class. </summary>

using System;

namespace FHR_TIA_GIT_2.Utillities.Logger
{
    /// <summary>   A log 4 net. </summary>
    public static class Log4Net
    {
        /// <summary>   The log. </summary>
        public static log4net.ILog Log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        /// <summary>   Details of the exception. </summary>
        private static Exception _initException = null;

        /// <summary>   Gets the initialize exception. </summary>
        ///
        /// <value> The initialize exception. </value>

        public static Exception InitException
        {
            get { return _initException; }
        }
        
        /// <summary>   The logger configuration file. </summary>
        private static string _loggerConfigFile = AppDomain.CurrentDomain.BaseDirectory + @"Utillities\Logger\LoggerConfig.xml";

        /// <summary>   Gets the logger configuration file. </summary>
        ///
        /// <value> The logger configuration file. </value>

        public static string LoggerConfigFile
        {
            get { return _loggerConfigFile; }
        }

        /// <summary>   Initializes the logger. </summary>
        ///
        /// <returns>   True if it succeeds, false if it fails. </returns>

        public static bool InitLogger()
        {
            try
            {
                log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo(LoggerConfigFile));
            }
            catch (Exception ex)
            {
                _initException = ex;
                return false;
            }
            return true;
        }
    }
}