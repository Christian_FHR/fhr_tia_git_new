﻿
// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT_2\Utillities\Constants.cs </file>
///
/// <copyright file="Constants.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the constants class. </summary>

namespace FHR_TIA_GIT_2.Utillities
{
    /// <summary>   A constants. </summary>
    public static class Constants
    {
        /// <summary>   (Immutable) the attribute not supported. </summary>
        public const string AttributeNotSupported = "Not Supported";
        /// <summary>   (Immutable) the file extension data block. </summary>
        public const string FileExtensionDataBlock = ".db";
        /// <summary>   (Immutable) the file extension scl. </summary>
        public const string FileExtensionScl = ".scl";
        /// <summary>   (Immutable) the file extension awl. </summary>
        public const string FileExtensionAwl = ".awl";
        /// <summary>   (Immutable) the file extension udt. </summary>
        public const string FileExtensionUdt = ".udt";

        /// <summary>   (Immutable) the file extension XML. </summary>
        public const string FileExtensionXml = ".xml";
        /// <summary>   (Immutable) the file extension attributes. </summary>
        public const string FileExtensionAttributes = "_att.xml";
        /// <summary>   (Immutable) the file extension certificate. </summary>
        public const string FileExtensionCertificate = ".cer";
        /// <summary>   (Immutable) full pathname of the export target file. </summary>
        public const string ExportTargetPath = @"C:\FHR";
    }
}