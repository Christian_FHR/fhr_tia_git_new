﻿// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT\Utillities\RelayCommand.cs </file>
///
/// <copyright file="RelayCommand.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the relay command class. </summary>

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;

namespace FHR_TIA_GIT_2.Utillities
{
/// <summary>   A relay command. </summary>
///
/// <typeparam name="T">    . </typeparam>
///
/// <seealso cref="System.Windows.Input.ICommand"/>

public class RelayCommand<T> : ICommand where T : EventArgs
    {
        /// <summary>   (Immutable) The execute. </summary>
        private readonly Action<T> _execute;
        /// <summary>   (Immutable) The can execute. </summary>
        private readonly Func<bool> _canExecute;

        /// <summary>   Initializes a new instance of the <see cref="RelayCommand{T}"/> class. </summary>
        ///
        /// <param name="execute">  The execute. </param>

        public RelayCommand(Action<T> execute) : this(execute, null) { }

        /// <summary>   Initializes a new instance of the <see cref="RelayCommand{T}"/> class. </summary>
        ///
        /// <exception cref="ArgumentNullException">    <paramref name="execute" /> is <c>null</c>. </exception>
        ///
        /// <param name="execute">      The execute. </param>
        /// <param name="canExecute">   The can execute. </param>

        public RelayCommand(Action<T> execute, Func<bool> canExecute)
        {
            _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            _canExecute = canExecute;
        }

        /// <summary>
        ///     Occurs when changes occur that affect whether or not the command should execute.
        /// </summary>

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        /// <summary>
        ///     Defines the method that determines whether the command can execute in its current state.
        /// </summary>
        ///
        /// <param name="parameter">    Data used by the command.  If the command does not require data
        ///                             to be passed, this object can be set to null. </param>
        ///
        /// <returns>   true if this command can be executed; otherwise, false. </returns>

        [DebuggerStepThrough]
        public bool CanExecute(object parameter)
        {
            return _canExecute?.Invoke() ?? true;
        }

        /// <summary>   Defines the method to be called when the command is invoked. </summary>
        ///
        /// <param name="parameter">    Data used by the command.  If the command does not require data
        ///                             to be passed, this object can be set to null. </param>

        public void Execute(object parameter)
        {
            _execute(parameter as T);
        }
    }
}