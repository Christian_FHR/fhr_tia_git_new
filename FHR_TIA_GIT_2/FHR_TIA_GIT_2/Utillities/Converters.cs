﻿// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT\Utillities\Converters.cs </file>
///
/// <copyright file="Converters.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the converters class. </summary>

using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace FHR_TIA_GIT_2.Utillities
{
    /// <summary>   An inverse boolean converter. </summary>
    [ValueConversion(typeof(bool), typeof(bool))]
    public class InverseBooleanConverter : IValueConverter
    {
        /// <summary>   Konvertiert einen Wert. </summary>
        ///
        /// <param name="value">        Der von der Bindungsquelle erzeugte Wert. </param>
        /// <param name="targetType">   Der Typ der Bindungsziel-Eigenschaft. </param>
        /// <param name="parameter">    Der zu verwendende Konverterparameter. </param>
        /// <param name="culture">      Die im Konverter zu verwendende Kultur. </param>
        ///
        /// <returns>
        ///     Ein konvertierter Wert.
        ///      Wenn die Methode <see langword="null" /> zurückgibt, wird der gültige NULL-Wert
        ///      verwendet.
        /// </returns>

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
                return !(bool)value;
            return value;
        }

        /// <summary>   Konvertiert einen Wert. </summary>
        ///
        /// <param name="value">        Der Wert, der vom Bindungsziel erzeugt wird. </param>
        /// <param name="targetType">   Der Typ, in den konvertiert werden soll. </param>
        /// <param name="parameter">    Der zu verwendende Konverterparameter. </param>
        /// <param name="culture">      Die im Konverter zu verwendende Kultur. </param>
        ///
        /// <returns>
        ///     Ein konvertierter Wert.
        ///      Wenn die Methode <see langword="null" /> zurückgibt, wird der gültige NULL-Wert
        ///      verwendet.
        /// </returns>

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
                return !(bool)value;
            return value;
        }
    }
    
    /// <summary>   A tree view line converter. </summary>
    [ValueConversion(typeof(TreeViewItem), typeof(bool))]
    public class TreeViewLineConverter : IValueConverter
    {
        /// <summary>   Konvertiert einen Wert. </summary>
        ///
        /// <param name="value">        Der von der Bindungsquelle erzeugte Wert. </param>
        /// <param name="targetType">   Der Typ der Bindungsziel-Eigenschaft. </param>
        /// <param name="parameter">    Der zu verwendende Konverterparameter. </param>
        /// <param name="culture">      Die im Konverter zu verwendende Kultur. </param>
        ///
        /// <returns>
        ///     Ein konvertierter Wert.
        ///      Wenn die Methode <see langword="null" /> zurückgibt, wird der gültige NULL-Wert
        ///      verwendet.
        /// </returns>

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            TreeViewItem item = (TreeViewItem)value;
            ItemsControl ic = ItemsControl.ItemsControlFromItemContainer(item);
            return ic.ItemContainerGenerator.IndexFromContainer(item) == ic.Items.Count - 1;
        }

        /// <summary>   Konvertiert einen Wert. </summary>
        ///
        /// <param name="value">        Der Wert, der vom Bindungsziel erzeugt wird. </param>
        /// <param name="targetType">   Der Typ, in den konvertiert werden soll. </param>
        /// <param name="parameter">    Der zu verwendende Konverterparameter. </param>
        /// <param name="culture">      Die im Konverter zu verwendende Kultur. </param>
        ///
        /// <returns>
        ///     Ein konvertierter Wert.
        ///      Wenn die Methode <see langword="null" /> zurückgibt, wird der gültige NULL-Wert
        ///      verwendet.
        /// </returns>

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return false;
        }
    }
    
    /// <summary>   to string converter. </summary>
    [ValueConversion(typeof(object), typeof(string))]
    public class ToStringConverter : IValueConverter
    {
        /// <summary>   Konvertiert einen Wert. </summary>
        ///
        /// <param name="value">        Der von der Bindungsquelle erzeugte Wert. </param>
        /// <param name="targetType">   Der Typ der Bindungsziel-Eigenschaft. </param>
        /// <param name="parameter">    Der zu verwendende Konverterparameter. </param>
        /// <param name="culture">      Die im Konverter zu verwendende Kultur. </param>
        ///
        /// <returns>
        ///     Ein konvertierter Wert.
        ///      Wenn die Methode <see langword="null" /> zurückgibt, wird der gültige NULL-Wert
        ///      verwendet.
        /// </returns>

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
                return value.ToString();
            else return "";
        }

        /// <summary>   Konvertiert einen Wert. </summary>
        ///
        /// <param name="value">        Der Wert, der vom Bindungsziel erzeugt wird. </param>
        /// <param name="targetType">   Der Typ, in den konvertiert werden soll. </param>
        /// <param name="parameter">    Der zu verwendende Konverterparameter. </param>
        /// <param name="culture">      Die im Konverter zu verwendende Kultur. </param>
        ///
        /// <returns>
        ///     Ein konvertierter Wert.
        ///      Wenn die Methode <see langword="null" /> zurückgibt, wird der gültige NULL-Wert
        ///      verwendet.
        /// </returns>

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
    
    /// <summary>   An object to type string converter. </summary>
    [ValueConversion(typeof(object), typeof(string))]
    public class ObjectToTypeStringConverter : IValueConverter
    {
        /// <summary>   Konvertiert einen Wert. </summary>
        ///
        /// <param name="value">        Der von der Bindungsquelle erzeugte Wert. </param>
        /// <param name="targetType">   Der Typ der Bindungsziel-Eigenschaft. </param>
        /// <param name="parameter">    Der zu verwendende Konverterparameter. </param>
        /// <param name="culture">      Die im Konverter zu verwendende Kultur. </param>
        ///
        /// <returns>
        ///     Ein konvertierter Wert.
        ///      Wenn die Methode <see langword="null" /> zurückgibt, wird der gültige NULL-Wert
        ///      verwendet.
        /// </returns>

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value?.GetType();
        }

        /// <summary>   Konvertiert einen Wert. </summary>
        ///
        /// <param name="value">        Der Wert, der vom Bindungsziel erzeugt wird. </param>
        /// <param name="targetType">   Der Typ, in den konvertiert werden soll. </param>
        /// <param name="parameter">    Der zu verwendende Konverterparameter. </param>
        /// <param name="culture">      Die im Konverter zu verwendende Kultur. </param>
        ///
        /// <returns>
        ///     Ein konvertierter Wert.
        ///      Wenn die Methode <see langword="null" /> zurückgibt, wird der gültige NULL-Wert
        ///      verwendet.
        /// </returns>

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(typeof(bool?), typeof(Visibility))]
    public class NullableBoolToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var bValue = (bool?) value;
            return bValue == null ? Visibility.Hidden : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}