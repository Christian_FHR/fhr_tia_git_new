﻿// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT\Utillities\TreeViewHelper.cs </file>
///
/// <copyright file="TreeViewHelper.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the tree view helper class. </summary>

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using FHR_TIA_GIT_2.ViewsAndModels.Models;

namespace FHR_TIA_GIT_2.Utillities
{
    /// <summary>   A tree view helper. </summary>
    public class TreeViewHelper : ViewModelBase
    {
        #region SelectedItem

        /// <summary>   (Immutable) the selected item property. </summary>
        public static readonly DependencyProperty SelectedItemProperty = DependencyProperty.RegisterAttached(
            "SelectedItem", 
            typeof(object), 
            typeof(TreeViewHelper),
            new UIPropertyMetadata(null, OnSelectedItemChanged));

        /// <summary>   Gets selected item. </summary>
        ///
        /// <param name="obj">  The object. </param>
        ///
        /// <returns>   The selected item. </returns>

        public static object GetSelectedItem(DependencyObject obj)
        {
            return obj.GetValue(SelectedItemProperty);
        }

        /// <summary>   Sets selected item. </summary>
        ///
        /// <param name="obj">      The object. </param>
        /// <param name="value">    The value. </param>

        public static void SetSelectedItem(DependencyObject obj, object value)
        {
            obj.SetValue(SelectedItemProperty, value);
        }

        /// <summary>   Raises the dependency property changed event. </summary>
        ///
        /// <param name="d">    A DependencyObject to process. </param>
        /// <param name="e">    Event information to send to registered event handlers. </param>

        private static void OnSelectedItemChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TreeView treeView = d as TreeView;
            
            if (treeView == null || e.NewValue == null) return;
            treeView.SelectedItemChanged += (sender, e2) => SetSelectedItem(treeView, e2.NewValue);

            ICommand cmd = (ICommand) treeView.GetValue(SelectedItemChangedProperty);
            if (cmd != null)
            {
                if (cmd.CanExecute(null))
                    cmd.Execute(new DependencyPropertyEventArgs(e));
            }
        }

        #endregion

        #region Selected Item changed

        /// <summary>   (Immutable) the selected item changed property. </summary>
        public static readonly DependencyProperty SelectedItemChangedProperty = DependencyProperty.RegisterAttached(
            "SelectedItemChanged",
            typeof(ICommand),
            typeof(TreeViewHelper));

        /// <summary>   Gets selected item changed. </summary>
        ///
        /// <param name="obj">  The object. </param>
        ///
        /// <returns>   The selected item changed. </returns>

        public static ICommand GetSelectedItemChanged(DependencyObject obj)
        {
            return (ICommand) obj.GetValue(SelectedItemChangedProperty);
        }

        /// <summary>   Sets selected item changed. </summary>
        ///
        /// <param name="obj">      The object. </param>
        /// <param name="value">    The value. </param>

        public static void SetSelectedItemChanged(DependencyObject obj, ICommand value)
        {
            obj.SetValue(SelectedItemChangedProperty, value);
        }

        #endregion

        #region Expanding Behavior

        /// <summary>   (Immutable) the expanding behavior property. </summary>
        public static readonly DependencyProperty ExpandingBehaviorProperty = DependencyProperty.RegisterAttached(
            "ExpandingBehavior",
            typeof(ICommand),
            typeof(TreeViewHelper),
            new UIPropertyMetadata(OnExpandingBehaviorChanged));

        /// <summary>   Gets expanding behavior. </summary>
        ///
        /// <param name="obj">  The object. </param>
        ///
        /// <returns>   The expanding behavior. </returns>

        public static ICommand GetExpandingBehavior(DependencyObject obj)
        {
            return (ICommand) obj.GetValue(ExpandingBehaviorProperty);
        }

        /// <summary>   Sets expanding behavior. </summary>
        ///
        /// <param name="obj">      The object. </param>
        /// <param name="value">    The value. </param>

        public static void SetExpandingBehavior(DependencyObject obj, ICommand value)
        {
            obj.SetValue(ExpandingBehaviorProperty, value);
        }

        /// <summary>   Raises the dependency property changed event. </summary>
        ///
        /// <param name="d">    A DependencyObject to process. </param>
        /// <param name="e">    Event information to send to registered event handlers. </param>

        private static void OnExpandingBehaviorChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TreeView treeView = d as TreeView;

            if (treeView == null || e.NewValue == null)
                return;

            treeView.SelectedItemChanged += (_, e2) => SetSelectedItem(treeView, e2.NewValue);

            ICommand command = (ICommand)treeView.GetValue(SelectedItemChangedProperty);
            if (command == null) return;
            
            if (command.CanExecute(null)) command.Execute(new DependencyPropertyEventArgs(e));
        }

        #endregion

        #region Constructor

        /// <summary>   Default constructor. </summary>
        public TreeViewHelper(){}

        #endregion
        
        #region Event Args
    
        /// <summary>   Additional information for dependency property events. </summary>
        public class DependencyPropertyEventArgs : EventArgs
        {
            /// <summary>   Gets or sets the dependency property changed event arguments. </summary>
            ///
            /// <value> The dependency property changed event arguments. </value>

            public DependencyPropertyChangedEventArgs DependencyPropertyChangedEventArgs { get; private set; }

            /// <summary>
            ///     Initializes a new instance of the <see cref="DependencyPropertyEventArgs"/> class.
            /// </summary>
            ///
            /// <param name="dependencyPropertyChangedEventArgs">   The
            ///                                                     <see cref="DependencyPropertyChangedEventArgs"/>
            ///                                                     instance containing the event data. </param>

            public DependencyPropertyEventArgs(DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
            {
                DependencyPropertyChangedEventArgs = dependencyPropertyChangedEventArgs;
            }
        }

        #endregion
    }
}