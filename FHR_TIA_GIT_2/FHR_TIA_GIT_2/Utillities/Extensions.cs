﻿// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT\Utillities\Extensions.cs </file>
///
/// <copyright file="Extensions.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the extensions class. </summary>

using System;
using System.Xml.Serialization;

namespace FHR_TIA_GIT_2.Utillities
{
    /// <summary>   An extensions. </summary>
    public static class Extensions
    {
        /// <summary>   A string[] extension method that array to custom string. </summary>
        ///
        /// <param name="str">  The str to act on. </param>
        ///
        /// <returns>   A string. </returns>

        public static string ArrayToCustomString(this string[] str)
        {
            return String.Join(";", str);
        }
    }
}