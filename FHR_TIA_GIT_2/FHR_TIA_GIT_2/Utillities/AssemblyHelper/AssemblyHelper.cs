﻿// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT\Utillities\AssemblyHelper\AssemblyHelper.cs </file>
///
/// <copyright file="AssemblyHelper.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the assembly helper class. </summary>

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.Win32;
using static FHR_TIA_GIT_2.Utillities.Logger.Log4Net;

namespace FHR_TIA_GIT_2.Utillities.AssemblyHelper
{
    /// <summary>   An assembly helper. </summary>
    public static class AssemblyHelper
    {
        private const string BasePath = @"SOFTWARE\WOW6432Node\Siemens\Automation\Openness\";
        private const string WhiteListPath = @"SOFTWARE\Siemens\Automation\Openness\";
        /// <summary>   Full pathname of the engineering file. </summary>
        private static string _engineeringPath = "";
        /// <summary>   Full pathname of the engineering hmi file. </summary>
        private static string _engineeringHmiPath = "";

        /// <summary>   Raises the resolve event. </summary>
        ///
        /// <param name="sender">   Source of the event. </param>
        /// <param name="args">     Event information to send to registered event handlers. </param>
        ///
        /// <returns>   An Assembly. </returns>

        public static Assembly OnAssemblyResolve(object sender, ResolveEventArgs args)
        {
            var assemblyName = new AssemblyName(args.Name);
            string path = "";
            
            if (assemblyName.Name.EndsWith("Siemens.Engineering"))
                path = _engineeringPath;
            if (assemblyName.Name.EndsWith("Siemens.Engineering.Hmi"))
                path = _engineeringHmiPath;

            if (string.IsNullOrEmpty(path) || !File.Exists(path)) return null;
            
            Log.Debug("Assembly loaded form path: " + path);
            return Assembly.LoadFrom(path);

        }

        /// <summary>   Gets the versions. </summary>
        ///
        /// <returns>   The versions. </returns>

        public static List<string> GetVersions()
        {
            // Try to open the base path in registry
            RegistryKey key = GetRegistryKey(BasePath);

            // Key found in registry => TIA openness is installed
            if (key != null)
            {
                // Read all subkeys
                var names = key.GetSubKeyNames().OrderBy(x => x).ToList();
                key.Dispose();

                return names;
            }

            return new List<string>();
        }

        /// <summary>   Gets registry key. </summary>
        ///
        /// <param name="keyName">  Name of the key. </param>
        ///
        /// <returns>   The registry key. </returns>

        private static RegistryKey GetRegistryKey(string keyName)
        {
            // Try to open key with a 64Bit view
            RegistryKey baseKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
            RegistryKey key = baseKey.OpenSubKey(keyName, true);
            
            // Check if key is found in 64Bit view
            if (key == null)
            {
                baseKey.Dispose();
                // Try to open key with a normal view
                baseKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default);
                key = baseKey.OpenSubKey(keyName, true);
            }
            
            // Check if key is found in normal view
            if (key == null)
            {
                baseKey.Dispose();
                // Try to open key with a 32Bit view
                baseKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32);
                key = baseKey.OpenSubKey(keyName, true);
            }
            
            baseKey.Dispose();

            return key;
        }

        /// <summary>   Gets the assmblies. </summary>
        ///
        /// <param name="version">  The version. </param>
        ///
        /// <returns>   The assmblies. </returns>

        public static List<string> GetAssmblies(string version)
        {
            RegistryKey key = GetRegistryKey(BasePath + version);

            if (key == null) return new List<string>();
            try
            {
                var subKey = key.OpenSubKey("PublicAPI");

                if (subKey != null)
                {
                    var result = subKey.GetSubKeyNames().OrderBy(x => x).ToList();
                    subKey.Dispose();
                    return result;
                }
            }
            finally
            {
                key.Dispose();
            }

            return new List<string>();
        }

        /// <summary>   Gets assembly path. </summary>
        ///
        /// <param name="version">  The version. </param>
        /// <param name="assembly"> The assembly. </param>
        ///
        /// <returns>   The assembly path. </returns>

        public static string GetAssemblyPath(string version, string assembly)
        {
            RegistryKey key = GetRegistryKey(BasePath + version + "\\PublicAPI\\" + assembly);

            if(key != null)
            {
                try
                {
                    _engineeringPath = key.GetValue("Siemens.Engineering").ToString();
                    _engineeringHmiPath = key.GetValue("Siemens.Engineering.Hmi").ToString();
                    
                    return _engineeringPath;
                }
                finally
                {
                    key.Dispose();
                }
            }

            return null;
        }

        /// <summary>   Sets white list entry. </summary>
        ///
        /// <param name="version">  The version. </param>

        public static void SetWhiteListEntry(string version)
        {
            string appName = AppDomain.CurrentDomain.FriendlyName;
            string appPath = Assembly.GetExecutingAssembly().Location;
            string lastModified = File.GetLastWriteTimeUtc(appPath).ToString(@"yyyy/MM/dd HH:mm:ss.fff", CultureInfo.InvariantCulture);

            FileStream stream = File.OpenRead(appPath);
            string fileHash = Convert.ToBase64String(System.Security.Cryptography.SHA256.Create().ComputeHash(stream));
            stream.Close();

            // string keyPath = WhiteListPath + version + "\\Whitelist\\" + appName + "\\Entry";
            RegistryKey key = GetRegistryKey(WhiteListPath + version);

            if (key == null) return;

            string keyPath;
            key = GetRegistryKey(WhiteListPath + version + "\\Whitelist\\");

            // Create Key Whitelist
            if (key == null)
            {
                key = GetRegistryKey(WhiteListPath + version);
                key = key?.CreateSubKey("Whitelist");
                key?.Close();
            }
            
            key = GetRegistryKey(WhiteListPath + version + "\\Whitelist\\" + appName);
            // Create Key with AppName
            if (key == null)
            {
                // App not in Whitelist => Create complete new
                keyPath = WhiteListPath + version + "\\Whitelist\\";
                key = GetRegistryKey(keyPath); // Get Key from Whitelist
                key = key?.CreateSubKey(appName); // Create Key with AppName
                key?.Close();
            }
            
            // Create Key Entry
            key = GetRegistryKey(WhiteListPath + version + "\\Whitelist\\" + appName + "\\Entry");
            if (key == null)
            {
                // App not in Whitelist => Create complete new
                keyPath = WhiteListPath + version + "\\Whitelist\\" + appName;
                key = GetRegistryKey(keyPath); // Get Key from Whitelist
                key = key?.CreateSubKey("Entry"); // Create Key with Entry
                key?.Close();
            }
            
            key = GetRegistryKey(WhiteListPath + version + "\\Whitelist\\" + appName + "\\Entry");
            if (key == null) return;
            
            key.SetValue("Path", appPath, RegistryValueKind.String);
            key.SetValue("DateModified", lastModified, RegistryValueKind.String);
            key.SetValue("FileHash", fileHash, RegistryValueKind.String);
            key.Close();
        }
    }
}