﻿
#pragma warning disable 1587
/// <file>  FHR_TIA_GIT_2\App.xaml.cs </file>
///
/// <copyright file="App.xaml.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the app.xaml class. </summary>
#pragma warning restore 1587

/// <file>  FHR_TIA_GIT_2\App.xaml.cs </file>
///
/// <copyright file="App.xaml.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the app.xaml class. </summary>

using System;
using System.Diagnostics;
using System.Security.Principal;
using System.Threading;
using System.Windows;
using FHR_TIA_GIT_2.ViewsAndModels.Models;
using FHR_TIA_GIT_2.ViewsAndModels.Views;
using System.DirectoryServices.AccountManagement;
using MsgboxModernStyle;
using static FHR_TIA_GIT_2.Utillities.Logger.Log4Net;
using static FHR_TIA_GIT_2.Utillities.AssemblyHelper.AssemblyHelper;
using Settings = FHR_TIA_GIT_2.Properties.Settings;

namespace FHR_TIA_GIT_2
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        /// <summary>   Default constructor. </summary>
        public App()
        {
            AppDomain.CurrentDomain.AssemblyResolve += OnAssemblyResolve;
        }

        /// <summary>   Löst das <see cref="E:System.Windows.Application.Startup" />-Ereignis aus. </summary>
        ///
        /// <param name="e">    Ein <see cref="T:System.Windows.StartupEventArgs" />, das die
        ///                     Ereignisdaten enthält. </param>

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            string messageText;
            string messageTitle;
            
            try
            {
                #region Init Logger

                if (!InitLogger())
                {
                    MessageBox.Show("Log4Net cannot start: " + 
                                    (InitException != null ? InitException.Message : ""),
                        "Error at startup", MessageBoxButton.OK, MessageBoxImage.Error);
                    
                    Shutdown();
                    return;
                }

                #endregion
                
                // // TODO Delete by release
                // Log.Debug("Wait for Debugger!");
                // while (!Debugger.IsAttached)
                // {
                //     Thread.Sleep(100);
                // }
                // Log.Debug("Debugger has attached!");
                
                #region Check Admin privileges
                using (var identity = WindowsIdentity.GetCurrent())
                {
                    var principal = new WindowsPrincipal(identity);
                    if (!principal.IsInRole(WindowsBuiltInRole.Administrator))
                    {
                        messageText = string.Format(
                            "Current User: {0} has no Admin rights. Please start application with Admin rights.", identity.Name);
                        messageTitle = "Start without Admin rights";
                        MsgBox.ShowDialog(messageText, messageTitle, MsgboxIcons.Error, MsgboxButtons.Ok);
                        Log.Error(messageText);
                        Shutdown();
                        return;
                    }
                    
                    var ctx = new PrincipalContext(ContextType.Machine);
                    var user = UserPrincipal.Current;
                    var group = GroupPrincipal.FindByIdentity(ctx, "Siemens TIA Openness");

                    if (group != null && !user.IsMemberOf(group))
                    {
                        messageText = string.Format(
                            "Current User: {0} is not a Member of Group {1}. Please add User in the group and restart our PC.", identity.Name);
                        messageTitle = "User not in Group";
                        MsgBox.ShowDialog(messageText, messageTitle, MsgboxIcons.Error, MsgboxButtons.Ok);
                        Log.Error(messageText);
                        Shutdown();
                        return;
                    }
                }

                #endregion

                #region Load Assembly or show Dialog

                if (Settings.Default.HideAssemblySelection)
                {
                    GetAssemblyPath(Settings.Default.EngineeringVersion, Settings.Default.AssemblyVersion);
                }
                else
                {
                    var viewModel = new AssemblySelectionModel();
                    var view = new AssemblySelection(viewModel);
                    viewModel.OnRequestClose += (sender, args) => view.Close();
                    view.ShowDialog();
                    
                    if (viewModel.DialogResult != true)
                    {
                        MessageBox.Show("No Assembly was selected.", "Shutdown");
                        Shutdown();
                        return;
                    }

                    Settings.Default.HideAssemblySelection = viewModel.Hide;
                    Settings.Default.EngineeringVersion = viewModel.SelectedVersion;
                    Settings.Default.AssemblyVersion = viewModel.SelectedAssembly;
                    Settings.Default.Save();
                }

                #endregion
                
                SetWhiteListEntry(Settings.Default.EngineeringVersion);
                
                var mainWindow = new MainWindow();
                mainWindow.Show();
            }
            catch (Exception ex)
            {
                messageText = "Please delete the 'TiaPortalOpennessDemo.exe.config' file and start the application again.";
                messageTitle = "Error";
                MsgBox.ShowDialog(messageText, messageTitle, MsgboxIcons.Error, MsgboxButtons.Ok);
                Log.FatalFormat("Exception: {0}", ex.Message);
                Shutdown();
                return;
            }
        }
    }
}