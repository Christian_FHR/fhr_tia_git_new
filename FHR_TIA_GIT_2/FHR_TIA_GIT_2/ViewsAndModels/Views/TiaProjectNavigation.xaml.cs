﻿
#pragma warning disable 1587
/// <file>  FHR_TIA_GIT\ViewsAndModels\Views\TiaProjectNavigation.xaml.cs </file>
///
/// <copyright file="TiaProjectNavigation.xaml.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the tia project navigation.xaml class. </summary>
#pragma warning restore 1587

/// <file>  FHR_TIA_GIT_2\ViewsAndModels\Views\TiaProjectNavigation.xaml.cs </file>
///
/// <copyright file="TiaProjectNavigation.xaml.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the tia project navigation.xaml class. </summary>

using System.Windows;
using System.Windows.Controls;

namespace FHR_TIA_GIT_2.ViewsAndModels.Views
{
    public partial class TiaProjectNavigation : UserControl
    {
        /// <summary>   Default constructor. </summary>
        public TiaProjectNavigation()
        {
            InitializeComponent();
        }
    }
}