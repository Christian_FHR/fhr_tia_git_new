﻿
#pragma warning disable 1587
/// <file>  FHR_TIA_GIT\ViewsAndModels\Views\Settings.xaml.cs </file>
///
/// <copyright file="Settings.xaml.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the settings.xaml class. </summary>
#pragma warning restore 1587

/// <file>  FHR_TIA_GIT_2\ViewsAndModels\Views\Settings.xaml.cs </file>
///
/// <copyright file="Settings.xaml.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the settings.xaml class. </summary>

using System.Windows.Controls;

namespace FHR_TIA_GIT_2.ViewsAndModels.Views
{
    public partial class Settings : UserControl
    {
        /// <summary>   Default constructor. </summary>
        public Settings()
        {
            InitializeComponent();
        }
    }
}