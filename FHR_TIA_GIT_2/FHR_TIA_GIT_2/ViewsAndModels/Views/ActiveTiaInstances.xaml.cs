﻿
#pragma warning disable 1587
/// <file>  FHR_TIA_GIT\ViewsAndModels\Views\ActiveTiaInstances.xaml.cs </file>
///
/// <copyright file="ActiveTiaInstances.xaml.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the active tia instances.xaml class. </summary>
#pragma warning restore 1587

/// <file>  FHR_TIA_GIT_2\ViewsAndModels\Views\ActiveTiaInstances.xaml.cs </file>
///
/// <copyright file="ActiveTiaInstances.xaml.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the active tia instances.xaml class. </summary>

using System.Windows;
using System.Windows.Controls;
using FHR_TIA_GIT_2.ViewsAndModels.Models;

namespace FHR_TIA_GIT_2.ViewsAndModels.Views
{
    public partial class ActiveTiaInstances : UserControl
    {
        /// <summary>   Default constructor. </summary>
        public ActiveTiaInstances()
        {
            InitializeComponent();
        }

        /// <summary>   Event handler. Called by TreeView for on selected item changed events. </summary>
        ///
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        A RoutedPropertyChangedEventArgs&lt;object&gt; to process. </param>

        private void TreeView_OnSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            ((ActiveTiaInstancesModel) DataContext).SelectedItem = e.NewValue as ActiveTiaInstancesItem;
        }
    }
}