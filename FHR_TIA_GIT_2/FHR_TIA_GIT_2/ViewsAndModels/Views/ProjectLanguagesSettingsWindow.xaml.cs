﻿// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT\ViewsAndModels\Views\ProjectLanguagesSettingsWindow.xaml.cs </file>
///
/// <copyright file="ProjectLanguagesSettingsWindow.xaml.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the project languages settings window.xaml class. </summary>


using System.Windows;
using FHR_TIA_GIT_2.TIA.TiaObjects;

namespace FHR_TIA_GIT_2.ViewsAndModels.Views
{
    public partial class ProjectLanguagesSettingsWindow
    {
        /// <summary>   Default constructor. </summary>
        public ProjectLanguagesSettingsWindow()
        {
            InitializeComponent();
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="projectLanguages"> The project languages. </param>

        public ProjectLanguagesSettingsWindow(TiaProjectLanguages projectLanguages) : this()
        {
            this.DataContext = projectLanguages;

        }
    }
}