﻿using System.Collections;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using Chrischi666.Lib.ListTreeView.TreeViewList;
using FHR_TIA_GIT_2.ViewsAndModels.Models;

namespace FHR_TIA_GIT_2.ViewsAndModels.Views.DetailsViews
{
    public partial class TagTableControl : UserControl
    {
        public TagTableControl()
        {
            InitializeComponent();
        }
    }

    public class TagTableInformationTreeViewRoot : ViewModelBase, ITreeModel
    {
        #region Constructors

        #endregion

        #region Types

        #endregion

        #region Fields

        #endregion

        #region Properties

        #endregion

        #region Methods

        #endregion

        #region Override metods

        public IEnumerable GetChildrens(object parent)
        {
            throw new System.NotImplementedException();
        }

        public bool HasChildren(object parent)
        {
            throw new System.NotImplementedException();
        }

        #endregion
    }
    
    public class TagTableInformationTreeViewChild : ViewModelBase
    {
        #region Constructors

        

        #endregion

        #region Types

        #endregion

        #region Fields

        private ObservableCollection<TagTableInformationTreeViewChild> _childs;
        private string _name;
        private string _datentype;
        private string _adress;

        #endregion

        #region Properties

        public ObservableCollection<TagTableInformationTreeViewChild> Childs
        {
            get { return _childs; }
            set { _childs = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Datentype
        {
            get { return _datentype; }
            set { _datentype = value; }
        }

        public string Adress
        {
            get { return _adress; }
            set { _adress = value; }
        }

        #endregion

        #region Methods

        #endregion

        #region Override metods

        #endregion
    }
}