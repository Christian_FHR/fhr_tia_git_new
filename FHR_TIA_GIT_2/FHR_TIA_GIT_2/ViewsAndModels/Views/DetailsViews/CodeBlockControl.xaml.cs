﻿// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT_2\ViewsAndModels\Views\DetailsViews\CodeBlockControl.xaml.cs </file>
///
/// <copyright file="CodeBlockControl.xaml.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the code block control.xaml class. </summary>

using System.Collections;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using Chrischi666.Lib.ListTreeView.TreeViewList;
using FHR_TIA_GIT_2.TIA;
using FHR_TIA_GIT_2.TIA.TiaObjects;
using FHR_TIA_GIT_2.TIA.XMLParser.Models.Block;
using FHR_TIA_GIT_2.TIA.XMLParser.Models.Type;
using FHR_TIA_GIT_2.TIA.XMLParser.Models.XmlMembers;
using FHR_TIA_GIT_2.ViewsAndModels.Models;
using JetBrains.Annotations;

namespace FHR_TIA_GIT_2.ViewsAndModels.Views.DetailsViews
{
    public partial class CodeBlockControl : UserControl
    {
        /// <summary>   Default constructor. </summary>
        public CodeBlockControl()
        {
            InitializeComponent();
        }
    }
    
    public class TypeCodeInformationTreeViewRoot : ViewModelBase, ITreeModel
    {
        #region Constructors

        public TypeCodeInformationTreeViewRoot()
        {
            Root = new TypeCodeInformationTreeViewChild();
        }

        public TypeCodeInformationTreeViewRoot(TiaObject obj) : this()
        {
            switch (obj)
            {
                case TiaBlock {SimaticMlInformation: { }} tiaBlock:
                    if (tiaBlock.SimaticMlInformation.XmlInformations is BlockInformation blockInformation)
                        ParseInformation(blockInformation);
                    break;
                case TiaType {SimaticMlInformation: { }} tiaType:
                    if (tiaType.SimaticMlInformation.XmlInformations is TypeInformation typeInformation)
                        ParseInformation(typeInformation);
                    break;
            }
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        private TypeCodeInformationTreeViewChild _root;
        
        #endregion

        #region Properties

        public TypeCodeInformationTreeViewChild Root
        {
            get { return _root; }
            set { _root = value; RaisePropertyChanged(); }
        }

        #endregion

        #region Methods

        private void ParseInformation(BlockInformation xmlInfo)
        {
            xmlInfo.BlockInterface.InterfaceSections.ForEach(section => Root.AddChild(section));
        }
        
        private void ParseInformation(TypeInformation xmlInfo)
        {
            xmlInfo.Members.ForEach(member => Root.AddChild(member));
        }
        
        #endregion

        #region Override metods

        public IEnumerable GetChildrens(object parent)
        {
            parent ??= Root;
            return ((TypeCodeInformationTreeViewChild) parent).Childs;
        }

        public bool HasChildren(object parent)
        {
            return ((TypeCodeInformationTreeViewChild) parent).Childs.Count > 0;
        }
        
        #endregion
    }

    public class TypeCodeInformationTreeViewChild : ViewModelBase
    {
        #region Constructors

        public TypeCodeInformationTreeViewChild()
        {
            Childs = new ObservableCollection<TypeCodeInformationTreeViewChild>();
        }
        
        public TypeCodeInformationTreeViewChild(string name, string datatype = "", string defaultValue = "", 
            string comment = "", bool? accessible = null, bool? writeable = null, bool? visible = null) : this()
        {
            Name = name;
            Datatype = datatype;
            DefaultValue = defaultValue;
            Comment = comment;
            Accessible = accessible;
            Writeable = writeable;
            Visible = visible;
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        private ObservableCollection<TypeCodeInformationTreeViewChild> _childs;
        private string _name;
        private string _datatype;
        private string _defaultValue;
        private string _comment;
        private bool? _accessible;
        private bool? _writeable;
        private bool? _visible;
        
        #endregion

        #region Properties

        public ObservableCollection<TypeCodeInformationTreeViewChild> Childs
        {
            get { return _childs; }
            set { _childs = value; RaisePropertyChanged(); }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; RaisePropertyChanged(); }
        }

        public string Datatype
        {
            get { return _datatype; }
            set { _datatype = value; RaisePropertyChanged(); }
        }

        public string DefaultValue
        {
            get { return _defaultValue; }
            set { _defaultValue = value; RaisePropertyChanged(); }
        }

        public string Comment
        {
            get { return _comment; }
            set { _comment = value; RaisePropertyChanged(); }
        }

        public bool? Accessible
        {
            get { return _accessible; }
            set { _accessible = value; RaisePropertyChanged(); }
        }

        public bool? Writeable
        {
            get { return _writeable; }
            set { _writeable = value; RaisePropertyChanged(); }
        }

        public bool? Visible
        {
            get { return _visible; }
            set { _visible = value; RaisePropertyChanged(); }
        }

        #endregion

        #region Methods

        public void AddChild(BlockInterfaceSection section)
        {
            var sectionChild = new TypeCodeInformationTreeViewChild(section.InterfaceSectionName);
            section.InterfaceMembers?.ForEach(member => sectionChild.AddChild(member));
            this.Childs?.Add(sectionChild);
        }

        public void AddChild([NotNull] XmlMember member)
        {
            switch (member)
            {
                case XmlStruct xmlStruct:
                    var structChild = new TypeCodeInformationTreeViewChild(xmlStruct.Name, xmlStruct.DataType);
                    xmlStruct.Members.ForEach(x => structChild.AddChild(x));
                    this.Childs.Add(structChild);
                    break;
                case XmlMultiInstance xmlMultiInstance:
                    var multiChild = new TypeCodeInformationTreeViewChild(xmlMultiInstance.Name, xmlMultiInstance.DataType);
                    xmlMultiInstance.InterfaceSections.ForEach(x =>
                    {
                        if (x.InterfaceSectionName == "None")
                            x.InterfaceMembers.ForEach(y => multiChild.AddChild(y));
                        else multiChild.AddChild(x);
                    });
                    this.Childs.Add(multiChild);
                    break;
                default:
                    var child = new TypeCodeInformationTreeViewChild(
                        member.Name, 
                        member.DataType, 
                        member.DefaultValue, 
                        member.GetMemberCommentWhitActualCulture(),
                        member.ExternalAccessible,
                        member.ExternalWritable,
                        member.ExternalVisible);
                    this.Childs.Add(child);
                    break;
            }
        }
        
        #endregion

        #region Override metods

        #endregion


    }
}