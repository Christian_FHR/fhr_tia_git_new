﻿// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT_2\ViewsAndModels\Views\DetailsViews\DetailWindow.xaml.cs </file>
///
/// <copyright file="DetailWindow.xaml.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the detail window.xaml class. </summary>

using System.Windows;
using FHR_TIA_GIT_2.TIA;

namespace FHR_TIA_GIT_2.ViewsAndModels.Views.DetailsViews
{
    public partial class DetailWindow : Window
    {        
        public TypeCodeInformationTreeViewRoot TypeCodeInformationTreeViewRoot { get; internal set; }
        
        /// <summary>   Default constructor. </summary>
        public DetailWindow()
        {
            InitializeComponent();
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="dataContext">  Context for the data. </param>

        public DetailWindow(object dataContext) : this()
        {
            this.DataContext = dataContext;
        }

        public DetailWindow(TiaObject obj) : this()
        {
            this.TypeCodeInformationTreeViewRoot = new TypeCodeInformationTreeViewRoot(obj);
            this.DataContext = this;
        }
    }


}