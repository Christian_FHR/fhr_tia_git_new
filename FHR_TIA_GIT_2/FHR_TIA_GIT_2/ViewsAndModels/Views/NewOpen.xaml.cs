﻿
#pragma warning disable 1587
/// <file>  FHR_TIA_GIT\ViewsAndModels\Views\NewOpen.xaml.cs </file>
///
/// <copyright file="NewOpen.xaml.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the new open.xaml class. </summary>
#pragma warning restore 1587

/// <file>  FHR_TIA_GIT_2\ViewsAndModels\Views\NewOpen.xaml.cs </file>
///
/// <copyright file="NewOpen.xaml.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the new open.xaml class. </summary>

using System.Windows.Controls;

namespace FHR_TIA_GIT_2.ViewsAndModels.Views
{
    public partial class NewOpen : UserControl
    {
        /// <summary>   Default constructor. </summary>
        public NewOpen()
        {
            InitializeComponent();
        }
    }
}