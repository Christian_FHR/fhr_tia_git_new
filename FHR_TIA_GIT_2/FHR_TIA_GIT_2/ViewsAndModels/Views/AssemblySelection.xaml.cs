﻿
#pragma warning disable 1587
/// <file>  FHR_TIA_GIT\ViewsAndModels\Views\AssemblySelection.xaml.cs </file>
///
/// <copyright file="AssemblySelection.xaml.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the assembly selection.xaml class. </summary>
#pragma warning restore 1587

/// <file>  FHR_TIA_GIT_2\ViewsAndModels\Views\AssemblySelection.xaml.cs </file>
///
/// <copyright file="AssemblySelection.xaml.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the assembly selection.xaml class. </summary>

using System.Windows;

namespace FHR_TIA_GIT_2.ViewsAndModels.Views
{
    public partial class AssemblySelection : Window
    {
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="viewModel">    The view model. </param>

        public AssemblySelection(Models.ViewModelBase viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
        }
    }
}