﻿
#pragma warning disable 1587
/// <file>  FHR_TIA_GIT\ViewsAndModels\Views\MainWindow.xaml.cs </file>
///
/// <copyright file="MainWindow.xaml.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the main window.xaml class. </summary>
#pragma warning restore 1587

/// <file>  FHR_TIA_GIT_2\ViewsAndModels\Views\MainWindow.xaml.cs </file>
///
/// <copyright file="MainWindow.xaml.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the main window.xaml class. </summary>

using System;
using FHR_TIA_GIT_2.ViewsAndModels.Models;

namespace FHR_TIA_GIT_2.ViewsAndModels.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        /// <summary>   (Immutable) the view model. </summary>
        private readonly MainWindowModel _viewModel = new MainWindowModel();
        /// <summary>   Default constructor. </summary>
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = _viewModel;
            this.Closed += OnClosed;
        }

        /// <summary>   Raises the closed event. </summary>
        ///
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>

        private void OnClosed(object sender, EventArgs e)
        {
            _viewModel.Dispose();
            App.Current.Shutdown();
        }
    }
}