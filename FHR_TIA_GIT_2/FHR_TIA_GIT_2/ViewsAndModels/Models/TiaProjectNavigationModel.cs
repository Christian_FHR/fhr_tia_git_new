﻿// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT\ViewsAndModels\Models\TiaProjectNavigationModel.cs </file>
///
/// <copyright file="TiaProjectNavigationModel.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the tia project navigation model class. </summary>

using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using FHR_TIA_GIT_2.Properties;
using FHR_TIA_GIT_2.Utillities;
using Siemens.Engineering;

namespace FHR_TIA_GIT_2.ViewsAndModels.Models
{
    /// <summary>   A data Model for the tia project navigation. </summary>
    public class TiaProjectNavigationModel : ViewModelBase, IDisposable
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public TiaProjectNavigationModel()
        {
            TiaInstances = new ObservableCollection<TIA.TiaObjects.TiaInstance>();
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        /// <summary>   The tia instances. </summary>
        private ObservableCollection<TIA.TiaObjects.TiaInstance> _tiaInstances;
        /// <summary>   The selected item. </summary>
        private TIA.TiaObjects.TiaInstance _selectedItem;

        #endregion

        #region Properties

        /// <summary>   Gets or sets the tia instances. </summary>
        ///
        /// <value> The tia instances. </value>

        public ObservableCollection<TIA.TiaObjects.TiaInstance> TiaInstances
        {
            get { return _tiaInstances; }
            set
            {
                _tiaInstances = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>   Gets or sets the selected item. </summary>
        ///
        /// <value> The selected item. </value>

        public TIA.TiaObjects.TiaInstance SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                if (_selectedItem != null && _selectedItem == value)
                    return;
                _selectedItem = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Methods

        /// <summary>   Adds a new tia instance. </summary>
        public void AddNewTiaInstance()
        {
            Task.Factory.StartNew(() =>
            {
                var tmp = new TIA.TiaObjects.TiaInstance(
                    new TiaPortal(Settings.Default.UserInterfaceEnabled
                        ? TiaPortalMode.WithUserInterface
                        : TiaPortalMode.WithoutUserInterface));
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                {
                    tmp.CmdDisconnect = new CommandBase(OnCmdDisconnect);
                    TiaInstances.Add(tmp);
                    SelectedItem = tmp;
                }));
            });
        }

        /// <summary>   Adds a new tia instance. </summary>
        ///
        /// <param name="projectPath">  Full pathname of the project file. </param>

        public void AddNewTiaInstance(string projectPath)
        {
            // Check the Version of the selected project
            var selectedVersion = Convert.ToInt32(Path.GetExtension(projectPath).Replace(".ap", ""));
            var settingsVersion = Convert.ToInt32(Convert.ToDouble(Settings.Default.EngineeringVersion));
            if (selectedVersion < settingsVersion)
            {
                string text =
                    string.Format(
                        @"The project to be opened is version V{0} and not compatible with V{1}. Do you want to upgrade it?",
                        selectedVersion, settingsVersion);
                string head = @"Version not compatible";
                if (MessageBox.Show(text, head, MessageBoxButton.YesNo, MessageBoxImage.Question) !=
                    MessageBoxResult.Yes) return;
                
                Task.Factory.StartNew(() =>
                {
                    var newTia = new TiaPortal(Settings.Default.UserInterfaceEnabled
                        ? TiaPortalMode.WithUserInterface
                        : TiaPortalMode.WithoutUserInterface);
                    newTia.Projects.OpenWithUpgrade(new FileInfo(projectPath));
                    var tmp = new TIA.TiaObjects.TiaInstance(newTia);
                    
                    Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                    {
                        tmp.CmdDisconnect = new CommandBase(OnCmdDisconnect);
                        TiaInstances.Add(tmp);
                        SelectedItem = tmp;
                    }));
                });
            }
            else if (selectedVersion == settingsVersion)
            {
                Task.Factory.StartNew(() =>
                {
                    var newTia = new TiaPortal(Settings.Default.UserInterfaceEnabled
                        ? TiaPortalMode.WithUserInterface
                        : TiaPortalMode.WithoutUserInterface);
                    newTia.Projects.Open(new FileInfo(projectPath));
                    var tmp = new TIA.TiaObjects.TiaInstance(newTia);
                    
                    Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                    {
                        tmp.CmdDisconnect = new CommandBase(OnCmdDisconnect);
                        TiaInstances.Add(tmp);
                        SelectedItem = tmp;
                    }));
                });
            }
            else
            {
                string text =
                    string.Format(
                        @"The project to be opened is version V{0} and cannot be opened with V{1}.",
                        selectedVersion, settingsVersion);
                string head = @"Version not compatible";
                MessageBox.Show(text, head, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        /// <summary>   Adds a new tia instance. </summary>
        ///
        /// <param name="tiaPortalProcess"> The tia portal process. </param>

        public void AddNewTiaInstance(TiaPortalProcess tiaPortalProcess)
        {
            var tmp = new TIA.TiaObjects.TiaInstance(tiaPortalProcess);
            tmp.CmdDisconnect = new CommandBase(OnCmdDisconnect);
            TiaInstances.Add(tmp);
            SelectedItem = tmp;
        }

        /// <summary>   Adds a new tia instance. </summary>
        ///
        /// <param name="tiaPortal">    The tia portal. </param>

        public void AddNewTiaInstance(TiaPortal tiaPortal)
        {
            var tmp = new TIA.TiaObjects.TiaInstance(tiaPortal);
            tmp.CmdDisconnect = new CommandBase(OnCmdDisconnect);
            TiaInstances.Add(tmp);
            SelectedItem = tmp;
        }

        /// <summary>   Adds a new tia instance. </summary>
        ///
        /// <param name="tiaGitDirectoryPath">  Pathname of the tia git directory. </param>
        /// <param name="bla">                  (Optional) True to bla. </param>

        // ReSharper disable once MethodOverloadWithOptionalParameter
        public void AddNewTiaInstance(string tiaGitDirectoryPath, bool bla = false)
        {
            foreach (string file in Directory.GetFiles(tiaGitDirectoryPath, "*.xml"))
            {
                var tmp = new TIA.TiaObjects.TiaInstance(file, tiaGitDirectoryPath);
                tmp.CmdDisconnect = new CommandBase(OnCmdDisconnect);
                TiaInstances.Add(tmp);
                SelectedItem = tmp;
            }
        }

        /// <summary>   Executes the 'command disconnect' action. </summary>
        ///
        /// <param name="obj">  The object. </param>

        private void OnCmdDisconnect(object obj)
        {
            if (obj == null) return;
            var instance = (TIA.TiaObjects.TiaInstance) obj;
            TiaInstances.Remove(instance);
            instance.Dispose();
        }

        /// <summary>   Determines if we can export selected. </summary>
        ///
        /// <returns>   True if it succeeds, false if it fails. </returns>

        public bool ExportSelected()
        {
            // if (SelectedItem != null)
            // {
            //     return SelectedItem.ExportSelected();
            // }
            // else
            // {
            //     return true;
            // }

            return false;
        }

        /// <summary>   Import to selected. </summary>
        public void ImportToSelected()
        {
            // if (SelectedItem != null)
            // {
            //     SelectedItem.ImportToSelected();
            // }
        }

        /// <summary>
        ///     Führt anwendungsspezifische Aufgaben durch, die mit der Freigabe, der Zurückgabe oder dem
        ///     Zurücksetzen von nicht verwalteten Ressourcen zusammenhängen.
        /// </summary>

        public void Dispose()
        {
            TiaInstances.ToList().ForEach(x => x.Dispose());
        }
        
        #endregion

        #region Override metods

        #endregion
        
    }
}