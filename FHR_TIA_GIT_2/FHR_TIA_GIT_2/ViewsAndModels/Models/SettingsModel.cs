﻿
#pragma warning disable 1587
/// <file>  FHR_TIA_GIT\ViewsAndModels\Models\SettingsModel.cs </file>
///
/// <copyright file="SettingsModel.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the settings model class. </summary>
#pragma warning restore 1587

/// <file>  FHR_TIA_GIT_2\ViewsAndModels\Models\SettingsModel.cs </file>
///
/// <copyright file="SettingsModel.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the settings model class. </summary>

using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows.Forms;
using FHR_TIA_GIT_2.Properties;
using FHR_TIA_GIT_2.Utillities;
using static FHR_TIA_GIT_2.Utillities.AssemblyHelper.AssemblyHelper;

namespace FHR_TIA_GIT_2.ViewsAndModels.Models
{
    /// <summary>   A data Model for the settings. </summary>
    public class SettingsModel : ViewModelBase, IDisposable
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public SettingsModel()
        {
            LoadConfiguration();
            InitializeCommand();
        }
        
        #endregion

        #region Types

        #endregion

        #region Fields

        /// <summary>   Full pathname of the library git file. </summary>
        private string _libraryGitPath;
        /// <summary>   True to use project path as git path. </summary>
        private bool _useProjectPathAsGitPath;
        /// <summary>   Full pathname of the project git file. </summary>
        private string _projectGitPath;
        /// <summary>   True to use tia interface. </summary>
        private bool _useTiaInterface;

        /// <summary>   The engineering version. </summary>
        private string _engineeringVersion;
        /// <summary>   The assembly version. </summary>
        private string _assemblyVersion;
        /// <summary>   True to hide, false to show the assembly selection. </summary>
        private bool _hideAssemblySelection;
        /// <summary>   The engineering versions. </summary>
        private ObservableCollection<string> _engineeringVersions;
        /// <summary>   The assembly versions. </summary>
        private ObservableCollection<string> _assemblyVersions;

        #endregion

        #region Properties

        /// <summary>   Gets or sets the full pathname of the library git file. </summary>
        ///
        /// <value> The full pathname of the library git file. </value>

        public string LibraryGitPath
        {
            get { return _libraryGitPath; }
            set
            {
                _libraryGitPath = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether this object use project path as git path.
        /// </summary>
        ///
        /// <value> True if use project path as git path, false if not. </value>

        public bool UseProjectPathAsGitPath
        {
            get { return _useProjectPathAsGitPath; }
            set { _useProjectPathAsGitPath = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets the full pathname of the project git file. </summary>
        ///
        /// <value> The full pathname of the project git file. </value>

        public string ProjectGitPath
        {
            get { return _projectGitPath; }
            set
            {
                _projectGitPath = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>   Gets or sets a value indicating whether this object use tia interface. </summary>
        ///
        /// <value> True if use tia interface, false if not. </value>

        public bool UseTiaInterface
        {
            get { return _useTiaInterface; }
            set
            {
                _useTiaInterface = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>   Gets or sets the engineering version. </summary>
        ///
        /// <value> The engineering version. </value>

        public string EngineeringVersion
        {
            get { return _engineeringVersion; }
            set
            {
                _engineeringVersion = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>   Gets or sets the assembly version. </summary>
        ///
        /// <value> The assembly version. </value>

        public string AssemblyVersion
        {
            get { return _assemblyVersion; }
            set
            {
                _assemblyVersion = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>   Gets or sets a value indicating whether the assembly selection is hidden. </summary>
        ///
        /// <value> True if hide assembly selection, false if not. </value>

        public bool HideAssemblySelection
        {
            get { return _hideAssemblySelection; }
            set
            {
                _hideAssemblySelection = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>   Gets or sets the engineering versions. </summary>
        ///
        /// <value> The engineering versions. </value>

        public ObservableCollection<string> EngineeringVersions
        {
            get { return _engineeringVersions; }
            set
            {
                _engineeringVersions = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>   Gets or sets the assembly versions. </summary>
        ///
        /// <value> The assembly versions. </value>

        public ObservableCollection<string> AssemblyVersions
        {
            get { return _assemblyVersions; }
            set
            {
                _assemblyVersions = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>   Gets or sets the full pathname of the set library git file. </summary>
        ///
        /// <value> The full pathname of the set library git file. </value>

        public CommandBase SetLibraryGitPath { get; private set; }

        /// <summary>   Gets or sets the get engineering versions. </summary>
        ///
        /// <value> The get engineering versions. </value>

        public CommandBase GetEngineeringVersions { get; private set; }

        /// <summary>   Gets or sets the get assembly versions. </summary>
        ///
        /// <value> The get assembly versions. </value>

        public CommandBase GetAssemblyVersions { get; private set; }

        /// <summary>   Gets or sets the command save settings. </summary>
        ///
        /// <value> The command save settings. </value>

        public CommandBase CmdSaveSettings { get; private set; }
        
        #endregion

        #region Methods

        /// <summary>   Initializes the command. </summary>
        private void InitializeCommand()
        {
            SetLibraryGitPath = new CommandBase(OnSetLibraryGitPath);
            GetEngineeringVersions = new CommandBase(OnGetEngineeringVersions);
            GetAssemblyVersions = new CommandBase(OnGetAssemblyVersions);
            CmdSaveSettings = new CommandBase(OnCmdSaveSettings);
        }

        /// <summary>   Loads the configuration. </summary>
        public void LoadConfiguration()
        {
            LibraryGitPath = Settings.Default.LibraryGitPath;
            UseTiaInterface = Settings.Default.UserInterfaceEnabled;
            OnGetEngineeringVersions(null);
            EngineeringVersion = Settings.Default.EngineeringVersion;
            OnGetAssemblyVersions(null);
            AssemblyVersion = Settings.Default.AssemblyVersion;
            HideAssemblySelection = Settings.Default.HideAssemblySelection;
            UseProjectPathAsGitPath = Settings.Default.UseProjectPathAsGitPath;
        }
        
        /// <summary>   Saves the configuration. </summary>
        public void SaveConfiguration()
        {
            Settings.Default.LibraryGitPath = LibraryGitPath;
            Settings.Default.UserInterfaceEnabled = UseTiaInterface;
            Settings.Default.EngineeringVersion = EngineeringVersion;
            Settings.Default.AssemblyVersion = AssemblyVersion;
            Settings.Default.HideAssemblySelection = HideAssemblySelection;
            Settings.Default.UseProjectPathAsGitPath = UseProjectPathAsGitPath;
            
            Settings.Default.Save();
        }

        #region Commands execute

        /// <summary>   Executes the 'set library git path' action. </summary>
        ///
        /// <param name="obj">  The object. </param>

        private void OnSetLibraryGitPath(object obj)
        {
            using (FolderBrowserDialog fbd = new FolderBrowserDialog())
            {
                if (string.IsNullOrEmpty(LibraryGitPath))
                    fbd.SelectedPath = @"C:\fhr";
                else
                    fbd.SelectedPath = LibraryGitPath;

                if (fbd.ShowDialog() != DialogResult.OK)
                    return;

                if (!Directory.Exists(fbd.SelectedPath + @"\.git"))
                {
                    MessageBox.Show("Selected directory is not a GIT directory.", "Selection wrong", 
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                LibraryGitPath = fbd.SelectedPath;
            }
        }

        /// <summary>   Executes the 'get engineering versions' action. </summary>
        ///
        /// <param name="obj">  The object. </param>

        private void OnGetEngineeringVersions(object obj)
        {
            EngineeringVersions = new ObservableCollection<string>(GetVersions());
        }

        /// <summary>   Executes the 'get assembly versions' action. </summary>
        ///
        /// <param name="obj">  The object. </param>

        private void OnGetAssemblyVersions(object obj)
        {
            if(string.IsNullOrEmpty(EngineeringVersion)) return;
            AssemblyVersions = new ObservableCollection<string>(GetAssmblies(EngineeringVersion));
        }

        /// <summary>   Executes the 'command save settings' action. </summary>
        ///
        /// <param name="obj">  The object. </param>

        private void OnCmdSaveSettings(object obj)
        {
            SaveConfiguration();
        }
        
        #endregion

        /// <summary>
        ///     Führt anwendungsspezifische Aufgaben durch, die mit der Freigabe, der Zurückgabe oder dem
        ///     Zurücksetzen von nicht verwalteten Ressourcen zusammenhängen.
        /// </summary>

        public void Dispose()
        {
            //SaveConfiguration();
        }
        
        #endregion

        #region Override metods

        #endregion
    }
}