﻿
#pragma warning disable 1587
/// <file>  FHR_TIA_GIT\ViewsAndModels\Models\ViewModelBase.cs </file>
///
/// <copyright file="ViewModelBase.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the view model base class. </summary>
#pragma warning restore 1587

/// <file>  FHR_TIA_GIT_2\ViewsAndModels\Models\ViewModelBase.cs </file>
///
/// <copyright file="ViewModelBase.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the view model base class. </summary>

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Xml.Serialization;

namespace FHR_TIA_GIT_2.ViewsAndModels.Models
{
    /// <summary>   A view model base. </summary>
    public abstract class ViewModelBase : DependencyObject, INotifyPropertyChanged, INotifyPropertyChanging
    {
        /// <summary>   Tritt ein, wenn sich ein Eigenschaftswert ändert. </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>   Tritt auf, wenn sich ein Eigenschaftswert ändert. </summary>
        public event PropertyChangingEventHandler PropertyChanging;

        /// <summary>   Gets or sets a value indicating whether the ignore property change events. </summary>
        ///
        /// <value> True if ignore property change events, false if not. </value>

        [XmlIgnore]
        public virtual bool IgnorePropertyChangeEvents { get; set; }

        #region Public members

        /// <summary>   Raises the <see cref="E:PropertyChanged" /> event. </summary>
        ///
        /// <param name="e">    The <see cref="PropertyChangedEventArgs"/> instance containing the event
        ///                     data. </param>

        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>   Raises the PropertyChanged event. </summary>
        ///
        /// <param name="propertyName"> (Optional) The name of the changed property. </param>

        public virtual void RaisePropertyChangedEvent([CallerMemberName] string propertyName = "")
        {
            // Exit if changes ignored
            if (IgnorePropertyChangeEvents) return;

            // Exit if no subscribers
            if (PropertyChanged == null) return;

            // Raise event
            var e = new PropertyChangedEventArgs(propertyName);
            PropertyChanged(this, e);
        }

        /// <summary>   Raises the PropertyChanging event. </summary>
        ///
        /// <param name="propertyName"> (Optional) The name of the changing property. </param>

        public virtual void RaisePropertyChangingEvent([CallerMemberName] string propertyName = "")
        {
            // Exit if changes ignored
            if (IgnorePropertyChangeEvents) return;

            // Exit if no subscribers
            if (PropertyChanging == null) return;

            // Raise event
            var e = new PropertyChangingEventArgs(propertyName);
            PropertyChanging(this, e);
        }

        /// <summary>   Raises the property changed. </summary>
        ///
        /// <param name="propertyName"> (Optional) Name of the property. </param>

        protected void RaisePropertyChanged([CallerMemberName] String propertyName = "")
        {
            VerifyPropertyName(propertyName);
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        /// <summary>
        ///     Warns the developer if this Object does not have a public property with the specified
        ///     name. This method does not exist in a Release build.
        /// </summary>
        ///
        /// <param name="propertyName"> Name of the property. </param>

        [Conditional("DEBUG")]
        [DebuggerStepThrough]
        public void VerifyPropertyName(String propertyName)
        {
            // verify that the property name matches a real,  
            // public, instance property on this Object.
            if (TypeDescriptor.GetProperties(this)[propertyName] == null)
            {
                Debug.Fail("Invalid property name: " + propertyName);
            }
        }
    }
}