﻿// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT_2\ViewsAndModels\Models\ActiveTiaInstancesModel.cs </file>
///
/// <copyright file="ActiveTiaInstancesModel.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the active tia instances model class. </summary>

using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FHR_TIA_GIT_2.Utillities;
using FHR_TIA_GIT_2.Utillities.Logger;
using Siemens.Engineering;

namespace FHR_TIA_GIT_2.ViewsAndModels.Models
{
    /// <summary>   A data Model for the active tia instances. </summary>
    public class ActiveTiaInstancesModel : ViewModelBase, IDisposable
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public ActiveTiaInstancesModel()
        {
            RefreshInstances();
            _cts = new CancellationTokenSource();
            _refreshTask = Task.Factory.StartNew(AsyncRefreshInstances, _cts.Token);
        }

        #endregion

        #region Types

        #endregion

        #region Fields
        /// <summary>   (Immutable) the refresh task. </summary>
        private readonly Task _refreshTask;
        /// <summary>   (Immutable) the cts. </summary>
        private readonly CancellationTokenSource _cts;

        /// <summary>   The instances items. </summary>
        private ObservableCollection<ActiveTiaInstancesItem> _instancesItems =
            new ObservableCollection<ActiveTiaInstancesItem>();
        /// <summary>   The selected item. </summary>
        private ActiveTiaInstancesItem _selectedItem;

        /// <summary>   Connects to tia delegate. </summary>
        ///
        /// <param name="e">    An ActiveTiaInstancesItem to process. </param>

        public delegate void ConnectToTiaDelegate(ActiveTiaInstancesItem e);

        /// <summary>   Event queue for all listeners interested in EventConnectToTia events. </summary>
        public event ConnectToTiaDelegate EventConnectToTia;

        #endregion

        #region Properties

        /// <summary>   Gets or sets the instances items. </summary>
        ///
        /// <value> The instances items. </value>

        public ObservableCollection<ActiveTiaInstancesItem> InstancesItems
        {
            get { return _instancesItems; }
            set
            {
                if (_instancesItems.Count == value.Count)
                    return;
                _instancesItems = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>   Gets or sets the selected item. </summary>
        ///
        /// <value> The selected item. </value>

        public ActiveTiaInstancesItem SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                if (_selectedItem != null && _selectedItem.Equals(value))
                    return;
                _selectedItem = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Methods

        /// <summary>   Select target. </summary>
        public void SelectTarget()
        {
            EventConnectToTia?.Invoke(SelectedItem);
        }
        
        /// <summary>   Refresh instances. </summary>
        private void RefreshInstances()
        {
            var tmpList = new ObservableCollection<ActiveTiaInstancesItem>();
            TiaPortal.GetProcesses().ToList().ForEach(x =>
            {
                var instance = new ActiveTiaInstancesItem(x);
                instance.evtConnectTo += delegate(object sender, EventArgs args) { SelectedItem = (ActiveTiaInstancesItem)sender; SelectTarget(); };
                tmpList.Add(instance);
            });
            InstancesItems = tmpList;
        }

        /// <summary>   Asynchronous refresh instances. </summary>
        ///
        /// <returns>   An asynchronous result. </returns>

        private async Task AsyncRefreshInstances()
        {
            try
            {
                using (_cts.Token.Register(Thread.CurrentThread.Abort))
                {
                    do
                    {
                        await Task.Delay(1000);
                        var tmpSelected = SelectedItem;
                        RefreshInstances();
                        SelectedItem = tmpSelected;
                    } while (true);
                }
            }
            catch (ThreadAbortException e)
            {
                Log4Net.Log.InfoFormat("Thread abort: {0}", e.Message);
            }
        }

        /// <summary>
        ///     Führt anwendungsspezifische Aufgaben durch, die mit der Freigabe, der Zurückgabe oder dem
        ///     Zurücksetzen von nicht verwalteten Ressourcen zusammenhängen.
        /// </summary>

        public void Dispose()
        {
            _cts.Cancel();
            do
            {
                Thread.Sleep(10);
            } while (_refreshTask.Status == TaskStatus.Running);
            _refreshTask?.Dispose();
        }
        
        #endregion

        #region Override metods

        #endregion
    }
    
    /// <summary>   An active tia instances item. </summary>
    public class ActiveTiaInstancesItem : ViewModelBase, IDisposable
    {
        #region Constructors

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="activeProcess">    The active process. </param>

        public ActiveTiaInstancesItem(TiaPortalProcess activeProcess)
        {
            ActiveProcess = activeProcess;
            CmdConnectTo = new CommandBase(OnCmdConnectTo);
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        /// <summary>   The header. </summary>
        private string _header;
        /// <summary>   The active process. </summary>
        private TiaPortalProcess _activeProcess;
        
        /// <summary>   Event queue for all listeners interested in evtConnectTo events. </summary>
        public event EventHandler evtConnectTo;
        
        #endregion

        #region Properties

        /// <summary>   Gets or sets the header. </summary>
        ///
        /// <value> The header. </value>

        public string Header
        {
            get { return _header; }
            set
            {
                _header = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>   Gets or sets the active process. </summary>
        ///
        /// <value> The active process. </value>

        public TiaPortalProcess ActiveProcess
        {
            get { return _activeProcess; }
            set
            {
                _activeProcess = value;
                SetHeader(_activeProcess);
                RaisePropertyChanged();
            }
        }

        /// <summary>   Gets or sets the command connect to. </summary>
        ///
        /// <value> The command connect to. </value>

        public CommandBase CmdConnectTo { get; set; }

        #endregion

        #region Methods

        /// <summary>   Sets a header. </summary>
        ///
        /// <param name="_process"> The process. </param>

        private void SetHeader(TiaPortalProcess _process)
        {
            string tmpHeader = "";
            if (_process.ProjectPath != null)
                tmpHeader = Path.GetFileNameWithoutExtension(_process.ProjectPath.ToString());
            else
            {
                tmpHeader = string.Format("No project loaded [PID: {0}]", _process.Id);
            }

            if (_process.Mode == TiaPortalMode.WithUserInterface)
                tmpHeader += " [With UI]";
            else
                tmpHeader += " [Without UI]";

            Header = tmpHeader;
        }

        /// <summary>   Executes the 'command connect to' action. </summary>
        ///
        /// <param name="obj">  The object. </param>

        private void OnCmdConnectTo(object obj)
        {
            evtConnectTo?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        ///     Führt anwendungsspezifische Aufgaben durch, die mit der Freigabe, der Zurückgabe oder dem
        ///     Zurücksetzen von nicht verwalteten Ressourcen zusammenhängen.
        /// </summary>

        public void Dispose()
        {
        }
        
        #endregion

        #region Override metods

        #endregion
    }
}