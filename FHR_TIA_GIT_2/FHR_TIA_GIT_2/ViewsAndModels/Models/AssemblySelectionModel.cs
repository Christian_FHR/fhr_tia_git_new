﻿
#pragma warning disable 1587
/// <file>  FHR_TIA_GIT\ViewsAndModels\Models\AssemblySelectionModel.cs </file>
///
/// <copyright file="AssemblySelectionModel.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the assembly selection model class. </summary>
#pragma warning restore 1587

/// <file>  FHR_TIA_GIT_2\ViewsAndModels\Models\AssemblySelectionModel.cs </file>
///
/// <copyright file="AssemblySelectionModel.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the assembly selection model class. </summary>

using System;
using System.Collections.ObjectModel;
using System.Windows;
using FHR_TIA_GIT_2.Utillities;
using static FHR_TIA_GIT_2.Utillities.AssemblyHelper.AssemblyHelper;

namespace FHR_TIA_GIT_2.ViewsAndModels.Models
{
    /// <summary>   A data Model for the assembly selection. </summary>
    public class AssemblySelectionModel : ViewModelBase
    {
        #region Fields

        /// <summary>   The engineering versions. </summary>
        private ObservableCollection<string> _engineeringVersions = new ObservableCollection<string>();
        /// <summary>   The assembly version. </summary>
        private ObservableCollection<string> _assemblyVersion = new ObservableCollection<string>();
        /// <summary>   The selected version. </summary>
        private string _selectedVersion;
        /// <summary>   The selected assembly. </summary>
        private string _selectedAssembly;
        /// <summary>   True to hide, false to show. </summary>
        private bool _hide;
        /// <summary>   True to dialog result. </summary>
        private bool _dialogResult;

        #endregion

        #region Properties

        /// <summary>   Gets or sets the engineering versions. </summary>
        ///
        /// <value> The engineering versions. </value>

        public ObservableCollection<string> EngineeringVersions
        {
            get { return _engineeringVersions; }
            set
            {
                _engineeringVersions = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>   Gets or sets the assembly version. </summary>
        ///
        /// <value> The assembly version. </value>

        public ObservableCollection<string> AssemblyVersion
        {
            get { return _assemblyVersion; }
            set
            {
                _assemblyVersion = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>   Gets or sets the selected version. </summary>
        ///
        /// <value> The selected version. </value>

        public string SelectedVersion
        {
            get { return _selectedVersion; }
            set
            {
                _selectedVersion = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>   Gets or sets the selected assembly. </summary>
        ///
        /// <value> The selected assembly. </value>

        public string SelectedAssembly
        {
            get { return _selectedAssembly; }
            set
            {
                _selectedAssembly = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>   Gets or sets a value indicating whether this object is hidden. </summary>
        ///
        /// <value> True if hide, false if not. </value>

        public bool Hide
        {
            get { return _hide; }
            set
            {
                _hide = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>   Gets or sets a value indicating whether the dialog result. </summary>
        ///
        /// <value> True if dialog result, false if not. </value>

        public bool DialogResult
        {
            get { return _dialogResult; }
            set
            {
                _dialogResult = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>   The result. </summary>
        public string Result;

        /// <summary>   Gets the 'confirmation' command. </summary>
        ///
        /// <value> The 'confirmation' command. </value>

        public RelayCommand<EventArgs> ConfirmationCommand
        {
            get { return new RelayCommand<EventArgs>(OnConfirmation, CanConfirm); }
        }

        /// <summary>   Gets the 'get version' command. </summary>
        ///
        /// <value> The 'get version' command. </value>

        public RelayCommand<EventArgs> GetVersionCommand
        {
            get { return new RelayCommand<EventArgs>(OnGetVersion); }
        }

        /// <summary>   Gets the 'get assemblies' command. </summary>
        ///
        /// <value> The 'get assemblies' command. </value>

        public RelayCommand<EventArgs> GetAssembliesCommand
        {
            get { return new RelayCommand<EventArgs>(OnGetAssemblies); }
        }

        /// <summary>   Event queue for all listeners interested in OnRequestClose events. </summary>
        public event EventHandler OnRequestClose;
        
        #endregion

        #region Methodes

        /// <summary>   Raises the confirmation event. </summary>
        ///
        /// <param name="obj">  Event information to send to registered event handlers. </param>

        private void OnConfirmation(EventArgs obj)
        {
            if (CanConfirm())
            {
                Result = GetAssemblyPath(SelectedVersion, SelectedAssembly);
                DialogResult = true;
                OnRequestClose?.Invoke(this, EventArgs.Empty);
            }
        }

        /// <summary>   Determine if we can confirm. </summary>
        ///
        /// <returns>   True if we can confirm, false if not. </returns>

        private bool CanConfirm()
        {
            return (!string.IsNullOrEmpty(SelectedVersion)) && (!string.IsNullOrEmpty(SelectedAssembly));
        }

        /// <summary>   Raises the get version event. </summary>
        ///
        /// <param name="obj">  Event information to send to registered event handlers. </param>

        private void OnGetVersion(EventArgs obj)
        {
            EngineeringVersions = new ObservableCollection<string>(GetVersions());
        }

        /// <summary>   Raises the get assemblies event. </summary>
        ///
        /// <param name="obj">  Event information to send to registered event handlers. </param>

        private void OnGetAssemblies(EventArgs obj)
        {
            if (string.IsNullOrEmpty(SelectedVersion)) return;
            AssemblyVersion = new ObservableCollection<string>(GetAssmblies(SelectedVersion));
        }

        #endregion
    }
}