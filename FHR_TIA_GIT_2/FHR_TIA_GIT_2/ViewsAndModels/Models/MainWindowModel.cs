﻿
#pragma warning disable 1587
/// <file>  FHR_TIA_GIT\ViewsAndModels\Models\MainWindowModel.cs </file>
///
/// <copyright file="MainWindowModel.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the main window model class. </summary>
#pragma warning restore 1587

/// <file>  FHR_TIA_GIT_2\ViewsAndModels\Models\MainWindowModel.cs </file>
///
/// <copyright file="MainWindowModel.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the main window model class. </summary>

using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using FHR_TIA_GIT_2.Utillities;
using Siemens.Engineering;
using static FHR_TIA_GIT_2.Utillities.Logger.Log4Net;

namespace FHR_TIA_GIT_2.ViewsAndModels.Models
{
    /// <summary>   A data Model for the main window. </summary>
    public class MainWindowModel : ViewModelBase, IDisposable
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public MainWindowModel()
        {
            InitializeCommands();

            ShowRibbonBackstage = true;
            SettingsModel = new SettingsModel();
            TiaInstancesModel = new ActiveTiaInstancesModel();
            TiaProjectNavigationModel = new TiaProjectNavigationModel();

            NewProjectAuthor = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            TiaInstancesModel.EventConnectToTia += TiaInstancesModelOnEventConnectToTia;
        }
        
        #endregion

        #region Types

        #endregion

        #region Fields

        /// <summary>   The settings model. </summary>
        private SettingsModel _settingsModel;
        /// <summary>   The tia instances model. </summary>
        private ActiveTiaInstancesModel _tiaInstancesModel;
        /// <summary>   The tia project navigation model. </summary>
        private TiaProjectNavigationModel _tiaProjectNavigationModel;

        /// <summary>   True to show, false to hide the ribbon backstage. </summary>
        private bool _showRibbonBackstage;
        /// <summary>   True to export run. </summary>
        private bool _exportRun = false;
        /// <summary>   True to import run. </summary>
        private bool _importRun = false;

        /// <summary>   Pathname of the new project target directory. </summary>
        private string _newProjectTargetDirectory;
        /// <summary>   Name of the new project. </summary>
        private string _newProjectName;
        /// <summary>   The new project author. </summary>
        private string _newProjectAuthor;
        /// <summary>   The new project comment. </summary>
        private string _newProjectComment;
        
        #endregion

        #region Properties

        /// <summary>   Gets or sets the settings model. </summary>
        ///
        /// <value> The settings model. </value>

        public SettingsModel SettingsModel
        {
            get { return _settingsModel; }
            set
            {
                _settingsModel = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>   Gets or sets the tia instances model. </summary>
        ///
        /// <value> The tia instances model. </value>

        public ActiveTiaInstancesModel TiaInstancesModel
        {
            get { return _tiaInstancesModel; }
            set
            {
                _tiaInstancesModel = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>   Gets or sets the tia project navigation model. </summary>
        ///
        /// <value> The tia project navigation model. </value>

        public TiaProjectNavigationModel TiaProjectNavigationModel
        {
            get { return _tiaProjectNavigationModel; }
            set
            {
                _tiaProjectNavigationModel = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>   Gets or sets a value indicating whether the ribbon backstage is shown. </summary>
        ///
        /// <value> True if show ribbon backstage, false if not. </value>

        public bool ShowRibbonBackstage
        {
            get { return _showRibbonBackstage; }
            set
            {
                _showRibbonBackstage = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>   Gets or sets the pathname of the new project target directory. </summary>
        ///
        /// <value> The pathname of the new project target directory. </value>

        public string NewProjectTargetDirectory
        {
            get { return _newProjectTargetDirectory; }
            set { _newProjectTargetDirectory = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets the name of the new project. </summary>
        ///
        /// <value> The name of the new project. </value>

        public string NewProjectName
        {
            get { return _newProjectName; }
            set { _newProjectName = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets the new project author. </summary>
        ///
        /// <value> The new project author. </value>

        public string NewProjectAuthor
        {
            get { return _newProjectAuthor; }
            set { _newProjectAuthor = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets the new project comment. </summary>
        ///
        /// <value> The new project comment. </value>

        public string NewProjectComment
        {
            get { return _newProjectComment; }
            set { _newProjectComment = value; RaisePropertyChanged(); }
        }

        #region Commands

        /// <summary>   Gets or sets the command new tia instance. </summary>
        ///
        /// <value> The command new tia instance. </value>

        public CommandBase CmdNewTiaInstance { get; set; }

        /// <summary>   Gets or sets the command new tia project. </summary>
        ///
        /// <value> The command new tia project. </value>

        public CommandBase CmdNewTiaProject { get; set; }

        /// <summary>   Gets or sets the command open tia project local. </summary>
        ///
        /// <value> The command open tia project local. </value>

        public CommandBase CmdOpenTiaProjectLocal { get; set; }

        /// <summary>   Gets or sets the command open git repository. </summary>
        ///
        /// <value> The command open git repository. </value>

        public CommandBase CmdOpenGitRepository { get; set; }

        /// <summary>   Gets or sets the command clone tia project. </summary>
        ///
        /// <value> The command clone tia project. </value>

        public CommandBase CmdCloneTiaProject { get; set; }

        /// <summary>   Gets or sets the pathname of the command select target directory. </summary>
        ///
        /// <value> The pathname of the command select target directory. </value>

        public CommandBase CmdSelectTargetDirectory { get; set; }

        /// <summary>   Gets or sets the command export selected. </summary>
        ///
        /// <value> The command export selected. </value>

        public CommandBase CmdExportSelected { get; set; }

        /// <summary>   Gets or sets the command import to selected. </summary>
        ///
        /// <value> The command import to selected. </value>

        public CommandBase CmdImportToSelected { get; set; }
        
        #endregion
        
        #endregion

        #region Methods

        /// <summary>   Initializes the commands. </summary>
        private void InitializeCommands()
        {
            CmdNewTiaInstance = new CommandBase(OnCmdNewTiaInstance);
            CmdNewTiaProject = new CommandBase(OnCmdNewTiaProject);
            CmdOpenTiaProjectLocal = new CommandBase(OnCmdOpenTiaProjectLocal);
            CmdOpenGitRepository = new CommandBase(OnCmdOpenGitRepository);
            CmdCloneTiaProject = new CommandBase(OnCmdCloneTiaProject);
            CmdSelectTargetDirectory = new CommandBase(OnCmdSelectTargetDirectory);

            CmdExportSelected = new CommandBase(OnCmdExportSelected, CanCmdExportSelected);
            CmdImportToSelected = new CommandBase(OnCmdImportToSelected, CanCmdImportToSelected);
        }

        /// <summary>   Tia instances model on event connect to tia. </summary>
        ///
        /// <param name="e">    An ActiveTiaInstancesItem to process. </param>

        private void TiaInstancesModelOnEventConnectToTia(ActiveTiaInstancesItem e)
        {
            if (e != null && e.ActiveProcess != null)
            {
                ShowRibbonBackstage = false;
                TiaProjectNavigationModel.AddNewTiaInstance(e.ActiveProcess);
            }
        }
        
        #region Command execute

        /// <summary>   Executes the 'command new tia instance' action. </summary>
        ///
        /// <param name="obj">  The object. </param>

        private void OnCmdNewTiaInstance(object obj)
        {
            ShowRibbonBackstage = false;
            TiaProjectNavigationModel.AddNewTiaInstance();
        }

        /// <summary>   Executes the 'command new tia project' action. </summary>
        ///
        /// <param name="obj">  The object. </param>

        private void OnCmdNewTiaProject(object obj)
        {
            if (string.IsNullOrEmpty(NewProjectTargetDirectory))
            {
                string infoText = "New TIA Project: Target directory must not be empty!";
                Log.Info(infoText);
                MessageBox.Show(infoText, "Input warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (string.IsNullOrEmpty(NewProjectName))
            {
                string infoText = "New TIA Project: Project name must not be empty!";
                Log.Info(infoText);
                MessageBox.Show(infoText, "Input warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            
            if (string.IsNullOrEmpty(NewProjectAuthor))
            {
                string infoText = "New TIA Project: Project author must not be empty!";
                Log.Info(infoText);
                MessageBox.Show(infoText, "Input warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            TiaPortal tiaPortal = new TiaPortal(
                Properties.Settings.Default.UserInterfaceEnabled
                    ? TiaPortalMode.WithUserInterface
                    : TiaPortalMode.WithoutUserInterface);

            ProjectComposition projectComposition = tiaPortal.Projects;

            IEnumerable<KeyValuePair<string, object>> createParameters = new[]
            {
                new KeyValuePair<string, object>("TargetDirectory", new DirectoryInfo(NewProjectTargetDirectory)),
                new KeyValuePair<string, object>("Name", NewProjectName),
                new KeyValuePair<string, object>("Author", NewProjectAuthor),
                new KeyValuePair<string, object>("Comment", NewProjectComment),
            };

            ((IEngineeringComposition) projectComposition).Create(typeof(Project), createParameters);

            ShowRibbonBackstage = false;
            TiaProjectNavigationModel.AddNewTiaInstance(tiaPortal);
        }

        /// <summary>   Executes the 'command open tia project local' action. </summary>
        ///
        /// <param name="obj">  The object. </param>

        private void OnCmdOpenTiaProjectLocal(object obj)
        {
            OpenFileDialog ofd = new OpenFileDialog()
            {
                Filter = @"Siemens TIA Portal project| *.ap*",
                FilterIndex = 2,
                RestoreDirectory = true,
                Multiselect = false,
            };

            if (ofd.ShowDialog() != DialogResult.OK || string.IsNullOrEmpty(ofd.FileName)) return;
            ShowRibbonBackstage = false;
            TiaProjectNavigationModel.AddNewTiaInstance(ofd.FileName);
            
        }

        /// <summary>   Executes the 'command open git repository' action. </summary>
        ///
        /// <param name="obj">  The object. </param>

        private void OnCmdOpenGitRepository(object obj)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog()
            {
                Description = "Select the GIT Folder to open",
                ShowNewFolderButton = false,
            };
            
            if (fbd.ShowDialog() != DialogResult.OK) return;
            
            TiaProjectNavigationModel.AddNewTiaInstance(fbd.SelectedPath, false);
        }

        /// <summary>   Executes the 'command clone tia project' action. </summary>
        ///
        /// <param name="obj">  The object. </param>

        private void OnCmdCloneTiaProject(object obj)
        {
        }

        /// <summary>   Executes the 'command select target directory' action. </summary>
        ///
        /// <param name="obj">  The object. </param>

        private void OnCmdSelectTargetDirectory(object obj)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog()
            {
                ShowNewFolderButton = true,
                Description = @"Select the target directory for the new TIA project",
            };

            if (fbd.ShowDialog() == DialogResult.OK)
            {
                NewProjectTargetDirectory = fbd.SelectedPath;
            }
            
            fbd.Dispose();
            fbd = null;
        }

        /// <summary>   Executes the 'command export selected' action. </summary>
        ///
        /// <param name="obj">  The object. </param>

        private void OnCmdExportSelected(object obj)
        {
            _exportRun = true;
            TiaProjectNavigationModel.ExportSelected();
            _exportRun = false;
        }

        /// <summary>   Determine if we can command export selected. </summary>
        ///
        /// <param name="obj">  The object. </param>
        ///
        /// <returns>   True if we can command export selected, false if not. </returns>

        private bool CanCmdExportSelected(object obj)
        {
            return !_exportRun;
        }

        /// <summary>   Executes the 'command import to selected' action. </summary>
        ///
        /// <param name="obj">  The object. </param>

        private void OnCmdImportToSelected(object obj)
        {
            _importRun = true;
            TiaProjectNavigationModel.ImportToSelected();
            _importRun = false;
        }

        /// <summary>   Determine if we can command import to selected. </summary>
        ///
        /// <param name="obj">  The object. </param>
        ///
        /// <returns>   True if we can command import to selected, false if not. </returns>

        private bool CanCmdImportToSelected(object obj)
        {
            return !_importRun;
        }
        #endregion

        /// <summary>
        ///     Führt anwendungsspezifische Aufgaben durch, die mit der Freigabe, der Zurückgabe oder dem
        ///     Zurücksetzen von nicht verwalteten Ressourcen zusammenhängen.
        /// </summary>

        public void Dispose()
        {
            SettingsModel?.Dispose();
            TiaInstancesModel?.Dispose();
            TiaProjectNavigationModel?.Dispose();
        }
        
        #endregion

        #region Override metods

        #endregion

    }
}