﻿// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT_2\TiaInstance\TiaObjectExtensions.cs </file>
///
/// <copyright file="TiaObjectExtensions.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the tia object extensions class. </summary>

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using FHR_TIA_GIT_2.Properties;
using Siemens.Engineering.HW;
using Siemens.Engineering.HW.Features;
using Siemens.Engineering.SW.Blocks;

namespace FHR_TIA_GIT_2.TIA
{
    /// <summary>   A tia object extensions. </summary>
    public static class TiaObjectExtensions
    {
        /// <summary>   Enumerates sort objects in this collection. </summary>
        ///
        /// <param name="list"> The list to act on. </param>
        ///
        /// <returns>
        ///     An enumerator that allows foreach to be used to process sort objects in this collection.
        /// </returns>

        public static IEnumerable<TiaObject> SortObjects(this IReadOnlyCollection<TiaObject> list)
        {
            if (list.ToList().TrueForAll(x => x.ObjType.Equals(nameof(DeviceItem))))
            {
                return new ObservableCollection<TiaObject>(list.OrderBy(o => o.GetAttributeValue<bool>("IsBuiltIn") ? 1 : 0));
            }
            return new ObservableCollection<TiaObject>(list);
        }

        /// <summary>   Enumerates sort PLC blocks in this collection. </summary>
        ///
        /// <param name="list"> The list to act on. </param>
        ///
        /// <returns>
        ///     An enumerator that allows foreach to be used to process sort PLC blocks in this
        ///     collection.
        /// </returns>

        public static IEnumerable<TiaObject> SortPlcBlocks(this IReadOnlyCollection<TiaObject> list)
        {
            ObservableCollection<TiaObject> newList = new ObservableCollection<TiaObject>();
            // Insert all objects are BlockGroups
            list.Where(o => o.ObjType is nameof(PlcBlockSystemGroup) or nameof(PlcBlockUserGroup))
                .ToList().ForEach(x => newList.Add(x));
            // Insert all object are FCs
            list.Where(o => o.ObjType == nameof(FC)).ToList().ForEach(x => newList.Add(x));
            // Insert all object are FBs
            list.Where(o => o.ObjType == nameof(FB)).ToList().ForEach(x => newList.Add(x));
            // Insert all object are DBs
            list.Where(o => o.ObjType is nameof(GlobalDB) or nameof(InstanceDB))
                .ToList().ForEach(x => newList.Add(x));
            // Insert all object are OBs
            list.Where(o => o.ObjType == nameof(OB)).ToList().ForEach(x => newList.Add(x));
            
            return newList;
        }

        /// <summary>   A Device extension method that gets a software. </summary>
        ///
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="device">   The device to act on. </param>
        ///
        /// <returns>   The software. </returns>

        public static T GetSoftware<T>(this Device device)
        {
            if (device == null) throw new ArgumentNullException(nameof(device), Resources.ErrorParameterIsNull);
            
            var itemStack = new Stack<DeviceItem>();
            device.DeviceItems.ToList().ForEach(x => itemStack.Push(x));

            while (itemStack.Any())
            {
                var item = itemStack.Pop();
                var target = item.GetService<SoftwareContainer>();
                if (target is {Software: T software}) return software;
                item.DeviceItems.ToList().ForEach(x => itemStack.Push(x));
            }

            return default(T);
        }

        /// <summary>   A string extension method that parse to enum. </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="value">    The value to act on. </param>
        ///
        /// <returns>   A T. </returns>

        public static T ParseToEnum<T>(this string value)
        {
            return (T) Enum.Parse(typeof(T), value);
        }

        /// <summary>   A string extension method that try deserialize. </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="fileName"> The fileName to act on. </param>
        ///
        /// <returns>   A TiaObject. </returns>

        public static TiaObject TryDeserialize<T>(this string fileName) where T : TiaObject
        {
            TiaObject impObj = null;
            
            // ReSharper disable once ConvertToUsingDeclaration
            using (Stream stream = new FileStream(fileName,FileMode.Open))
            {
                XmlReader xmlReader = new XmlTextReader(stream);

                XmlSerializer xmlSerializer= new XmlSerializer(typeof(T));
                if (xmlSerializer.CanDeserialize(xmlReader))
                {
                    impObj = (T)xmlSerializer.Deserialize(xmlReader);
                }
            }

            return impObj;
        }
    }
}