﻿// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT_2\TiaInstance\TiaPlcSoftware.cs </file>
///
/// <copyright file="TiaPlcSoftware.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the tia PLC software class. </summary>

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Forms;
using FHR_TIA_GIT_2.Properties;
using Siemens.Engineering;
using Siemens.Engineering.HW;
using Siemens.Engineering.SW;
using Siemens.Engineering.SW.Blocks;
using Siemens.Engineering.SW.Tags;
using Siemens.Engineering.SW.Types;
using static FHR_TIA_GIT_2.Utillities.Logger.Log4Net;

namespace FHR_TIA_GIT_2.TIA.TiaObjects
{
    /// <summary>   A tia PLC software. </summary>
    public class TiaPlcSoftware : TiaObject
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public TiaPlcSoftware()
        {
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="obj">      The object. </param>
        /// <param name="parent">   The parent. </param>

        public TiaPlcSoftware(object obj, TiaObject parent) : base(obj, parent)
        {
            this.EvtRefresh += (_, _) => ReadMainComponents();
            this.EvtExport += (_, _) => ExportObject(this);
            this.EvtImport += delegate { ImportObject(); ReadMainComponents(); };
            
            this.ReadMainComponents();
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        #endregion

        #region Properties

        #endregion

        #region Methods

        #endregion

        #region Override metods

        /// <summary>   Import object. </summary>
        public override void ImportObject()
        {
            var ofd = new OpenFileDialog()
            {
                Title = Resources.OpenFileDialogTitleImportFile,
                Filter = Resources.OpenFileFilterXML,
                Multiselect = true,
            };
            
            if (ofd.ShowDialog() != DialogResult.OK) return;
            foreach (string fileName in ofd.FileNames)
            {
                TiaObject impObj = fileName.TryDeserialize<TiaTypeGroup>() ??
                                   fileName.TryDeserialize<TiaType>() ??
                                   fileName.TryDeserialize<TiaTagTableGroup>() ??
                                   fileName.TryDeserialize<TiaTagTable>() ??
                                   fileName.TryDeserialize<TiaTag>() ??
                                   fileName.TryDeserialize<TiaBlockGroup>() ??
                                   fileName.TryDeserialize<TiaBlock>() ?? 
                                   fileName.TryDeserialize<TiaObject>();

                impObj?.CreateObject((IEngineeringObject)this.Obj);
            }
        }

        /// <summary>   Reads main components. </summary>
        public sealed override void ReadMainComponents()
        {
            if (Obj is not PlcSoftware plcSoftware) return;

            Subitems ??= new ObservableCollection<TiaObject>();
            Subitems?.Clear();
            Subitems.Add(new TiaTypeGroup(plcSoftware.TypeGroup, this));
            Subitems.Add(new TiaTagTableGroup(plcSoftware.TagTableGroup, this));
            Subitems.Add(new TiaBlockGroup(plcSoftware.BlockGroup, this));
        }

        /// <summary>   Reads all attributes recursive. </summary>
        ///
        /// <param name="readAttributDoTo"> (Optional) True to synchronize. </param>

        public override void ReadAllAttributesRecursive(EnmReadAttributDoTo readAttributDoTo = EnmReadAttributDoTo.AllSync)
        {
            if (readAttributDoTo.HasFlag(EnmReadAttributDoTo.Software)) 
                this.ReadAllAttributes(readAttributDoTo);
            
            Subitems?.ToList().ForEach(x => x.ReadAllAttributesRecursive(readAttributDoTo));
        }

        /// <summary>   Creates an object. </summary>
        ///
        /// <param name="parent">   The parent. </param>

        public override void CreateObject(IEngineeringObject parent)
        {
            if(parent is not Device device) { Log.Debug("Create Object: Parent is not from type Device"); return; }

            this.Obj = device.GetSoftware<PlcSoftware>();
            if(Obj == null) { Log.Debug("Create Object: Cannot found the PLCSoftware"); return; }

            // Sorting the list to import the data
            var sortedList = new List<TiaObject>();
            sortedList.AddRange(Subitems?.Where(x => x.ObjType == nameof(PlcTypeSystemGroup)) ?? Array.Empty<TiaObject>());
            sortedList.AddRange(Subitems?.Where(x => x.ObjType == nameof(PlcTagTableSystemGroup)) ?? Array.Empty<TiaObject>());
            sortedList.AddRange(Subitems?.Where(x => x.ObjType == nameof(PlcBlockSystemGroup)) ?? Array.Empty<TiaObject>());
            
            sortedList.ToList().ForEach(x => x.CreateObject((IEngineeringObject)this.Obj, (PlcSoftware)this.Obj));
        }

        #endregion
    }
}