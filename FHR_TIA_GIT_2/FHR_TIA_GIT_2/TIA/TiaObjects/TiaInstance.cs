﻿// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT_2\TiaInstance\TiaProject.cs </file>
///
/// <copyright file="TiaProject.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the tia project class. </summary>

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Serialization;
using FHR_TIA_GIT_2.Properties;
using FHR_TIA_GIT_2.Utillities;
using MsgboxModernStyle;
using Siemens.Engineering;
using Siemens.Engineering.HW;
using static FHR_TIA_GIT_2.Utillities.Logger.Log4Net;
using MessageBox = System.Windows.Forms.MessageBox;
using View = Siemens.Engineering.HW.View;

namespace FHR_TIA_GIT_2.TIA.TiaObjects
{
    /// <summary>   (Serializable) a tia instance. </summary>
    [Serializable]
    public class TiaInstance : TiaObject
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public TiaInstance()
        {
            InitCommands();
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="obj">      The object. </param>
        /// <param name="parent">   The parent. </param>

        public TiaInstance(object obj, TiaObject parent) : base(obj, parent)
        {
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="tiaPortalProcess"> The tia portal process. </param>

        public TiaInstance(TiaPortalProcess tiaPortalProcess) : this()
        {
            try
            {
                TiaPortalProcess = tiaPortalProcess;
                TiaPortal = tiaPortalProcess.Attach();
            }
            catch (Exception e)
            {
                Log.FatalFormat("Error by attaching a TiaInstance. Message {0}.", e.Message);
            }
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="tiaPortal">    The tia portal. </param>

        public TiaInstance(TiaPortal tiaPortal) : this()
        {
            TiaPortalProcess = tiaPortal.GetCurrentProcess();
            TiaPortal = tiaPortal;
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="tiaProjectXmlFile">    The tia project XML file. </param>
        /// <param name="actDirectoryPath">     Pathname of the act directory. </param>

        public TiaInstance(string tiaProjectXmlFile, string actDirectoryPath) : this()
        {
            TiaObject impObj = tiaProjectXmlFile.TryDeserialize<TiaProject>();
            if(impObj == null) return;

            this.ProjectName = "GIT: " + impObj.Name;
            impObj.Parent = this;

            Directory.GetDirectories(actDirectoryPath, impObj.Name).ToList().ForEach(x => impObj.ReadGitDirectory(x));

            this.Subitems ??= new ObservableCollection<TiaObject>();
            this.Subitems.Clear();
            this.Parent = null;
            Subitems.Add(impObj);
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        /// <summary>   The tia portal process. </summary>
        private TiaPortalProcess _tiaPortalProcess;
        /// <summary>   The tia portal. </summary>
        private TiaPortal _tiaPortal;
        /// <summary>   The project. </summary>
        private Project _project;

        /// <summary>   The selected object. </summary>
        private ITiaObject _selectedObject;

        /// <summary>   Full pathname of the project file. </summary>
        private string _projectPath;
        /// <summary>   Name of the project. </summary>
        private string _projectName;
        /// <summary>   True to with user interface. </summary>
        private bool _withUi;

        /// <summary>   (Immutable) the lock object. </summary>
        private readonly object _lockObject = new object();
        
        /// <summary>   The project languages. </summary>
        private TiaProjectLanguages _projectLanguages;

        #endregion

        #region Properties

        /// <summary>   Gets or sets the tia portal process. </summary>
        ///
        /// <value> The tia portal process. </value>

        [XmlIgnore]
        public TiaPortalProcess TiaPortalProcess
        {
            get { return _tiaPortalProcess; }
            set { _tiaPortalProcess = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets the tia portal. </summary>
        ///
        /// <value> The tia portal. </value>

        [XmlIgnore]
        public TiaPortal TiaPortal
        {
            get { return _tiaPortal; }
            set
            {
                _tiaPortal = value;
                if (_tiaPortal != null) GetProjectFromTiaPortal();
                RaisePropertyChanged();
            }
        }

        /// <summary>   Gets or sets the project. </summary>
        ///
        /// <value> The project. </value>

        [XmlIgnore]
        public Project Project
        {
            get { return _project; }
            set
            {
                _project = value;
                ReadMainComponents();
                RaisePropertyChanged();
            }
        }

        /// <summary>   Gets or sets the selected object. </summary>
        ///
        /// <value> The selected object. </value>

        [XmlIgnore]
        public ITiaObject SelectedObject
        {
            get { return _selectedObject; }
            set { _selectedObject = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets the full pathname of the project file. </summary>
        ///
        /// <value> The full pathname of the project file. </value>

        public string ProjectPath
        {
            get { return _projectPath; }
            set { _projectPath = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets the name of the project. </summary>
        ///
        /// <value> The name of the project. </value>

        public string ProjectName
        {
            get { return _projectName; }
            set { _projectName = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets a value indicating whether the with user interface. </summary>
        ///
        /// <value> True if with user interface, false if not. </value>

        [XmlIgnore]
        public bool WithUi
        {
            get { return _withUi; }
            set { _withUi = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets the project languages. </summary>
        ///
        /// <value> The project languages. </value>

        [XmlIgnore]
        public TiaProjectLanguages ProjectLanguages
        {
            get { return _projectLanguages; }
            set { _projectLanguages = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets the command selected item changed. </summary>
        ///
        /// <value> The command selected item changed. </value>

        [XmlIgnore]
        public CommandBase CmdSelectedItemChanged { get; set; }

        /// <summary>   Gets or sets the command disconnect. </summary>
        ///
        /// <value> The command disconnect. </value>

        [XmlIgnore]
        public CommandBase CmdDisconnect { get; set; }

        /// <summary>   Gets or sets the command language settings. </summary>
        ///
        /// <value> The command language settings. </value>

        [XmlIgnore]
        public CommandBase CmdLanguageSettings { get; set; }

        /// <summary>   Gets or sets the command import project complete. </summary>
        ///
        /// <value> The command import project complete. </value>

        [XmlIgnore]
        public CommandBase CmdImportProjectComplete { get; set; }
        
        #endregion

        #region Methods

        /// <summary>   Gets project from tia portal. </summary>
        private void GetProjectFromTiaPortal()
        {
            if (TiaPortal?.Projects?.Count != 0)
            {
                Project = TiaPortal?.Projects?.First();
                ProjectPath = TiaPortalProcess?.ProjectPath?.DirectoryName;
                ProjectName = TiaPortalProcess?.ProjectPath?.Name;
                WithUi = TiaPortalProcess?.Mode == TiaPortalMode.WithUserInterface;
            }
            else
            {
                WithUi = TiaPortalProcess?.Mode == TiaPortalMode.WithUserInterface;
                Project = null;
                ProjectPath = string.Empty;
                ProjectName = $"No project [{(WithUi ? "With UI" : "No UI")}] [UID: {TiaPortalProcess?.Id.ToString()}]";
            }
        }

        #region Commands

        /// <summary>   Initializes the commands. </summary>
        private void InitCommands()
        {
            CmdSelectedItemChanged = new CommandBase(OnCmdSelectedItemChanged);
            CmdLanguageSettings = new CommandBase(OnCmdLanguageSettings, CanCmdLanguageSettings);
            CmdImportProjectComplete = new CommandBase(OnCmdImportProjectComplete, CanCmdImportProjectComplete);
        }

        /// <summary>   Executes the 'command selected item changed' action. </summary>
        ///
        /// <param name="obj">  The object. </param>

        private void OnCmdSelectedItemChanged(object obj)
        {
            if(obj is not TiaObject tiaObject)return;
            SelectedObject = tiaObject;
            SelectedObject.ReadAllAttributes();
        }

        /// <summary>   Executes the 'command language settings' action. </summary>
        ///
        /// <param name="obj">  The object. </param>

        private void OnCmdLanguageSettings(object obj)
        {
            ProjectLanguages = new TiaProjectLanguages(Project);
            var languageWindow = new ViewsAndModels.Views.ProjectLanguagesSettingsWindow(ProjectLanguages);
            languageWindow.Closed += delegate { ProjectLanguages.Dispose(); };
            languageWindow.Show();
        }

        /// <summary>   Determine if we can command language settings. </summary>
        ///
        /// <param name="obj">  The object. </param>
        ///
        /// <returns>   True if we can command language settings, false if not. </returns>

        private bool CanCmdLanguageSettings(object obj)
        {
            return Project != null;
        }

        /// <summary>   Executes the 'command import project complete' action. </summary>
        ///
        /// <param name="obj">  The object. </param>

        private void OnCmdImportProjectComplete(object obj)
        {
            var ofd = new OpenFileDialog()
            {
                // ReSharper disable once LocalizableElement
                Title = "Select the Project-file",
                Filter = Resources.OpenFileFilterXML,
                Multiselect = false,
            };
            
            if (ofd.ShowDialog() != DialogResult.OK) return;

            TiaObject impObj = ofd.FileName.TryDeserialize<TiaProject>();
            if(impObj == null) return;

            if (impObj.ObjType == nameof(Project) && Project != null)
            {
                const string messageText = "Current data in project are overwritten if the respective CPU already exists. Do you want to continue?";
                
                Log.Warn(messageText);
                var result = MessageBox.Show(messageText, @"Input prompt", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.No) return;
            }

            using (TiaPortal?.ExclusiveAccess($"Import XML file ({ofd.FileName})"))
            {
                impObj.CreateObject(_project);
            }

            ofd.Dispose();
        }

        /// <summary>   Determine if we can command import project complete. </summary>
        ///
        /// <param name="obj">  The object. </param>
        ///
        /// <returns>   True if we can command import project complete, false if not. </returns>

        private bool CanCmdImportProjectComplete(object obj)
        {
            return true;
        }

        #endregion

        #region Events

        #endregion
        
        #endregion

        #region Override metods

        /// <summary>   Reads main components. </summary>
        public override void ReadMainComponents()
        {
            if (Project == null) return;

            using (_tiaPortal.ExclusiveAccess("Reading Project."))
            {
                this.Subitems ??= new ObservableCollection<TiaObject>();
                this.Subitems.Clear();
                this.Parent = null;

                var project = new TiaProject(_project, this);
                Subitems.Add(project);
            }
        }

        /// <summary>
        ///     Führt anwendungsspezifische Aufgaben durch, die mit der Freigabe, der Zurückgabe oder dem
        ///     Zurücksetzen von nicht verwalteten Ressourcen zusammenhängen.
        /// </summary>

        public override void Dispose()
        {
            base.Dispose();
            
            Log.InfoFormat("Succesful disconnected from the TIA Portal instance {0}", TiaPortalProcess?.Id);
            _tiaPortal?.Dispose();
            
            // If TIAPortal without UI => Close the TIAPortal
            // ReSharper disable once InvertIf
            if(!WithUi)
            {
                Log.InfoFormat("TIA Portal instance ({0}) closed, while without UI.", TiaPortalProcess?.Id);
                TiaPortalProcess?.Dispose();
            }
        }

        #endregion
    }

    /// <summary>   (Serializable) a tia project. </summary>
    [Serializable]
    public class TiaProject : TiaObject
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public TiaProject()
        {
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="obj">      The object. </param>
        /// <param name="parent">   The parent. </param>

        public TiaProject(object obj, TiaObject parent) : base(obj, parent)
        {
            this.EvtRefresh += (_, _) => ReadMainComponents();
            this.EvtExport += (_, _) => ExportObject(this);
            this.EvtImport += delegate { ImportObject(); ReadMainComponents(); };
            this.EvtCreate += (_, _) => CreateObject();
            
            this.ReadMainComponents();
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        #endregion

        #region Properties

        #endregion

        #region Methods

        #endregion

        #region Override metods

        /// <summary>   Import object. </summary>
        public override void ImportObject()
        {
            var ofd = new OpenFileDialog()
            {
                Title = Resources.OpenFileDialogTitleImportFile,
                Filter = Resources.OpenFileFilterXML,
                Multiselect = true,
            };
            
            if (ofd.ShowDialog() != DialogResult.OK) return;
            foreach (string fileName in ofd.FileNames)
            {
                TiaObject impObj = fileName.TryDeserialize<TiaDevice>() ?? 
                                   fileName.TryDeserialize<TiaDeviceGroup>() ?? 
                                   fileName.TryDeserialize<TiaObject>();
                
                impObj?.CreateObject((IEngineeringObject)this.Obj);
            }
        }

        /// <summary>   Creates the object. </summary>
        public override void CreateObject()
        {
            if (this.Obj is not Project project) return;

            var messageText = $"Do you want to create a new Device or DeviceGroup for Project {this.Name}?";
            var msgWhatDo = MsgBox.ComboboxSelection(messageText, Resources.CreateQuestion,
                MsgboxIcons.Question, new List<string>() {nameof(Device), nameof(DeviceItem)});
            
            if (msgWhatDo.Button != MsgboxButton.Ok) return;

            switch (msgWhatDo.ComboboxText)
            {
                case nameof(Device):
                    messageText = "Please enter the name of the DeviceGroup!";
                    var msgName = MsgBox.TextInput(messageText, "Enter name", MsgboxIcons.Question, "Name");
                    
                    if (msgName.Button != MsgboxButton.Ok) return;
                    if (string.IsNullOrEmpty(msgName.EnteredText) || msgName.EnteredText == "Name") return;
                    
                    if (project.DeviceGroups.Find(msgName.EnteredText) == null)
                    {
                        project.DeviceGroups.Create(msgName.EnteredText);
                        this.Parent?.ReadMainComponents();
                    }
                    break;
                case nameof(DeviceItem):
                    messageText = "Function is only supported in the hardware editor of the TIA Portal. Open it now?";
                    var msgEditor = MsgBox.Question(messageText, "Create");
                    
                    if (msgEditor.Button != MsgboxButton.Yes) return;
                    project.ShowHwEditor(View.Network);
                    
                    break;
            }
        }

        /// <summary>   Reads main components. </summary>
        public sealed override void ReadMainComponents()
        {
            if (this.Obj is not Project project) return;
            
            this.Subitems ??= new ObservableCollection<TiaObject>();
            this.Subitems?.Clear();
            project.Devices.ToList().ForEach(x => this.Subitems.Add(new TiaDevice(x, this)));
            project.DeviceGroups.ToList().ForEach(x => this.Subitems.Add(new TiaDeviceGroup(x,this)));
            this.Subitems.Add(new TiaDeviceGroup(project.UngroupedDevicesGroup, this));
        }

        /// <summary>   Reads all attributes recursive. </summary>
        ///
        /// <param name="readAttributDoTo"> (Optional) True to synchronize. </param>

        public override void ReadAllAttributesRecursive(EnmReadAttributDoTo readAttributDoTo = EnmReadAttributDoTo.AllSync)
        {
            this.ReadAllAttributes(readAttributDoTo);
            Subitems?.ToList().ForEach(x => x.ReadAllAttributesRecursive(readAttributDoTo));
        }

        #endregion
    }
}