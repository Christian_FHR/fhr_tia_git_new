﻿// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT_2\TiaInstance\TiaSubClasses.cs </file>
///
/// <copyright file="TiaSubClasses.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the tia sub classes class. </summary>

using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Xml.Serialization;
using Siemens.Engineering;
using Siemens.Engineering.HW;
using Siemens.Engineering.HW.Features;

namespace FHR_TIA_GIT_2.TIA.TiaObjects
{
    /// <summary>   (Serializable) a tia address. </summary>
    [Serializable]
    public class TiaAddress : TiaObject
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public TiaAddress()
        {
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="obj">      The object. </param>
        /// <param name="parent">   The parent. </param>

        public TiaAddress(object obj, TiaObject parent) : base(obj, parent)
        {
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        #endregion

        #region Properties

        #endregion

        #region Methods

        #endregion

        #region Override metods

        /// <summary>   Reads all attributes recursive. </summary>
        ///
        /// <param name="readAttributDoTo"> (Optional) True to synchronize. </param>

        public override void ReadAllAttributesRecursive(EnmReadAttributDoTo readAttributDoTo = EnmReadAttributDoTo.AllSync)
        {
            if (readAttributDoTo.HasFlag(EnmReadAttributDoTo.Hardware)) this.ReadAllAttributes(readAttributDoTo);
        }
        
        #endregion
    }
    
    /// <summary>   (Serializable) a tia channel. </summary>
    [Serializable]
    public class TiaChannel : TiaObject
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public TiaChannel()
        {
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="obj">      The object. </param>
        /// <param name="parent">   The parent. </param>

        public TiaChannel(object obj, TiaObject parent) : base(obj, parent)
        {
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        #endregion

        #region Properties

        #endregion

        #region Methods

        #endregion

        #region Override metods

        /// <summary>   Reads all attributes recursive. </summary>
        ///
        /// <param name="readAttributDoTo"> (Optional) True to synchronize. </param>

        public override void ReadAllAttributesRecursive(EnmReadAttributDoTo readAttributDoTo = EnmReadAttributDoTo.AllSync)
        {
            if (readAttributDoTo.HasFlag(EnmReadAttributDoTo.Hardware)) this.ReadAllAttributes(readAttributDoTo);
        }

        #endregion
    }

    /// <summary>   (Serializable) a tia network interface. </summary>
    [Serializable]
    public class TiaNetworkInterface : TiaObject
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public TiaNetworkInterface()
        {
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="obj">      The object. </param>
        /// <param name="parent">   The parent. </param>

        public TiaNetworkInterface(object obj, TiaObject parent) : base(obj, parent)
        {
            if (Obj is not NetworkInterface networkInterface) return;
            if (networkInterface.Nodes.Count >= 1)
                Node = new TiaNode(networkInterface.Nodes.First(), this);
            if (networkInterface.IoControllers.Count >= 1)
                IoController = new TiaIoController(networkInterface.IoControllers.First(),this);
            if (networkInterface.IoConnectors.Count >= 1)
                IoConnector = new TiaIoConnector(networkInterface.IoConnectors.First(),this);
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        /// <summary>   The node. </summary>
        private TiaNode _node;
        /// <summary>   The i/o controller. </summary>
        private TiaIoController _ioController;
        /// <summary>   The i/o connector. </summary>
        private TiaIoConnector _ioConnector;

        #endregion

        #region Properties

        /// <summary>   Gets or sets the node. </summary>
        ///
        /// <value> The node. </value>

        [XmlElement]
        public TiaNode Node
        {
            get { return _node; }
            set { _node = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets the i/o controller. </summary>
        ///
        /// <value> The i/o controller. </value>

        [XmlElement]
        public TiaIoController IoController
        {
            get { return _ioController; }
            set { _ioController = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets the i/o connector. </summary>
        ///
        /// <value> The i/o connector. </value>

        [XmlElement]
        public TiaIoConnector IoConnector
        {
            get { return _ioConnector; }
            set { _ioConnector = value; RaisePropertyChanged(); }
        }

        #endregion

        #region Methods

        #endregion

        #region Override metods

        /// <summary>   Creates an object. </summary>
        ///
        /// <param name="parent">   The parent. </param>

        public override void CreateObject(IEngineeringObject parent)
        {
            if (parent is not DeviceItem deviceItem) return;
            var netInterface = deviceItem.GetService<NetworkInterface>();
            if (netInterface == null) return;

            this.Obj = netInterface;
            this.SetAllAttributes();
            
            this.Node?.CreateObject((IEngineeringObject)this.Obj);
            this.IoController?.CreateObject((IEngineeringObject)this.Obj);
            this.IoConnector?.CreateObject((IEngineeringObject)this.Obj);
        }

        /// <summary>   Reads all attributes recursive. </summary>
        ///
        /// <param name="readAttributDoTo"> (Optional) True to synchronize. </param>

        public override void ReadAllAttributesRecursive(EnmReadAttributDoTo readAttributDoTo = EnmReadAttributDoTo.AllSync)
        {
            if (readAttributDoTo.HasFlag(EnmReadAttributDoTo.Hardware))
            {
                this.ReadAllAttributes(readAttributDoTo);
                Node?.ReadAllAttributesRecursive(readAttributDoTo);
                IoController?.ReadAllAttributesRecursive(readAttributDoTo);
                IoConnector?.ReadAllAttributesRecursive(readAttributDoTo);
            }
        }

        #endregion
    }

    /// <summary>   (Serializable) a tia node. </summary>
    [Serializable]
    public class TiaNode : TiaObject
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public TiaNode()
        {
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="obj">      The object. </param>
        /// <param name="parent">   The parent. </param>

        public TiaNode(object obj, TiaObject parent) : base(obj, parent)
        {
            if (Obj is not Node node) return;
            if (node.ConnectedSubnet != null) Subnet = new TiaSubnet(node.ConnectedSubnet, this);
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        /// <summary>   The subnet. </summary>
        private TiaSubnet _subnet;

        #endregion

        #region Properties

        /// <summary>   Gets or sets the subnet. </summary>
        ///
        /// <value> The subnet. </value>

        [XmlElement]
        public TiaSubnet Subnet
        {
            get { return _subnet; }
            set { _subnet = value; RaisePropertyChanged(); }
        }

        #endregion

        #region Methods

        #endregion

        #region Override metods

        /// <summary>   Creates an object. </summary>
        ///
        /// <param name="parent">   The parent. </param>

        public override void CreateObject(IEngineeringObject parent)
        {
            if (parent is not NetworkInterface networkInterface) return;
            this.Obj = networkInterface.Nodes.Find(this.Name);
            this.SetAllAttributes();

            this.Subnet?.CreateObject((IEngineeringObject)this.Obj);
        }

        /// <summary>   Reads all attributes recursive. </summary>
        ///
        /// <param name="readAttributDoTo"> (Optional) True to synchronize. </param>

        public override void ReadAllAttributesRecursive(EnmReadAttributDoTo readAttributDoTo = EnmReadAttributDoTo.AllSync)
        {
            if (readAttributDoTo.HasFlag(EnmReadAttributDoTo.Hardware))
            {
                this.ReadAllAttributes(readAttributDoTo);
                Subnet?.ReadAllAttributesRecursive(readAttributDoTo);
            }
        }

        #endregion
    }

    /// <summary>   (Serializable) a tia subnet. </summary>
    [Serializable]
    public class TiaSubnet : TiaObject
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public TiaSubnet()
        {
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="obj">      The object. </param>
        /// <param name="parent">   The parent. </param>

        public TiaSubnet(object obj, TiaObject parent) : base(obj, parent)
        {
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        #endregion

        #region Properties

        #endregion

        #region Methods

        #endregion

        #region Override metods

        /// <summary>   Creates an object. </summary>
        ///
        /// <param name="parent">   The parent. </param>

        public override void CreateObject(IEngineeringObject parent)
        {
            if (parent is not Node node) return;
            if (node.ConnectedSubnet == null)
            {
                this.Obj = this.GetTypeFromObjectParent<Project>(parent).Subnets.Find(this.Name);
                if(this.Obj == null)
                    this.Obj = node.CreateAndConnectToSubnet(this.Name);
                else
                    node.ConnectToSubnet((Subnet)this.Obj);
            }

            this.SetAllAttributes();
        }

        /// <summary>   Reads all attributes recursive. </summary>
        ///
        /// <param name="readAttributDoTo"> (Optional) True to synchronize. </param>

        public override void ReadAllAttributesRecursive(EnmReadAttributDoTo readAttributDoTo = EnmReadAttributDoTo.AllSync)
        {
            if (readAttributDoTo.HasFlag(EnmReadAttributDoTo.Hardware)) this.ReadAllAttributes(readAttributDoTo);
        }

        #endregion
    }

    /// <summary>   (Serializable) a controller for handling tia i/oes. </summary>
    [Serializable]
    public class TiaIoController : TiaObject
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public TiaIoController()
        {
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="obj">      The object. </param>
        /// <param name="parent">   The parent. </param>

        public TiaIoController(object obj, TiaObject parent) : base(obj, parent)
        {
            if (Obj is not IoController ioController) return;
            if (ioController.Addresses.Count > 0)
            {
                Addresses ??= new ObservableCollection<TiaAddress>();
                ioController.Addresses.ToList().ForEach(x => this.Addresses.Add(new TiaAddress(x, this)));
            }
            if (ioController.IoSystem != null) IoSystem = new TiaIoSystem(ioController.IoSystem, this);
        }

        #endregion

        #region Types

        #endregion

        #region Fields
        
        /// <summary>   The i/o system. </summary>
        private TiaIoSystem _ioSystem;
        /// <summary>   The addresses. </summary>
        private ObservableCollection<TiaAddress> _addresses;
        
        #endregion

        #region Properties

        /// <summary>   Gets or sets the i/o system. </summary>
        ///
        /// <value> The i/o system. </value>

        [XmlElement]
        public TiaIoSystem IoSystem
        {
            get { return _ioSystem; }
            set { _ioSystem = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets the addresses. </summary>
        ///
        /// <value> The addresses. </value>

        [XmlArray("Addresses")]
        [XmlArrayItem("Address")]
        public ObservableCollection<TiaAddress> Addresses
        {
            get { return _addresses; }
            set { _addresses = value; RaisePropertyChanged(); }
        }

        #endregion

        #region Methods

        #endregion

        #region Override metods

        /// <summary>   Creates an object. </summary>
        ///
        /// <param name="parent">   The parent. </param>

        public override void CreateObject(IEngineeringObject parent)
        {
            if (parent is not NetworkInterface networkInterface) return;
            if (networkInterface.IoControllers?.Count >= 1)
                this.Obj = networkInterface.IoControllers.First();
            
            if (this.Obj == null) return;
            this.SetAllAttributes();
            
            this.IoSystem?.CreateObject((IEngineeringObject)this.Obj);
            
            if (this.Addresses.Count >= 0)
            {
                for (var i = 0; i < this.Addresses.Count; i++)
                {
                    this.Addresses[i].Obj = ((IoController) this.Obj).Addresses[i];
                    this.Addresses[i].SetAllAttributes();
                }
            }
        }

        /// <summary>   Reads all attributes recursive. </summary>
        ///
        /// <param name="readAttributDoTo"> (Optional) True to synchronize. </param>

        public override void ReadAllAttributesRecursive(EnmReadAttributDoTo readAttributDoTo = EnmReadAttributDoTo.AllSync)
        {
            if (readAttributDoTo.HasFlag(EnmReadAttributDoTo.Hardware))
            {
                this.ReadAllAttributes(readAttributDoTo);
                if (Addresses?.Count > 0) Addresses.ToList().
                    ForEach(x => x.ReadAllAttributesRecursive(readAttributDoTo));
                IoSystem?.ReadAllAttributesRecursive(readAttributDoTo);
            }
        }
        
        #endregion
    }

    /// <summary>   (Serializable) a tia i/o system. </summary>
    [Serializable]
    public class TiaIoSystem : TiaObject
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public TiaIoSystem()
        {
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="obj">      The object. </param>
        /// <param name="parent">   The parent. </param>

        public TiaIoSystem(object obj, TiaObject parent) : base(obj, parent)
        {
            if (Obj is not IoSystem ioSystem) return;
            if (ioSystem.Subnet == null) return;
            SubnetName = ioSystem.Subnet.Name;
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        /// <summary>   Name of the subnet. </summary>
        private string _subnetName;
        
        #endregion

        #region Properties

        /// <summary>   Gets or sets the name of the subnet. </summary>
        ///
        /// <value> The name of the subnet. </value>

        [XmlAttribute]
        public string SubnetName
        {
            get { return _subnetName; }
            set { _subnetName = value; RaisePropertyChanged(); }
        }

        #endregion

        #region Methods

        #endregion

        #region Override metods

        /// <summary>   Creates an object. </summary>
        ///
        /// <param name="parent">   The parent. </param>

        public override void CreateObject(IEngineeringObject parent)
        {
            if (parent is not IoController ioController) return;
            this.Obj = ioController.CreateIoSystem(this.Name);
            this.SetAllAttributes();
        }

        /// <summary>   Reads all attributes recursive. </summary>
        ///
        /// <param name="readAttributDoTo"> (Optional) True to synchronize. </param>

        public override void ReadAllAttributesRecursive(EnmReadAttributDoTo readAttributDoTo = EnmReadAttributDoTo.AllSync)
        {
            if (readAttributDoTo.HasFlag(EnmReadAttributDoTo.Hardware)) this.ReadAllAttributes(readAttributDoTo);
        }
        
        #endregion
    }

    /// <summary>   (Serializable) a tia i/o connector. </summary>
    [Serializable]
    public class TiaIoConnector : TiaObject
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public TiaIoConnector()
        {
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="obj">      The object. </param>
        /// <param name="parent">   The parent. </param>

        public TiaIoConnector(object obj, TiaObject parent) : base(obj, parent)
        {
            if (Obj is not IoConnector ioConnector) return;
            if (ioConnector.ConnectedToIoSystem != null)
                IoSystem = new TiaIoSystem(ioConnector.ConnectedToIoSystem, this);
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        /// <summary>   The i/o system. </summary>
        private TiaIoSystem _ioSystem;
        #endregion

        #region Properties

        /// <summary>   Gets or sets the i/o system. </summary>
        ///
        /// <value> The i/o system. </value>

        [XmlElement]
        public TiaIoSystem IoSystem
        {
            get { return _ioSystem; }
            set { _ioSystem = value; RaisePropertyChanged(); }
        }

        #endregion

        #region Methods

        #endregion

        #region Override metods

        /// <summary>   Creates an object. </summary>
        ///
        /// <param name="parent">   The parent. </param>

        public override void CreateObject(IEngineeringObject parent)
        {
            if (parent is not NetworkInterface networkInterface) return;
            if (networkInterface.IoConnectors?.Count == 0) return;

            var connector = networkInterface.IoConnectors?.First();
            this.Obj = connector;
            
            if (connector?.ConnectedToIoSystem != null) 
                this.IoSystem.Obj = connector.ConnectedToIoSystem;
            else
            {
                var subnet = this.GetTypeFromObjectParent<Project>(networkInterface).Subnets.Find(this.IoSystem.SubnetName);
                if (!(subnet?.IoSystems?.Count >= 1)) return;
                
                this.IoSystem.Obj = subnet.IoSystems.ToList().Find(x => x.Name == this.IoSystem.Name);
                if (this.IoSystem.Obj != null) connector?.ConnectToIoSystem((IoSystem)this.IoSystem.Obj);
            }
        }

        /// <summary>   Reads all attributes recursive. </summary>
        ///
        /// <param name="readAttributDoTo"> (Optional) True to synchronize. </param>

        public override void ReadAllAttributesRecursive(EnmReadAttributDoTo readAttributDoTo = EnmReadAttributDoTo.AllSync)
        {
            if (readAttributDoTo.HasFlag(EnmReadAttributDoTo.Hardware))
            {
                this.ReadAllAttributes(readAttributDoTo);
                IoSystem?.ReadAllAttributesRecursive(readAttributDoTo);
            }
        }

        #endregion
    }
}