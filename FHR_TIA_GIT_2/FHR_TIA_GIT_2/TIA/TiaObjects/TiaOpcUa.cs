﻿// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT_2\TiaInstance\TiaOpcUa.cs </file>
///
/// <copyright file="TiaOpcUa.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the tia opc UA class. </summary>

using Siemens.Engineering;
using Siemens.Engineering.HW;
using Siemens.Engineering.HW.Features;

namespace FHR_TIA_GIT_2.TIA.TiaObjects
{
    /// <summary>   A tia opc ua. </summary>
    public class TiaOpcUa : TiaObject
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public TiaOpcUa()
        {
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="obj">      The object. </param>
        /// <param name="parent">   The parent. </param>

        public TiaOpcUa(object obj, TiaObject parent) : base(obj, parent)
        {
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        #endregion

        #region Properties

        #endregion

        #region Methods

        #endregion

        #region Override metods

        /// <summary>   Reads all attributes recursive. </summary>
        ///
        /// <param name="readAttributDoTo"> (Optional) True to synchronize. </param>

        public override void ReadAllAttributesRecursive(EnmReadAttributDoTo readAttributDoTo = EnmReadAttributDoTo.AllSync)
        {
            if (readAttributDoTo.HasFlag(EnmReadAttributDoTo.Hardware)) this.ReadAllAttributes(readAttributDoTo);
            
            // TODO: Export as *.cer works
            // var certAttribute = this.Attributes?.ToList().Find(x => x.Name == "OpcUaServerCertificate");
            // if (certAttribute == null) return;
            //
            // Certificate certificate = this.GetAttributeValue<Certificate>(certAttribute.Name, true);
            //
            // string path = @"C:\FHR";
            // if (Settings.Default.UseProjectPathAsGitPath) path = this.GetTypeFromObjectParent<Project>().Path.Directory + "\\GIT";
            // else path = Settings.Default.LibraryGitPath + "\\" + this.GetTypeFromObjectParent<Project>().Name;
            //
            // string[] certName = certificate.SubjectCommonName.Split('/');
            //
            // path += "\\" + certName[0];
            // if (!Directory.Exists(path)) Directory.CreateDirectory(path);
            // string fileName = path + "\\" + certName[1] + FileExtensionCertificate;
            //
            // certificate.Export(new FileInfo(fileName), CertificateExportFormat.Cer);
        }

        /// <summary>   Creates an object. </summary>
        ///
        /// <param name="parent">   The parent. </param>

        public override void CreateObject(IEngineeringObject parent)
        {
            if (parent is not DeviceItem deviceItem) return;
            var opcUaUserManagement = deviceItem.GetService<OpcUaUserManagement>();
            if (opcUaUserManagement == null) return;

            this.Obj = opcUaUserManagement;

            // TODO: Import of Certification after the PLC is unprotected => Import successfully but do not see the certification in the Project
            // var ofd = new OpenFileDialog()
            // {
            //     Title = "Select the OPC UA certificate",
            //     Multiselect = false,
            //     Filter = "Certificate (*.cer)|*.cer"
            // };
            //
            // if (ofd.ShowDialog() == DialogResult.OK)
            // {
            //     var certAttribute = this.Attributes.ToList().Find(x => x.Name == "OpcUaServerCertificate");
            //     if (certAttribute != null)
            //     {
            //         var masterSecretConfig =
            //             this.GetServiceFromObjectParentWithService<PlcMasterSecretConfigurator>(parent);
            //         if (masterSecretConfig != null)
            //         {
            //             masterSecretConfig.Unprotect();
            //             var localCertManager = this.GetServiceFromObjectParentWithService<LocalCertificateManager>(parent);
            //             if (localCertManager != null)
            //             {
            //                 certAttribute.Value =
            //                     localCertManager.LocalCertificateStore.Certificates.Import(new FileInfo(ofd.FileName));
            //             }
            //         }
            //     }
            // }
            
            this.SetAllAttributes();
        }

        #endregion
    }
}