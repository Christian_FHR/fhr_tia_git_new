﻿// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT_2\TiaInstance\TiaDevice.cs </file>
///
/// <copyright file="TiaDevice.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the tia device class. </summary>

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Serialization;
using FHR_TIA_GIT_2.Properties;
using MsgboxModernStyle;
using Siemens.Engineering;
using Siemens.Engineering.HW;
using Siemens.Engineering.HW.Features;
using Siemens.Engineering.Safety;
using Siemens.Engineering.SW;
using static FHR_TIA_GIT_2.Utillities.Logger.Log4Net;
// ReSharper disable All

namespace FHR_TIA_GIT_2.TIA.TiaObjects
{
    /// <summary>   (Serializable) a tia device. </summary>
    [Serializable]
    public class TiaDevice : TiaObject
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public TiaDevice()
        {
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="obj">      The object. </param>
        /// <param name="parent">   The parent. </param>

        public TiaDevice(object obj, TiaObject parent) : base(obj, parent)
        {
            this.EvtRefresh += (_, _) => this.ReadMainComponents();
            this.EvtExport += (_, _) => this.ExportObject(this);
            this.EvtImport += delegate { ImportObject(); ReadMainComponents(); };
            this.EvtDelete += (_, _) => DeleteObject();
            this.EvtCreate += (_, _) => CreateObject();
            
            this.ReadMainComponents();
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        #endregion

        #region Properties

        #endregion

        #region Methods

        /// <summary>   Gets a software. </summary>
        ///
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        ///
        /// <param name="device">   The device. </param>
        ///
        /// <returns>   The software. </returns>

        private PlcSoftware GetSoftware(Device device)
        {
            if (device == null) throw new ArgumentNullException(nameof(device), Resources.ErrorParameterIsNull);
            
            var itemStack = new Stack<DeviceItem>();
            device.DeviceItems.ToList().ForEach(x => itemStack.Push(x));

            while (itemStack.Any())
            {
                var item = itemStack.Pop();
                var target = item.GetService<SoftwareContainer>();
                if (target is {Software: PlcSoftware software}) return software;
                item.DeviceItems.ToList().ForEach(x => itemStack.Push(x));
            }

            return null;
        }
        
        #endregion

        #region Override metods

        /// <summary>   Import object. </summary>
        public override void ImportObject()
        {
            var ofd = new OpenFileDialog()
            {
                Title = Resources.OpenFileDialogTitleImportFile,
                Filter = Resources.OpenFileFilterXML,
                Multiselect = true,
            };
            
            if (ofd.ShowDialog() != DialogResult.OK) return;
            foreach (var fileName in ofd.FileNames)
            {
                TiaObject impObj = fileName.TryDeserialize<TiaDeviceItem>() ?? 
                                   fileName.TryDeserialize<TiaPlcSoftware>() ?? 
                                   fileName.TryDeserialize<TiaObject>();
                
                impObj?.CreateObject((IEngineeringObject)this.Obj);
            }
        }
        
        /// <summary>   Deletes the object. </summary>
        public override void DeleteObject()
        {
            if (Obj is not Device device) return;

            var messageText = string.Format(Resources.DeleteDeviceQuestion, this.Name);
            if (MsgBox.Question(messageText, Resources.DeleteQuestion).Button != MsgboxButton.Yes) return;
            device.Delete();
            
            Log.InfoFormat("Device {0} was deleted.", this.Name);
            this.Parent?.CmdRefresh.Execute(null);
        }

        /// <summary>   Creates an object. </summary>
        public override void CreateObject()
        {
            if (Obj is not Device device) return;
            
            var messageText = string.Format(Resources.CreateDeviceitemQuestion, this.Name);
            if (MessageBox.Show(messageText, Resources.CreateQuestion, MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;

            messageText = Resources.FunctionOnlySupportedTiaHWEditorOpenQuestion;
            if (MessageBox.Show(messageText, Resources.CreateQuestion, MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;
            
            device.ShowInEditor(Siemens.Engineering.HW.View.Device);
        }

        /// <summary>   Reads main components. </summary>
        public sealed override void ReadMainComponents()
        {
            if(Obj is not Device device) return;
            var software = GetSoftware(device);

            if (device.Items.Count > 0 || software != null)
            {
                this.Subitems ??= new ObservableCollection<TiaObject>();
                this.Subitems?.Clear();
            }
            
            if (device.Items.Count > 0) device.Items.ToList().ForEach(x => Subitems.Add(new TiaDeviceItem(x, this)));
            if (software != null) Subitems.Add(new TiaPlcSoftware(software, this));
            
            if (device.Items.Count == 0 && software == null)
            {
                Subitems?.ToList().ForEach(x => x.Dispose());
                Subitems?.Clear();
                Subitems = null;
            }
        }

        /// <summary>   Reads all attributes recursive. </summary>
        ///
        /// <param name="readAttributDoTo"> (Optional) True to synchronize. </param>

        public override void ReadAllAttributesRecursive(EnmReadAttributDoTo readAttributDoTo = EnmReadAttributDoTo.AllSync)
        {
            if (readAttributDoTo.HasFlag(EnmReadAttributDoTo.Hardware)) this.ReadAllAttributes(readAttributDoTo);

            Subitems?.ToList().ForEach(x => x.ReadAllAttributesRecursive(readAttributDoTo));
        }

        /// <summary>   Creates an object. </summary>
        ///
        /// <param name="parent">   The parent. </param>

        public override void CreateObject(IEngineeringObject parent)
        {
            DeviceComposition devices;
            if(parent is Project project) devices = project.Devices;
            else if (parent is DeviceGroup deviceGroup) devices = deviceGroup.Devices;
            else { Log.Debug("Create Object: Parent is not from type Project/DeviceSystemGroup"); return; }
            
            Log.DebugFormat("{0}{0}Creating new Device (Name: {1})", Environment.NewLine, this.Name);
            this.Obj = devices?.Create(this.GetAttributeValue<string>("TypeIdentifier"), this.Name);
            this.SetAllAttributes();
            
            this.Subitems?.SortObjects().ToList().ForEach(x => x.CreateObject((IEngineeringObject)this.Obj));
        }

        #endregion
    }

    /// <summary>   (Serializable) a tia device item. </summary>
    [Serializable]
    public class TiaDeviceItem : TiaObject
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public TiaDeviceItem()
        {
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="obj">      The object. </param>
        /// <param name="parent">   The parent. </param>

        public TiaDeviceItem(object obj, TiaObject parent) : base(obj, parent)
        {
            this.EvtRefresh += (_, _) => ReadMainComponents();
            this.EvtExport += (_, _) => ExportObject(this);
            
            this.ReadMainComponents();
        }

        #endregion

        #region Types

        #endregion

        #region Fields
        /// <summary>   The network interface. </summary>
        private TiaNetworkInterface _networkInterface;
        /// <summary>   The addresses. </summary>
        private ObservableCollection<TiaAddress> _addresses;
        /// <summary>   The channels. </summary>
        private ObservableCollection<TiaChannel> _channels;
        /// <summary>   The opc UA. </summary>
        private TiaOpcUa _opcUa;
        
        #endregion

        #region Properties

        /// <summary>   Gets or sets the network interface. </summary>
        ///
        /// <value> The network interface. </value>

        [XmlElement]
        public TiaNetworkInterface NetworkInterface
        {
            get { return _networkInterface; }
            set { _networkInterface = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets the opc UA. </summary>
        ///
        /// <value> The opc UA. </value>

        [XmlElement]
        public TiaOpcUa OpcUa
        {
            get { return _opcUa; }
            set { _opcUa = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets the addresses. </summary>
        ///
        /// <value> The addresses. </value>

        [XmlArray("Addresses")]
        [XmlArrayItem("Address")]
        public ObservableCollection<TiaAddress> Addresses
        {
            get { return _addresses; }
            set { _addresses = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets the channels. </summary>
        ///
        /// <value> The channels. </value>

        [XmlArray("Channels")]
        [XmlArrayItem("Channel")]
        public ObservableCollection<TiaChannel> Channels
        {
            get { return _channels; }
            set { _channels = value; RaisePropertyChanged(); }
        }

        #endregion

        #region Methods

        #endregion

        #region Override metods

        /// <summary>   Deletes the object. </summary>
        public override void DeleteObject()
        {
            if (Obj is not DeviceItem deviceItem) return;

            var messageText = string.Format(Resources.DeleteDeviceitemQuestion, this.Name);
            if (MsgBox.Question(messageText, Resources.DeleteQuestion).Button != MsgboxButton.Yes) return;
            deviceItem.Delete();
            
            Log.InfoFormat("DeviceItem {0} was deleted.", this.Name);
            this.Parent?.CmdRefresh.Execute(null);
        }
        
        /// <summary>   Reads main components. </summary>
        public sealed override void ReadMainComponents()
        {
            if (Obj is not DeviceItem deviceItem) return;

            if (deviceItem.Items.Count > 0)
            {
                Subitems ??= new ObservableCollection<TiaObject>();
                Subitems?.Clear();
                deviceItem.Items.ToList().ForEach(x => Subitems.Add(new TiaDeviceItem(x, this)));
            }
            
            var safety = deviceItem.GetService<SafetyAdministration>();
            if (safety is null) return;

        }

        /// <summary>   Reads additional components. </summary>
        public override void ReadAdditionalComponents()
        {
            if (Obj is not DeviceItem deviceItem) return;

            if (deviceItem.Addresses is {Count: > 0})
            {
                Addresses ??= new ObservableCollection<TiaAddress>();
                deviceItem.Addresses.ToList().ForEach(x => Addresses.Add(new TiaAddress(x, this)));
            }
            
            if (deviceItem.Channels is {Count: > 0})
            {
                Channels ??= new ObservableCollection<TiaChannel>();
                deviceItem.Channels.ToList().ForEach(x => Channels.Add(new TiaChannel(x, this)));
            }

            var netInterface = deviceItem.GetService<NetworkInterface>();
            if (netInterface != null) NetworkInterface = new TiaNetworkInterface(netInterface, this);
            
            var opcUa = deviceItem.GetService<OpcUaUserManagement>();
            if (opcUa != null) OpcUa = new TiaOpcUa(opcUa, this);
        }

        /// <summary>   Reads all attributes recursive. </summary>
        ///
        /// <param name="readAttributDoTo"> (Optional) True to synchronize. </param>

        public override void ReadAllAttributesRecursive(EnmReadAttributDoTo readAttributDoTo = EnmReadAttributDoTo.AllSync)
        {
            if (readAttributDoTo.HasFlag(EnmReadAttributDoTo.Hardware))
            {
                this.ReadAdditionalComponents();
                this.ReadAllAttributes(readAttributDoTo);
                if (Addresses?.Count > 0) Addresses.ToList().ForEach(x => x.ReadAllAttributesRecursive(readAttributDoTo));
                if (Channels?.Count > 0) Channels.ToList().ForEach(x => x.ReadAllAttributesRecursive(readAttributDoTo));
                NetworkInterface?.ReadAllAttributesRecursive(readAttributDoTo);
                OpcUa?.ReadAllAttributesRecursive(readAttributDoTo);
            }
            
            Subitems?.ToList().ForEach(x => x.ReadAllAttributesRecursive(readAttributDoTo));
        }

        /// <summary>   Creates an object. </summary>
        ///
        /// <param name="parent">   The parent. </param>

        public override void CreateObject(IEngineeringObject parent)
        {
            if(parent is not HardwareObject hardwareObject) { Log.Debug("Create Object: Parent is not from type HardwareObject"); return; }

            Log.DebugFormat("Creating new DeviceItem (Name: {0})", this.Name);
            if (!this.GetAttributeValue<bool>("IsBuiltIn"))
            {
                string typeIdent = this.GetAttributeValue<string>("TypeIdentifier");
                int position = this.GetAttributeValue<int>("PositionNumber");
                if (hardwareObject.CanPlugNew(typeIdent, this.Name, position))
                {
                    Log.InfoFormat("Creating the deviceItem: {0} in parent: {1} on position {2}", this.Name, hardwareObject.Name, position);
                    this.Obj = hardwareObject.PlugNew(typeIdent, this.Name, position);
                    this.SetAllAttributes();
                    
                    this.Subitems?.SortObjects().ToList().ForEach(x => x.CreateObject((IEngineeringObject)this.Obj));
                }
            }
            else
            {
                // First search for Item with the same name
                var deviceItem = hardwareObject.Items.ToList().Find(x => x.Name == this.Name);
                
                // No DeviceItem found search with TypeIdentifier and PositionNumber
                // ReSharper disable once ConvertIfStatementToNullCoalescingExpression
                if (deviceItem == null)
                    deviceItem = hardwareObject.Items.ToList().Find(x =>
                        x.TypeIdentifier == this.GetAttributeValue<string>("TypeIdentifier") &&
                        x.PositionNumber == this.GetAttributeValue<int>("PositionNumber"));
                
                // DeviceItem found => Set all attributes
                if (deviceItem != null)
                {
                    this.Obj = deviceItem;
                    this.SetAllAttributes();
                    this.Subitems?.SortObjects().ToList().ForEach(x => x.CreateObject((IEngineeringObject)this.Obj));
                } 
            }
            
            if (this.Obj == null) return;
            this.NetworkInterface?.CreateObject((IEngineeringObject)this.Obj);
            this.OpcUa?.CreateObject((IEngineeringObject)this.Obj);

            var item = (DeviceItem) Obj;
            if (this.Addresses.Count > 0 && item.Addresses != null && item.Addresses.Count > 0)
            {
                for (var i = 0; i < this.Addresses.Count; i++)
                {
                    this.Addresses[i].Obj = item.Addresses[i];
                    this.Addresses[i].SetAllAttributes();
                }
            }
            
            if (this.Channels.Count > 0 && item.Channels is {Count: > 0})
            {
                for (var i = 0; i < this.Channels.Count; i++)
                {
                    this.Channels[i].Obj = item.Channels[i];
                    this.Channels[i].SetAllAttributes();
                }
            }
            
        }

        #endregion
    }

    /// <summary>   (Serializable) a tia device group. </summary>
    [Serializable]
    public class TiaDeviceGroup : TiaObject
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public TiaDeviceGroup()
        {
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="obj">      The object. </param>
        /// <param name="parent">   The parent. </param>

        public TiaDeviceGroup(object obj, TiaObject parent) : base(obj, parent)
        {
            this.EvtRefresh += (_, _) => ReadMainComponents();
            this.EvtExport += (_, _) => ExportObject(this);
            this.EvtImport += (_, _) => ImportObject();
            this.EvtDelete += (_, _) => DeleteObject();
            
            this.ReadMainComponents();
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        #endregion

        #region Properties

        #endregion

        #region Methods

        #endregion

        #region Override metods
        
        /// <summary>   Import object. </summary>
        public override void ImportObject()
        {
            var ofd = new OpenFileDialog()
            {
                Title = Resources.OpenFileDialogTitleImportFile,
                Filter = Resources.OpenFileFilterXML,
                Multiselect = true,
            };
            
            if (ofd.ShowDialog() != DialogResult.OK) return;
            foreach (string fileName in ofd.FileNames)
            {
                TiaObject impObj = fileName.TryDeserialize<TiaDevice>() ?? 
                                   fileName.TryDeserialize<TiaDeviceGroup>() ?? 
                                   fileName.TryDeserialize<TiaObject>();
                
                impObj?.CreateObject((IEngineeringObject)this.Obj);
            }
        }

        /// <summary>   Creates an object. </summary>
        public override void CreateObject()
        {
            if (Obj is not Device device) return;
            
            var messageText = string.Format(Resources.CreateDeviceitemQuestion, this.Name);
            if (MessageBox.Show(messageText, Resources.CreateQuestion, MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;

            messageText = Resources.FunctionOnlySupportedTiaHWEditorOpenQuestion;
            if (MessageBox.Show(messageText, Resources.CreateQuestion, MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;
            
            device.ShowInEditor(Siemens.Engineering.HW.View.Device);
        }
        
        /// <summary>   Deletes the object. </summary>
        public override void DeleteObject()
        {
            if (Obj is not DeviceUserGroup deviceUserGroup) return;

            var messageText = string.Format(Resources.DeleteDeviceUserGroupQuestion, this.Name);
            if (MsgBox.Question(messageText, Resources.DeleteQuestion).Button != MsgboxButton.Yes) return;
            deviceUserGroup.Delete();
            
            Log.InfoFormat("DeviceUserGroup {0} was deleted.", this.Name);
            this.Parent?.CmdRefresh.Execute(null);
        }
        
        /// <summary>   Reads main components. </summary>
        public sealed override void ReadMainComponents()
        {
            if (Obj is not DeviceGroup) return;

            switch (Obj)
            {
                case DeviceSystemGroup deviceSystemGroup:
                {
                    if (deviceSystemGroup.Devices.Count > 0)
                    {
                        Subitems ??= new ObservableCollection<TiaObject>();
                        Subitems?.Clear();
                        deviceSystemGroup.Devices.ToList().ForEach(x => Subitems.Add(new TiaDevice(x, this)));
                    }
                    else
                    {
                        Subitems?.ToList().ForEach(x => x.Dispose());
                        Subitems?.Clear();
                        Subitems = null;
                    }

                    break;
                }
                case DeviceUserGroup deviceUserGroup:
                {
                    if (deviceUserGroup.Groups.Count > 0 || deviceUserGroup.Devices.Count > 0)
                    {
                        Subitems ??= new ObservableCollection<TiaObject>();
                        Subitems?.Clear();
                        deviceUserGroup.Groups.ToList().ForEach(x => Subitems.Add(new TiaDeviceGroup(x, this)));
                        deviceUserGroup.Devices.ToList().ForEach(x => Subitems.Add(new TiaDevice(x, this)));
                    }
                
                    if (deviceUserGroup.Groups.Count == 0 && deviceUserGroup.Devices.Count == 0)
                    {
                        Subitems?.ToList().ForEach(x => x.Dispose());
                        Subitems?.Clear();
                        Subitems = null;
                    }

                    break;
                }
            }
        }

        /// <summary>   Reads all attributes recursive. </summary>
        ///
        /// <param name="readAttributDoTo"> (Optional) True to synchronize. </param>

        public override void ReadAllAttributesRecursive(EnmReadAttributDoTo readAttributDoTo = EnmReadAttributDoTo.AllSync)
        {
            if (readAttributDoTo.HasFlag(EnmReadAttributDoTo.Hardware)) this.ReadAllAttributes(readAttributDoTo);

            Subitems?.ToList().ForEach(x => x.ReadAllAttributesRecursive(readAttributDoTo));
        }

        /// <summary>   Creates an object. </summary>
        ///
        /// <param name="parent">   The parent. </param>

        public override void CreateObject(IEngineeringObject parent)
        {
            switch (ObjType)
            {
                case nameof(DeviceSystemGroup):
                {
                    if (parent is not Project project)
                    {
                        Log.Debug("Create Object: Parent is not from type Project"); 
                        return;
                    }
                
                    this.Obj = project.UngroupedDevicesGroup;
                    Subitems?.ToList().ForEach(x => x.CreateObject((IEngineeringObject)Obj));
                    break;
                }
                case nameof(DeviceUserGroup) when parent is Project project:
                    Log.DebugFormat("{0}Creating new DeviceGroup", Environment.NewLine);
                    this.Obj = project.DeviceGroups.Find(this.Name) ?? project.DeviceGroups.Create(this.Name);
                    Subitems?.ToList().ForEach(x => x.CreateObject((IEngineeringObject)Obj));
                    break;
                case nameof(DeviceUserGroup) when parent is DeviceUserGroup deviceUserGroup:
                    Log.DebugFormat("{0}Creating new DeviceGroup", Environment.NewLine);
                    this.Obj = deviceUserGroup.Groups.Find(this.Name) ?? deviceUserGroup.Groups.Create(this.Name);
                    Subitems?.ToList().ForEach(x => x.CreateObject((IEngineeringObject)Obj));
                    break;
                case nameof(DeviceUserGroup):
                    Log.Debug("Create Object: Parent is not from type Project/DeviceSystemGroup/DeviceUserGroup"); 
                    return;
            }
        }

        #endregion
    }
}