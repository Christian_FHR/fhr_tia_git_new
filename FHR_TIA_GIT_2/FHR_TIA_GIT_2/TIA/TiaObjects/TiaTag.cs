﻿// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT_2\TiaInstance\TiaTag.cs </file>
///
/// <copyright file="TiaTag.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the tia tag class. </summary>

using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Forms;
using FHR_TIA_GIT_2.Properties;
using MsgboxModernStyle;
using Siemens.Engineering;
using Siemens.Engineering.SW;
using Siemens.Engineering.SW.Tags;
using static FHR_TIA_GIT_2.Utillities.Logger.Log4Net;

namespace FHR_TIA_GIT_2.TIA.TiaObjects
{
    /// <summary>   (Serializable) a tia tag table group. </summary>
    [Serializable]
    public class TiaTagTableGroup : TiaObject
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public TiaTagTableGroup()
        {
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="obj">      The object. </param>
        /// <param name="parent">   The parent. </param>

        public TiaTagTableGroup(object obj, TiaObject parent) : base(obj, parent)
        {
            this.EvtRefresh += (_, _) => ReadMainComponents();
            this.EvtExport += (_, _) => ExportObject(this);
            this.EvtImport += delegate { ImportObject(); ReadMainComponents(); };
            this.EvtDelete += (_, _) => DeleteObject();
            
            this.ReadMainComponents();
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        #endregion

        #region Properties

        #endregion

        #region Methods

        #endregion

        #region Override metods

        /// <summary>   Reads main components. </summary>
        public sealed override void ReadMainComponents()
        {
            if (Obj is not PlcTagTableGroup plcTagTableGroup) return;

            if (plcTagTableGroup.Groups.Count > 0 || plcTagTableGroup.TagTables.Count > 0)
            {
                Subitems ??= new ObservableCollection<TiaObject>();
                Subitems?.Clear();
                
                plcTagTableGroup.Groups.ToList().ForEach(x => Subitems.Add(new TiaTagTableGroup(x, this)));
                plcTagTableGroup.TagTables.ToList().ForEach(x => Subitems.Add(new TiaTagTable(x, this)));
            }
            
            if (plcTagTableGroup.Groups.Count == 0 && plcTagTableGroup.TagTables.Count == 0)
            {
                Subitems?.ToList().ForEach(x => x.Dispose());
                Subitems?.Clear();
                Subitems = null;
            }
        }

        /// <summary>   Import object. </summary>
        public override void ImportObject()
        {
            var ofd = new OpenFileDialog()
            {
                Title = Resources.OpenFileDialogTitleImportFile,
                Filter = Resources.OpenFileFilterXML,
                Multiselect = true,
            };
            
            if (ofd.ShowDialog() != DialogResult.OK) return;
            foreach (string fileName in ofd.FileNames)
            {
                TiaObject impObj = fileName.TryDeserialize<TiaTagTableGroup>() ?? 
                                   fileName.TryDeserialize<TiaTagTable>() ?? 
                                   fileName.TryDeserialize<TiaTag>() ?? 
                                   fileName.TryDeserialize<TiaObject>();
                
                impObj?.CreateObject((IEngineeringObject)this.Obj);
            }
        }

        /// <summary>   Deletes the object. </summary>
        public override void DeleteObject()
        {
            if (Obj is not PlcTagTableUserGroup plcTagTableUserGroup) return;

            var messageText = $"Do you want to delete PlcTagTableUserGroup {this.Name} with all TagTables and Tags?";
            if (MsgBox.Question(messageText, "Delete?").Button != MsgboxButton.Yes) return;
            plcTagTableUserGroup.Delete();
            Log.InfoFormat("PlcTagTableUserGroup {0} was deleted.", this.Name);
            this.Parent?.CmdRefresh.Execute(null);
        }

        /// <summary>   Reads all attributes recursive. </summary>
        ///
        /// <param name="readAttributDoTo"> (Optional) True to synchronize. </param>

        public override void ReadAllAttributesRecursive(EnmReadAttributDoTo readAttributDoTo = EnmReadAttributDoTo.AllSync)
        {
            if (readAttributDoTo.HasFlag(EnmReadAttributDoTo.Software)) this.ReadAllAttributes(readAttributDoTo);

            Subitems?.ToList().ForEach(x => x.ReadAllAttributesRecursive(readAttributDoTo));
        }

        /// <summary>   Creates an object. </summary>
        ///
        /// <param name="parent">       The parent. </param>
        /// <param name="plcSoftware">  The PLC software. </param>

        public override void CreateObject(IEngineeringObject parent, PlcSoftware plcSoftware)
        {
            var software = plcSoftware ?? this.GetTypeFromObjectParent<PlcSoftware>(parent);

            switch (this.ObjType)
            {
                case nameof(PlcTagTableSystemGroup):
                    if (parent is not PlcSoftware)
                    {
                        Log.Debug("Create Object: Parent is not from type PLCSoftware");
                        return;
                    }

                    Log.DebugFormat("Creating new PlcTagTableSystemGroup (Name: {0})", this.Name);
                    this.Obj = software.TagTableGroup;
                    this.Subitems?.ToList().ForEach(x => x.CreateObject((IEngineeringObject) this.Obj, software));
                    break;

                case nameof(PlcTagTableUserGroup):

                    PlcTagTableGroup plcTagTableGroup;
                    switch (parent)
                    {
                        case PlcSoftware:
                            plcTagTableGroup = software?.TagTableGroup;
                            break;
                        case PlcTagTableGroup tagTableGroup:
                            plcTagTableGroup = tagTableGroup;
                            break;
                        default:
                            Log.Debug("Create Object: Parent is not from type PlcTagTableGroup"); return;
                    }

                    Log.DebugFormat("Creating new PlcTagTableUserGroup (Name: {0})", this.Name);
                    this.Obj = plcTagTableGroup?.Groups.Find(this.Name) ?? plcTagTableGroup?.Groups.Create(this.Name);
                    this.Subitems?.ToList().ForEach(x => x.CreateObject((IEngineeringObject) this.Obj, software));
                    break;
            }
        }

        #endregion
    }

    /// <summary>   (Serializable) a tia tag table. </summary>
    [Serializable]
    public class TiaTagTable : TiaObject
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public TiaTagTable()
        {
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="obj">      The object. </param>
        /// <param name="parent">   The parent. </param>

        public TiaTagTable(object obj, TiaObject parent) : base(obj, parent)
        {
            this.EvtRefresh += (_, _) => ReadMainComponents();
            this.EvtExport += (_, _) => ExportObject(this);
            this.EvtImport += delegate { ImportObject(); ReadMainComponents(); };
            this.EvtDelete += (_, _) => DeleteObject();
            
            this.ReadMainComponents();
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        #endregion

        #region Properties

        #endregion

        #region Methods

        #endregion

        #region Override metods

        /// <summary>   Reads main components. </summary>
        public sealed override void ReadMainComponents()
        {
            if (Obj is not PlcTagTable plcTagTable) return;

            if (plcTagTable.Tags.Count > 0 || plcTagTable.UserConstants.Count > 0)
            {
                Subitems ??= new ObservableCollection<TiaObject>();
                Subitems?.Clear();
                
                plcTagTable.Tags.ToList().ForEach(x => Subitems.Add(new TiaTag(x, this)));
                plcTagTable.UserConstants.ToList().ForEach(x => Subitems.Add(new TiaTag(x, this)));
            }

            if (plcTagTable.Tags.Count == 0 && plcTagTable.UserConstants.Count == 0)
            {
                Subitems?.ToList().ForEach(x => x.Dispose());
                Subitems?.Clear();
                Subitems = null;
            }

        }

        /// <summary>   Import object. </summary>
        public override void ImportObject()
        {
            var ofd = new OpenFileDialog()
            {
                Title = Resources.OpenFileDialogTitleImportFile,
                Filter = Resources.OpenFileFilterXML,
                Multiselect = true,
            };
            
            if (ofd.ShowDialog() != DialogResult.OK) return;
            foreach (string fileName in ofd.FileNames)
            {
                TiaObject impObj = fileName.TryDeserialize<TiaTagTable>() ?? 
                                   fileName.TryDeserialize<TiaTag>() ?? 
                                   fileName.TryDeserialize<TiaObject>();
                
                impObj?.CreateObject((IEngineeringObject)this.Obj);
            }
        }

        /// <summary>   Deletes the object. </summary>
        public override void DeleteObject()
        {
            if (Obj is not PlcTagTable plcTagTable) return;

            var messageText = $"Do you want to delete PLCTagTable {this.Name} with all Tags?";
            if (MsgBox.Question(messageText, "Delete?").Button != MsgboxButton.Yes) return;
            plcTagTable.Delete();
            Log.InfoFormat("PLCTagTable {0} was deleted.", this.Name);
            this.Parent?.CmdRefresh.Execute(null);
        }

        /// <summary>   Reads all attributes recursive. </summary>
        ///
        /// <param name="readAttributDoTo"> (Optional) True to synchronize. </param>

        public override void ReadAllAttributesRecursive(EnmReadAttributDoTo readAttributDoTo = EnmReadAttributDoTo.AllSync)
        {
            if (readAttributDoTo.HasFlag(EnmReadAttributDoTo.Software)) this.ReadAllAttributes(readAttributDoTo);

            Subitems?.ToList().ForEach(x => x.ReadAllAttributesRecursive(readAttributDoTo));
        }

        /// <summary>   Creates an object. </summary>
        ///
        /// <param name="parent">       The parent. </param>
        /// <param name="plcSoftware">  The PLC software. </param>

        public override void CreateObject(IEngineeringObject parent, PlcSoftware plcSoftware)
        {
            var software = plcSoftware ?? this.GetTypeFromObjectParent<PlcSoftware>(parent);

            PlcTagTableGroup plcTagTableGroup;
            switch (parent)
            {
                case PlcSoftware:
                    plcTagTableGroup = software?.TagTableGroup;
                    break;
                case PlcTagTableGroup tableGroup:
                    plcTagTableGroup = tableGroup;
                    break;
                default:
                    Log.Debug("Create Object: Parent is not from type PlcTagTableGroup"); return;
            }
            
            Log.DebugFormat("Creating new PlcTagTable (Name: {0})", this.Name);
            this.Obj = plcTagTableGroup?.TagTables.Find(this.Name) ?? plcTagTableGroup?.TagTables.Create(this.Name);
            this.Subitems?.ToList().ForEach(x => x.CreateObject((IEngineeringObject)this.Obj, software));
        }

        #endregion
    }

    /// <summary>   (Serializable) a tia tag. </summary>
    [Serializable]
    public class TiaTag : TiaObject
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public TiaTag()
        {
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="obj">      The object. </param>
        /// <param name="parent">   The parent. </param>

        public TiaTag(object obj, TiaObject parent) : base(obj, parent)
        {
            this.EvtExport += (_, _) => this.ExportObject(this);
            this.EvtDelete += (_, _) => this.DeleteObject();

            this.ReadMainComponents();
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        #endregion

        #region Properties

        #endregion

        #region Methods

        #endregion

        #region Override metods
        
        /// <summary>   Deletes the object. </summary>
        public override void DeleteObject()
        {
            if (Obj is not PlcTag plcTag) return;

            var messageText = $"Do you want to delete PLCTag {this.Name}?";
            if (MsgBox.Question(messageText, "Delete?").Button != MsgboxButton.Yes) return;
            plcTag.Delete();
            Log.InfoFormat("Tag {0} was deleted.", this.Name);
            this.Parent?.CmdRefresh.Execute(null);
        }

        /// <summary>   Reads main components. </summary>
        public sealed override void ReadMainComponents()
        {
            // if (Obj is not PlcTag) return;
        }

        /// <summary>   Reads all attributes recursive. </summary>
        ///
        /// <param name="readAttributDoTo"> (Optional) True to synchronize. </param>

        public override void ReadAllAttributesRecursive(EnmReadAttributDoTo readAttributDoTo = EnmReadAttributDoTo.AllSync)
        {
            if (readAttributDoTo.HasFlag(EnmReadAttributDoTo.Software)) this.ReadAllAttributes(readAttributDoTo);
        }

        /// <summary>   Creates an object. </summary>
        ///
        /// <param name="parent">       The parent. </param>
        /// <param name="plcSoftware">  The PLC software. </param>

        public override void CreateObject(IEngineeringObject parent, PlcSoftware plcSoftware)
        {
            var software = plcSoftware ?? this.GetTypeFromObjectParent<PlcSoftware>(parent);

            PlcTagTable plcTagTable;
            switch (parent)
            {
                case PlcSoftware:
                    plcTagTable = software?.TagTableGroup?.TagTables?.First();
                    break;
                case PlcTagTableGroup plcTagTableGroup:
                    plcTagTable = plcTagTableGroup.TagTables?.Count == 0 ? software?.TagTableGroup?.TagTables?.First() : plcTagTableGroup.TagTables?.First();
                    break;
                case PlcTagTable tagTable:
                    plcTagTable = tagTable;
                    break;
                default:
                    Log.Debug("Create Object: Parent is not from type PlcTagTable"); return;
            }
                    
            Log.DebugFormat("Creating new PlcTag (Name: {0})", this.Name);
            Log.InfoFormat("Creating the Tag: {0}", this.Name);
            this.Obj = plcTagTable?.Tags.Find(this.Name) ?? plcTagTable?.Tags.Create(this.Name);
            this.SetAllAttributes();
        }

        #endregion
    }
}