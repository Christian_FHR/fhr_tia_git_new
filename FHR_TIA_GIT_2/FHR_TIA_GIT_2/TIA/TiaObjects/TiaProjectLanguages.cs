﻿// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT\TiaInstances\TiaProjectLanguages.cs </file>
///
/// <copyright file="TiaProjectLanguages.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the tia project languages class. </summary>

using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using FHR_TIA_GIT_2.Utillities;
using FHR_TIA_GIT_2.ViewsAndModels.Models;
using static FHR_TIA_GIT_2.Utillities.Logger.Log4Net;

namespace FHR_TIA_GIT_2.TIA.TiaObjects
{
    /// <summary>   A tia project languages. </summary>
    public class TiaProjectLanguages : ViewModelBase, IDisposable
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public TiaProjectLanguages()
        {
            Languages = new ObservableCollection<TiaProjectLanguage>();
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="languages">    The languages. </param>

        public TiaProjectLanguages(ObservableCollection<TiaProjectLanguage> languages) : this()
        {
            Languages = languages;
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="project">  The project. </param>

        public TiaProjectLanguages(Siemens.Engineering.Project project) : this()
        {
            this._project = project; // Save internal to activate/deactivate a language
            
            project.LanguageSettings.Languages.ToList().ForEach(x =>
            {
                var lang = new TiaProjectLanguage(x.Culture, 
                    project.LanguageSettings.ReferenceLanguage.Equals(x),
                    project.LanguageSettings.EditingLanguage.Equals(x),
                    project.LanguageSettings.ActiveLanguages.Contains(x));
                
                lang.evtLanguageChange += OnevtLanguageChange;
                lang.evtLanguageReference += OnevtLanguageReference;
                lang.evtLanguageEditing += OnevtLanguageEditing;
                
                this.Languages.Add(lang);
            });
        }
        
        #endregion

        #region Types

        #endregion

        #region Fields

        /// <summary>   The languages. </summary>
        private ObservableCollection<TiaProjectLanguage> _languages;
        /// <summary>   The project. </summary>
        private Siemens.Engineering.Project _project;

        #endregion

        #region Properties

        /// <summary>   Gets or sets the languages. </summary>
        ///
        /// <value> The languages. </value>

        public ObservableCollection<TiaProjectLanguage> Languages
        {
            get { return _languages; }
            set { _languages = value; RaisePropertyChanged(); }
        }

        #endregion

        #region Methods

        /// <summary>   Onevt language change. </summary>
        ///
        /// <param name="sender">   The sender. </param>
        /// <param name="state">    True to state. </param>

        private void OnevtLanguageChange(TiaProjectLanguage sender, bool state)
        {
            try
            {
                if (state)
                {
                    // Check, if language is active
                    if (!IsLanguageActiveInProject(sender))
                        _project.LanguageSettings.ActiveLanguages.Add(_project.LanguageSettings.Languages.Find(sender.Language));
                    sender.IsActive = true;
                    Log.InfoFormat("Language {0} is set as active in Project {1}", sender.Language.Name, _project.Name);
                }
                else
                {
                    // Check if language is inactive
                    if (IsLanguageActiveInProject(sender))
                        _project.LanguageSettings.ActiveLanguages.Remove(_project.LanguageSettings.Languages.Find(sender.Language));
                    sender.IsActive = false;
                    Log.InfoFormat("Language {0} is set as inactive in Project {1}", sender.Language.Name, _project.Name);
                }
            }
            catch (Exception e)
            {
                Log.FatalFormat("Exception by activate/deactivte the language {0} {1} {2}", 
                    sender.Language.Name, Environment.NewLine, e.Message);
            }
        }

        /// <summary>   Onevt language reference. </summary>
        ///
        /// <param name="sender">   The sender. </param>

        private void OnevtLanguageReference(TiaProjectLanguage sender)
        {
            if (IsLanguageActiveInProject(sender))
            {
                Siemens.Engineering.Language lang = _project.LanguageSettings.Languages.Find(sender.Language);
                if (!_project.LanguageSettings.ReferenceLanguage.Equals(lang))
                {
                    _languages.First(x => x.Language.Equals(_project.LanguageSettings.ReferenceLanguage.Culture)).IsReference = false;
                    _project.LanguageSettings.ReferenceLanguage = lang;
                    sender.IsReference = true;
                }
            }
        }

        /// <summary>   Onevt language editing. </summary>
        ///
        /// <param name="sender">   The sender. </param>

        private void OnevtLanguageEditing(TiaProjectLanguage sender)
        {
            if (IsLanguageActiveInProject(sender))
            {
                Siemens.Engineering.Language lang = _project.LanguageSettings.Languages.Find(sender.Language);
                if (!_project.LanguageSettings.EditingLanguage.Equals(lang))
                {
                    _languages.First(x => x.Language.Equals(_project.LanguageSettings.EditingLanguage.Culture)).IsEditing = false;
                    _project.LanguageSettings.EditingLanguage = lang;
                    sender.IsEditing = true;
                }
            }
        }

        /// <summary>   Query if 'language' is language active in project. </summary>
        ///
        /// <param name="language"> The language. </param>
        ///
        /// <returns>   True if language active in project, false if not. </returns>

        private bool IsLanguageActiveInProject(TiaProjectLanguage language)
        {
            return _project.LanguageSettings.ActiveLanguages.Find(language.Language) != null;
        }

        /// <summary>
        ///     Führt anwendungsspezifische Aufgaben durch, die mit der Freigabe, der Zurückgabe oder dem
        ///     Zurücksetzen von nicht verwalteten Ressourcen zusammenhängen.
        /// </summary>

        public void Dispose()
        {
            if (_languages != null)
                _languages.ToList().ForEach(x => x.Dispose());

            _project = null;
        }
        
        #endregion

        #region Override metods

        #endregion
    }

    /// <summary>   A tia project language. </summary>
    public class TiaProjectLanguage : ViewModelBase, IDisposable
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public TiaProjectLanguage()
        {
            CmdLanguageSetActive = new CommandBase(OnCmdLanguageSetActive);
            CmdLanguageSetInactive = new CommandBase(OnCmdLanguageSetInactive);
            CmdLanguageSetAsReference = new CommandBase(OnCmdLanguageSetAsReference, CanCmdLanguageSetAsReference);
            CmdLanguageSetAsEditing = new CommandBase(OnCmdLanguageSetAsEditing, CanCmdLanguageSetAsEditing);

            CmIsOpen = false;
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="language">     The language. </param>
        /// <param name="isReference">  (Optional) True if is reference, false if not. </param>
        /// <param name="isEditing">    (Optional) True if is editing, false if not. </param>
        /// <param name="isActive">     (Optional) True if is active, false if not. </param>

        public TiaProjectLanguage(CultureInfo language, bool isReference = false, bool isEditing = false, bool isActive = false) : this()
        {
            Language = language;
            IsReference = isReference;
            IsEditing = isEditing;
            IsActive = isActive;

            Log.DebugFormat(
                "Readed Language Name: {0}; IsActive: {1}; IsEditingLanguage: {2}; IsReferenceLanguage: {3}", 
                _language.Name, _isActive, _isEditing, _isReference);
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        /// <summary>   The language. </summary>
        private CultureInfo _language;
        /// <summary>   True if is reference, false if not. </summary>
        private bool _isReference;
        /// <summary>   True if is editing, false if not. </summary>
        private bool _isEditing;
        /// <summary>   True if is active, false if not. </summary>
        private bool _isActive;
        /// <summary>   State of the language. </summary>
        private int _languageState;
        /// <summary>   True if centimetres is open. </summary>
        private bool _cmIsOpen;

        /// <summary>   Delegate for handling delgLanguageChange events. </summary>
        ///
        /// <param name="sender">   The sender. </param>
        /// <param name="state">    True to state. </param>

        public delegate void delgLanguageChangeEventHandler(TiaProjectLanguage sender, bool state);

        /// <summary>   Event queue for all listeners interested in evtLanguageChange events. </summary>
        public event delgLanguageChangeEventHandler evtLanguageChange;

        /// <summary>   Delegate for handling delgLanguageReference events. </summary>
        ///
        /// <param name="sender">   The sender. </param>

        public delegate void delgLanguageReferenceEventHandler(TiaProjectLanguage sender);

        /// <summary>   Event queue for all listeners interested in evtLanguageReference events. </summary>
        public event delgLanguageReferenceEventHandler evtLanguageReference;

        /// <summary>   Delegate for handling delgLanguageEditing events. </summary>
        ///
        /// <param name="sender">   The sender. </param>

        public delegate void delgLanguageEditingEventHandler(TiaProjectLanguage sender);

        /// <summary>   Event queue for all listeners interested in evtLanguageEditing events. </summary>
        public event delgLanguageEditingEventHandler evtLanguageEditing;
        
        #endregion

        #region Properties

        /// <summary>   Gets or sets the language. </summary>
        ///
        /// <value> The language. </value>

        public CultureInfo Language
        {
            get { return _language; }
            set { _language = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets a value indicating whether this object is reference. </summary>
        ///
        /// <value> True if this object is reference, false if not. </value>

        public bool IsReference
        {
            get { return _isReference; }
            set {
                _isReference = value;
                UpdateLanguageState();
                CmdLanguageSetAsReference.UpdateCanExecuteState();
                RaisePropertyChanged();
            }
        }

        /// <summary>   Gets or sets a value indicating whether this object is editing. </summary>
        ///
        /// <value> True if this object is editing, false if not. </value>

        public bool IsEditing
        {
            get { return _isEditing; }
            set
            {
                _isEditing = value;
                UpdateLanguageState();
                CmdLanguageSetAsEditing.UpdateCanExecuteState();
                RaisePropertyChanged();
            }
        }

        /// <summary>   Gets or sets a value indicating whether this object is active. </summary>
        ///
        /// <value> True if this object is active, false if not. </value>

        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                UpdateLanguageState();
                CmdLanguageSetAsReference.UpdateCanExecuteState();
                CmdLanguageSetAsEditing.UpdateCanExecuteState();
                RaisePropertyChanged();
            }
        }

        /// <summary>   Gets or sets the state of the language. </summary>
        ///
        /// <value> The language state. </value>

        public int LanguageState
        {
            get { return _languageState; }
            set { _languageState = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets a value indicating whether the centimetres is open. </summary>
        ///
        /// <value> True if centimetres is open, false if not. </value>

        public bool CmIsOpen
        {
            get { return _cmIsOpen; }
            set { _cmIsOpen = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets the command language set active. </summary>
        ///
        /// <value> The command language set active. </value>

        public CommandBase CmdLanguageSetActive { get; set; }

        /// <summary>   Gets or sets the command language set inactive. </summary>
        ///
        /// <value> The command language set inactive. </value>

        public CommandBase CmdLanguageSetInactive { get; set; }

        /// <summary>   Gets or sets the command language set as reference. </summary>
        ///
        /// <value> The command language set as reference. </value>

        public CommandBase CmdLanguageSetAsReference { get; set; }

        /// <summary>   Gets or sets the command language set as editing. </summary>
        ///
        /// <value> The command language set as editing. </value>

        public CommandBase CmdLanguageSetAsEditing { get; set; }

        #endregion

        #region Methods

        /// <summary>   Executes the 'command language set active' action. </summary>
        ///
        /// <param name="obj">  The object. </param>

        private void OnCmdLanguageSetActive(object obj)
        {
            CmIsOpen = false;
            evtLanguageChange?.Invoke(this, true);
        }

        /// <summary>   Executes the 'command language set inactive' action. </summary>
        ///
        /// <param name="obj">  The object. </param>

        private void OnCmdLanguageSetInactive(object obj)
        {
            CmIsOpen = false;
            evtLanguageChange?.Invoke(this, false);
        }

        /// <summary>   Executes the 'command language set as reference' action. </summary>
        ///
        /// <param name="obj">  The object. </param>

        private void OnCmdLanguageSetAsReference(object obj)
        {
            CmIsOpen = false;
            evtLanguageReference?.Invoke(this);
        }

        /// <summary>   Determine if we can command language set as reference. </summary>
        ///
        /// <param name="obj">  The object. </param>
        ///
        /// <returns>   True if we can command language set as reference, false if not. </returns>

        private bool CanCmdLanguageSetAsReference(object obj)
        {
            return !this.IsReference && this.IsActive;
        }

        /// <summary>   Executes the 'command language set as editing' action. </summary>
        ///
        /// <param name="obj">  The object. </param>

        private void OnCmdLanguageSetAsEditing(object obj)
        {
            CmIsOpen = false;
            evtLanguageEditing?.Invoke(this);
        }

        /// <summary>   Determine if we can command language set as editing. </summary>
        ///
        /// <param name="obj">  The object. </param>
        ///
        /// <returns>   True if we can command language set as editing, false if not. </returns>

        private bool CanCmdLanguageSetAsEditing(object obj)
        {
            return !this.IsEditing && this.IsActive;
        }

        /// <summary>   Updates the language state. </summary>
        private void UpdateLanguageState()
        {
            if (_isActive && !_isEditing && !_isReference) LanguageState = 1;
            else if (_isActive && _isEditing && !_isReference) LanguageState = 2;
            else if (_isActive && !_isEditing && _isReference) LanguageState = 3;
            else if (_isActive && _isEditing && _isReference) LanguageState = 4;
            else LanguageState = 0;
        }

        /// <summary>
        ///     Führt anwendungsspezifische Aufgaben durch, die mit der Freigabe, der Zurückgabe oder dem
        ///     Zurücksetzen von nicht verwalteten Ressourcen zusammenhängen.
        /// </summary>

        public void Dispose()
        {
            _language = null;
        }
        
        #endregion

        #region Override metods

        #endregion

    }
}