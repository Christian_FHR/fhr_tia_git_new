﻿
// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT_2\TiaInstance\TiaType.cs </file>
///
/// <copyright file="TiaType.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the tia type class. </summary>

using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Serialization;
using FHR_TIA_GIT_2.Properties;
using FHR_TIA_GIT_2.TIA.XMLParser;
using MsgboxModernStyle;
using Siemens.Engineering;
using Siemens.Engineering.SW;
using Siemens.Engineering.SW.ExternalSources;
using Siemens.Engineering.SW.Types;
using static FHR_TIA_GIT_2.Utillities.Logger.Log4Net;
using static FHR_TIA_GIT_2.Utillities.Constants;

namespace FHR_TIA_GIT_2.TIA.TiaObjects
{
    
    /// <summary>   (Serializable) a tia type group. </summary>
    [Serializable]
    public class TiaTypeGroup : TiaObject
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public TiaTypeGroup()
        {
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="obj">      The object. </param>
        /// <param name="parent">   The parent. </param>

        public TiaTypeGroup(object obj, TiaObject parent) : base(obj, parent)
        {
            this.EvtRefresh += (_, _) => ReadMainComponents();
            this.EvtExport += (_, _) => ExportObject(this);
            this.EvtImport += delegate { ImportObject(); ReadMainComponents(); };
            
            this.ReadMainComponents();
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        #endregion

        #region Properties

        #endregion

        #region Methods

        #endregion

        #region Override metods

        /// <summary>   Deletes the object. </summary>
        public override void DeleteObject()
        {
            if (Obj is not PlcTypeUserGroup plcTypeUserGroup) return;

            var messageText = $"Do you want to delete PlcTypeUserGroup {this.Name} with all Types?";
            if (MsgBox.Question(messageText, "Delete?").Button != MsgboxButton.Yes) return;
            plcTypeUserGroup.Delete();
            Log.InfoFormat("PlcTypeUserGroup {0} was deleted.", this.Name);
            this.Parent?.CmdRefresh.Execute(null);
        }
        
        /// <summary>   Reads main components. </summary>
        public sealed override void ReadMainComponents()
        {
            if (Obj is not PlcTypeGroup) return;

            switch (Obj)
            {
                case PlcTypeSystemGroup plcTypeSystemGroup:
                {
                    if (plcTypeSystemGroup.Groups.Count > 0 || plcTypeSystemGroup.Types.Count > 0)
                    {
                        Subitems ??= new ObservableCollection<TiaObject>();
                        Subitems?.Clear();
                        
                        plcTypeSystemGroup.Groups.ToList().ForEach(x => Subitems.Add(new TiaTypeGroup(x, this)));
                        plcTypeSystemGroup.Types.ToList().ForEach(x => Subitems.Add(new TiaType(x, this)));
                    }
                    
                    if (plcTypeSystemGroup.Groups.Count == 0 && plcTypeSystemGroup.Types.Count == 0)
                    {
                        Subitems?.ToList().ForEach(x => x.Dispose());
                        Subitems?.Clear();
                        Subitems = null;
                    }
                    
                    break;
                }
                case PlcTypeUserGroup plcTypeUserGroup:
                {
                    if (plcTypeUserGroup.Groups.Count > 0 || plcTypeUserGroup.Types.Count > 0)
                    {
                        Subitems ??= new ObservableCollection<TiaObject>();
                        Subitems?.Clear();
                        
                        plcTypeUserGroup.Groups.ToList().ForEach(x => Subitems.Add(new TiaTypeGroup(x, this)));
                        plcTypeUserGroup.Types.ToList().ForEach(x => Subitems.Add(new TiaType(x, this)));
                    }
                    
                    if (plcTypeUserGroup.Groups.Count == 0 && plcTypeUserGroup.Types.Count == 0)
                    {
                        Subitems?.ToList().ForEach(x => x.Dispose());
                        Subitems?.Clear();
                        Subitems = null;
                    }
                    break;
                }
            }
        }

        /// <summary>   Import object. </summary>
        public override void ImportObject()
        {
            var ofd = new OpenFileDialog()
            {
                Title = Resources.OpenFileDialogTitleImportFile,
                Filter = Resources.OpenFileFilterXML,
                Multiselect = true,
            };
            
            if (ofd.ShowDialog() != DialogResult.OK) return;
            foreach (string fileName in ofd.FileNames)
            {
                TiaObject impObj = fileName.TryDeserialize<TiaTypeGroup>() ?? 
                                   fileName.TryDeserialize<TiaType>() ?? 
                                   fileName.TryDeserialize<TiaObject>();
                
                impObj?.CreateObject((IEngineeringObject)this.Obj);
            }
        }

        /// <summary>   Reads all attributes recursive. </summary>
        ///
        /// <param name="readAttributDoTo"> (Optional) True to synchronize. </param>

        public override void ReadAllAttributesRecursive(EnmReadAttributDoTo readAttributDoTo = EnmReadAttributDoTo.AllSync)
        {
            if (readAttributDoTo.HasFlag(EnmReadAttributDoTo.Software)) this.ReadAllAttributes(readAttributDoTo);
            Subitems?.ToList().ForEach(x => x.ReadAllAttributesRecursive(readAttributDoTo));
        }

        /// <summary>   Creates an object. </summary>
        ///
        /// <param name="parent">       The parent. </param>
        /// <param name="plcSoftware">  The PLC software. </param>

        public override void CreateObject(IEngineeringObject parent, PlcSoftware plcSoftware)
        {
            var software = plcSoftware ?? this.GetTypeFromObjectParent<PlcSoftware>(parent);
            
            switch (ObjType)
            {
                case nameof(PlcTypeSystemGroup):
                    if(parent is not PlcSoftware) { Log.Debug("Create Object: Parent is not from type PlcSoftware"); return; }
                    
                    Log.DebugFormat("Creating new PlcTypeSystemGroup (Name: {0})", this.Name);
                    this.Obj = software.TypeGroup;
                    Subitems?.ToList().ForEach(x => x.CreateObject((IEngineeringObject)Obj, software));
                    break;
                
                case nameof(PlcTypeUserGroup):
                    PlcTypeGroup plcTypeGroup;
                    switch (parent)
                    {
                        case PlcSoftware:
                            plcTypeGroup = software?.TypeGroup;
                            break;
                        case PlcTypeGroup typeGroup:
                            plcTypeGroup = typeGroup;
                            break;
                        default:
                            Log.Debug("Create Object: Parent is not from type PlcTypeGroup"); return;
                    }

                    Log.DebugFormat("Creating new PlcTypeUserGroup (Name: {0})", this.Name);
                    this.Obj = plcTypeGroup?.Groups.Find(this.Name) ?? plcTypeGroup?.Groups.Create(this.Name);
                    Subitems?.ToList().ForEach(x => x.CreateObject((IEngineeringObject)Obj, software));
                    break;
            }
        }

        #endregion
    }

    /// <summary>   (Serializable) a tia type. </summary>
    [Serializable]
    public class TiaType : TiaObject
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public TiaType()
        {
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="obj">      The object. </param>
        /// <param name="parent">   The parent. </param>

        public TiaType(object obj, TiaObject parent) : base(obj, parent)
        {
            this.EvtExport += (_, _) => ExportObject(this);
            this.EvtDelete += (_, _) => DeleteObject();
            
            this.ReadMainComponents();
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        /// <summary>   Information describing the block. </summary>
        private string _blockData;

        /// <summary>   Information describing the simatic miles. </summary>
        private string _simaticMlData;
        /// <summary>   Information describing the source. </summary>
        private string _sourceData;

        /// <summary>   Information describing the simatic miles. </summary>
        private SimaticMlParser _simaticMlInformation;
        
        #endregion

        #region Properties

        /// <summary>   Gets or sets information describing the block. </summary>
        ///
        /// <value> Information describing the block. </value>

        //[XmlElement("Data")]
        [XmlIgnore]
        public string BlockData
        {
            get { return _blockData; }
            set { _blockData = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets information describing the simatic miles. </summary>
        ///
        /// <value> Information describing the simatic miles. </value>

        [XmlIgnore]
        public string SimaticMlData
        {
            get { return _simaticMlData; }
            set { _simaticMlData = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets information describing the source. </summary>
        ///
        /// <value> Information describing the source. </value>

        [XmlIgnore]
        public string SourceData
        {
            get { return _sourceData; }
            set { _sourceData = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets information describing the simatic miles. </summary>
        ///
        /// <value> Information describing the simatic miles. </value>

        public SimaticMlParser SimaticMlInformation
        {
            get { return _simaticMlInformation; }
            set { _simaticMlInformation = value; RaisePropertyChanged(); }
        }

        #endregion

        #region Methods

        #endregion

        #region Override metods

        /// <summary>   Deletes the object. </summary>
        public override void DeleteObject()
        {
            if (Obj is not PlcType plcType) return;

            var messageText = $"Do you want to delete PlcType {this.Name}?";
            if (MsgBox.Question(messageText, "Delete?").Button != MsgboxButton.Yes) return;
            plcType.Delete();
            Log.InfoFormat("PlcType {0} was deleted.", this.Name);
            this.Parent?.CmdRefresh.Execute(null);
        }
        
        /// <summary>   Reads main components. </summary>
        public sealed override void ReadMainComponents()
        {
            //if (Obj is not PlcStruct) return;
        }

        /// <summary>   Reads all attributes recursive. </summary>
        ///
        /// <param name="readAttributDoTo"> (Optional) True to synchronize. </param>

        public override void ReadAllAttributesRecursive(EnmReadAttributDoTo readAttributDoTo = EnmReadAttributDoTo.AllSync)
        {
            if (readAttributDoTo.HasFlag(EnmReadAttributDoTo.Software))
                this.ReadAllAttributes(readAttributDoTo);
        }

        public override void ParseSimaticMLData()
        {
            Log.DebugFormat("Start parsing Data from TiaType with Name: {0}", this.Name);
            this.SimaticMlData = GetSimaticMlData(this.Obj);
            SimaticMlInformation = new SimaticMlParser(this);
            SimaticMlInformation?.XmlInformations?.SortInstances(this.GetTypeFromParent<TiaPlcSoftware>());
        }
        
        /// <summary>   Creates an object. </summary>
        ///
        /// <param name="parent">       The parent. </param>
        /// <param name="plcSoftware">  The PLC software. </param>

        public override void CreateObject(IEngineeringObject parent, PlcSoftware plcSoftware)
        {
            var software = plcSoftware ?? this.GetTypeFromObjectParent<PlcSoftware>(parent);

            PlcTypeGroup typeGroup;
            switch (parent)
            {
                case PlcSoftware:
                    typeGroup = software?.TypeGroup;
                    break;
                case PlcTypeGroup plcTypeGroup:
                    typeGroup = plcTypeGroup;
                    break;
                default:
                    Log.Debug("Create Object: Parent is not from type PlcTypeGroup"); return;
            }
            
            if(string.IsNullOrEmpty(this.BlockData)) { Log.Debug("Create Object: Data is empty!"); return; }
            
            Log.DebugFormat("Creating new PlcType (Name: {0})", this.Name);
            string fileName = @"C:\FHR\" + this.Name + FileExtensionUdt;
            File.WriteAllText(fileName, this.BlockData);
            
            if (typeGroup?.Types.Find(this.Name) != null) typeGroup.Types.Find(this.Name).Delete();
            if (typeGroup is PlcTypeUserGroup plcTypeUserGroup)
                software?.ExternalSourceGroup.ExternalSources.CreateFromFile(this.Name, fileName)
                    .GenerateBlocksFromSource(plcTypeUserGroup, GenerateBlockOption.KeepOnError);
            else
                software?.ExternalSourceGroup.ExternalSources.CreateFromFile(this.Name, fileName)
                    .GenerateBlocksFromSource(GenerateBlockOption.KeepOnError);
            
            software?.ExternalSourceGroup.ExternalSources.Find(this.Name).Delete();
                    
            this.Obj = typeGroup?.Types.Find(this.Name);
            this.SetAllAttributes();
                    
            File.Delete(fileName);
        }

        #endregion
    }
}