﻿// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT_2\TiaInstance\TiaBlock.cs </file>
///
/// <copyright file="TiaBlock.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the tia block class. </summary>

using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Serialization;
using FHR_TIA_GIT_2.Properties;
using FHR_TIA_GIT_2.TIA.XMLParser;
using FHR_TIA_GIT_2.ViewsAndModels.Views.DetailsViews;
using MsgboxModernStyle;
using Siemens.Engineering;
using Siemens.Engineering.SW;
using Siemens.Engineering.SW.Blocks;
using Siemens.Engineering.SW.ExternalSources;
using static FHR_TIA_GIT_2.Utillities.Logger.Log4Net;
using static FHR_TIA_GIT_2.Utillities.Constants;

namespace FHR_TIA_GIT_2.TIA.TiaObjects
{
    /// <summary>   (Serializable) a tia block group. </summary>
    [Serializable]
    public class TiaBlockGroup : TiaObject
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public TiaBlockGroup()
        {
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="obj">      The object. </param>
        /// <param name="parent">   The parent. </param>

        public TiaBlockGroup(object obj, TiaObject parent) : base(obj, parent)
        {
            this.EvtRefresh += (_, _) => ReadMainComponents();
            this.EvtExport += (_, _) => ExportObject(this);
            this.EvtImport += delegate { ImportObject(); ReadMainComponents(); };
            this.EvtDelete += (_, _) => DeleteObject();
            
            this.ReadMainComponents();
        }

        #endregion

        #region Types

        #endregion

        #region Fields
        
        #endregion

        #region Properties

        #endregion

        #region Methods

        #endregion

        #region Override metods

        /// <summary>   Deletes the object. </summary>
        public override void DeleteObject()
        {
            if (Obj is not PlcBlockUserGroup plcBlockUserGroup) return;

            var messageText = string.Format(Resources.DeletePlcBlockUserGroupQuestion, this.Name);
            if (MsgBox.Question(messageText, Resources.DeleteQuestion).Button != MsgboxButton.Yes) return;
            plcBlockUserGroup.Delete();
            
            Log.InfoFormat("PlcBlockUserGroup {0} was deleted.", this.Name);
            this.Parent?.CmdRefresh.Execute(null);
        }
        
        /// <summary>   Reads main components. </summary>
        public sealed override void ReadMainComponents()
        {
            if (Obj is not PlcBlockGroup plcBlockGroup) return;

            if (plcBlockGroup.Groups.Count > 0 || plcBlockGroup.Blocks.Count > 0)
            {
                Subitems ??= new ObservableCollection<TiaObject>();
                Subitems?.Clear();
                
                plcBlockGroup.Groups.ToList().ForEach(x => Subitems.Add(new TiaBlockGroup(x, this)));
                plcBlockGroup.Blocks.ToList().ForEach(x => Subitems.Add(new TiaBlock(x, this)));
            }
            
            if (plcBlockGroup.Groups.Count == 0 && plcBlockGroup.Blocks.Count == 0)
            {
                Subitems?.ToList().ForEach(x => x.Dispose());
                Subitems?.Clear();
                Subitems = null;
            }
        }

        /// <summary>   Import object. </summary>
        public override void ImportObject()
        {
            var ofd = new OpenFileDialog()
            {
                Title = Resources.OpenFileDialogTitleImportFile,
                Filter = Resources.OpenFileFilterXML,
                Multiselect = true,
            };
            
            if (ofd.ShowDialog() != DialogResult.OK) return;
            foreach (string fileName in ofd.FileNames)
            {
                TiaObject impObj = fileName.TryDeserialize<TiaBlockGroup>() ?? 
                                   fileName.TryDeserialize<TiaBlock>() ?? 
                                   fileName.TryDeserialize<TiaObject>();
                
                impObj?.CreateObject((IEngineeringObject)this.Obj);
            }
        }

        /// <summary>   Reads all attributes recursive. </summary>
        ///
        /// <param name="readAttributDoTo"> (Optional) True to synchronize. </param>

        public override void ReadAllAttributesRecursive(EnmReadAttributDoTo readAttributDoTo = EnmReadAttributDoTo.AllSync)
        {
            if (readAttributDoTo.HasFlag(EnmReadAttributDoTo.Software))
            {
                this.ReadAllAttributes(readAttributDoTo);
                Subitems?.ToList().ForEach(x => x.ReadAllAttributesRecursive(readAttributDoTo));
            }
        }

        /// <summary>   Creates an object. </summary>
        ///
        /// <param name="parent">       The parent. </param>
        /// <param name="plcSoftware">  The PLC software. </param>

        public override void CreateObject(IEngineeringObject parent, PlcSoftware plcSoftware)
        {
            var software = plcSoftware ?? this.GetTypeFromObjectParent<PlcSoftware>(parent);
            switch (this.ObjType)
            {
                case nameof(PlcBlockSystemGroup):
                    if (parent is not PlcSoftware)
                    {
                        Log.Debug("Create Object: Parent is not from type PlcSoftware"); 
                        return;
                    }
                    
                    Log.DebugFormat("Creating new PlcBlockSystemGroup (Name: {0})", this.Name);
                    
                    this.Obj = software.BlockGroup;
                    this.Subitems?.SortPlcBlocks().ToList().ForEach(x => x.CreateObject((IEngineeringObject)this.Obj, software));
                    break;
                case nameof(PlcBlockUserGroup):
                    
                    PlcBlockGroup plcBlockGroup;
                    switch (parent)
                    {
                        case PlcSoftware:
                            plcBlockGroup = software?.BlockGroup;
                            break;
                        case PlcBlockGroup typeGroup:
                            plcBlockGroup = typeGroup;
                            break;
                        default:
                            Log.Debug("Create Object: Parent is not from type PlcBlockGroup"); return;
                    }

                    Log.DebugFormat("Creating new PlcBlockUserGroup (Name: {0})", this.Name);
                    this.Obj = plcBlockGroup?.Groups.Find(this.Name) ?? plcBlockGroup?.Groups.Create(this.Name);
                    this.Subitems?.SortPlcBlocks().ToList().ForEach(x => x.CreateObject((IEngineeringObject)this.Obj, software));
                    break;
            }
        }

        #endregion
    }
    
    /// <summary>   (Serializable) a tia block. </summary>
    [Serializable]
    public class TiaBlock : TiaObject
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public TiaBlock()
        {
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="obj">      The object. </param>
        /// <param name="parent">   The parent. </param>

        public TiaBlock(object obj, TiaObject parent) : base(obj, parent)
        {
            this.EvtExport += (_, _) => ExportObject(this);
            this.EvtDelete += (_, _) => DeleteObject();
            this.EvtParseFromXml += (_, _) => TESTPARSE();

            this.ReadMainComponents();
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        /// <summary>   Information describing the block. </summary>
        private string _blockData;

        /// <summary>   Information describing the simatic miles. </summary>
        private SimaticMlParser _simaticMlInformation;
        
        /// <summary>   Information describing the simatic miles. </summary>
        private string _simaticMlData;
        /// <summary>   Information describing the source. </summary>
        private string _sourceData;
        
        #endregion

        #region Properties

        //[XmlElement("Data")]

        /// <summary>   Gets or sets information describing the block. </summary>
        ///
        /// <value> Information describing the block. </value>

        [XmlIgnore]
        public string BlockData
        {
            get { return _blockData; }
            set { _blockData = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets information describing the simatic miles. </summary>
        ///
        /// <value> Information describing the simatic miles. </value>

        [XmlIgnore]
        public string SimaticMlData
        {
            get { return _simaticMlData; }
            set { _simaticMlData = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets information describing the source. </summary>
        ///
        /// <value> Information describing the source. </value>

        [XmlIgnore]
        public string SourceData
        {
            get { return _sourceData; }
            set { _sourceData = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets information describing the simatic miles. </summary>
        ///
        /// <value> Information describing the simatic miles. </value>

        [XmlIgnore]
        public SimaticMlParser SimaticMlInformation
        {
            get { return _simaticMlInformation; }
            set { _simaticMlInformation = value; RaisePropertyChanged();}
        }

        #endregion

        #region Methods

        /// <summary>   Testpars es this object. </summary>
        private void TESTPARSE()
        {
            try
            {
                ParseSimaticMLData();
                var detailWindow = new DetailWindow(this);
                detailWindow.ShowDialog();
            }
            catch (Exception e)
            {
                Log.FatalFormat("Exception. Message: {0}", e.Message);
            }
        }
        
        /// <summary>   Reads main components. </summary>
        public sealed override void ReadMainComponents()
        {
            // ReSharper disable once RedundantJumpStatement
            if (Obj is not PlcBlock) return;
        }

        /// <summary>   Creates block as code block. </summary>
        ///
        /// <param name="parent">       The parent. </param>
        /// <param name="plcSoftware">  The PLC software. </param>

        private void CreateBlockAsCodeBlock(IEngineeringObject parent, PlcSoftware plcSoftware)
        {
            Log.InfoFormat("Creating the Block: {0}", this.Name);
            string fileName;
            
            FindAndDeleteBlock(this.Name, null, plcSoftware);

            var progLang = this.GetAttributeValue<ProgrammingLanguage>(nameof(ProgrammingLanguage));
            if (progLang == ProgrammingLanguage.SCL)
            {
                fileName = @"C:\FHR\" + this.Name + FileExtensionScl;
                File.WriteAllText(fileName, this.BlockData);
                   
                if (parent is PlcBlockUserGroup blockUserGroup)
                    plcSoftware.ExternalSourceGroup.ExternalSources
                        .CreateFromFile(this.Name, fileName)
                        .GenerateBlocksFromSource(blockUserGroup, GenerateBlockOption.KeepOnError);
                else
                    plcSoftware.ExternalSourceGroup.ExternalSources
                        .CreateFromFile(this.Name, fileName)
                        .GenerateBlocksFromSource(GenerateBlockOption.KeepOnError);
                    
                plcSoftware.ExternalSourceGroup.ExternalSources.Find(this.Name).Delete();
            }
            else
            {
                fileName = @"C:\FHR\" + this.Name + FileExtensionAwl;
                File.WriteAllText(fileName, this.BlockData);
                ((PlcBlockGroup)parent).Blocks.Import(new FileInfo(fileName), ImportOptions.Override);
            }
                    
            this.Obj = ((PlcBlockGroup)parent).Blocks.Find(this.Name);
            this.SetAllAttributes();
                    
            File.Delete(fileName);
        }

        /// <summary>   Creates block as data block. </summary>
        ///
        /// <param name="parent">       The parent. </param>
        /// <param name="plcSoftware">  The PLC software. </param>

        private void CreateBlockAsDataBlock(IEngineeringObject parent, PlcSoftware plcSoftware)
        {
            Log.InfoFormat("Creating the Block: {0}", this.Name);
            
            string fileName = @"C:\FHR\" + this.Name + FileExtensionDataBlock;
            File.WriteAllText(fileName, this.BlockData);
                    
            FindAndDeleteBlock(this.Name, null, plcSoftware);
            var progLang = this.GetAttributeValue<ProgrammingLanguage>(nameof(ProgrammingLanguage));

            if (progLang != ProgrammingLanguage.F_DB)
            {
                if (parent is PlcBlockUserGroup plcBlockUserGroup)
                {
                    plcSoftware.ExternalSourceGroup.ExternalSources.CreateFromFile(this.Name, fileName).
                        GenerateBlocksFromSource(plcBlockUserGroup, GenerateBlockOption.KeepOnError);
                }
                else
                {
                    plcSoftware.ExternalSourceGroup.ExternalSources.CreateFromFile(this.Name, fileName).
                        GenerateBlocksFromSource(GenerateBlockOption.KeepOnError);
                }
                plcSoftware.ExternalSourceGroup.ExternalSources.Find(this.Name).Delete();
            }
            else
            {
                ((PlcBlockGroup)parent).Blocks.Import(new FileInfo(fileName), ImportOptions.Override);
            }

            this.Obj = ((PlcBlockGroup)parent).Blocks.Find(this.Name);
            this.SetAllAttributes();
                    
            File.Delete(fileName);
        }

        /// <summary>   Searches for the first and delete block. </summary>
        ///
        /// <param name="blockName">    Name of the block. </param>
        /// <param name="blockGroup">   Group the block belongs to. </param>
        /// <param name="plcSoftware">  The PLC software. </param>

        private void FindAndDeleteBlock(string blockName, PlcBlockGroup blockGroup, PlcSoftware plcSoftware)
        {
            PlcBlock block;
            if (blockGroup is null && plcSoftware is not null)
            {
                block = plcSoftware.BlockGroup.Blocks.Find(blockName);
                if (block != null) { block.Delete(); return; }

                plcSoftware.BlockGroup.Groups?.ToList().ForEach(x => FindAndDeleteBlock(blockName, x, null));
            }
            else if (blockGroup is not null)
            {
                block = blockGroup.Blocks.Find(blockName);
                if (block != null) { block.Delete(); return; }

                blockGroup.Groups?.ToList().ForEach(x => FindAndDeleteBlock(blockName, x, null));
            }
        }

        #endregion

        #region Override metods

        /// <summary>   Deletes the object. </summary>
        public override void DeleteObject()
        {
            if (Obj is not PlcBlock plcBlock) return;

            var messageText = $"Do you want to delete PlcBlock {this.Name}?";
            if (MsgBox.Question(messageText, "Delete?").Button != MsgboxButton.Yes) return;
            plcBlock.Delete();
            Log.InfoFormat("PlcBlock {0} was deleted.", this.Name);
            this.Parent?.CmdRefresh.Execute(null);
        }

        /// <summary>   Reads all attributes recursive. </summary>
        ///
        /// <param name="readAttributDoTo"> (Optional) True to synchronize. </param>

        public override void ReadAllAttributesRecursive(EnmReadAttributDoTo readAttributDoTo = EnmReadAttributDoTo.AllSync)
        {
            if (!readAttributDoTo.HasFlag(EnmReadAttributDoTo.Software)) return;
            
            this.ReadAllAttributes(readAttributDoTo);
        }
        
        public override void ParseSimaticMLData()
        {
            Log.DebugFormat("Start parsing Data from TiaBlock with Name: {0}", this.Name);
            this.SimaticMlData = GetSimaticMlData(this.Obj);
            SimaticMlInformation = new SimaticMlParser(this);
            SimaticMlInformation?.XmlInformations?.SortInstances(this.GetTypeFromParent<TiaPlcSoftware>());
        }

        /// <summary>   Creates an object. </summary>
        ///
        /// <param name="parent">       The parent. </param>
        /// <param name="plcSoftware">  The PLC software. </param>

        public override void CreateObject(IEngineeringObject parent, PlcSoftware plcSoftware)
        {
            var software = plcSoftware ?? this.GetTypeFromObjectParent<PlcSoftware>(parent);
            
            PlcBlockGroup plcBlockGroup;
            switch (parent)
            {
                case PlcSoftware:
                    plcBlockGroup = software?.BlockGroup;
                    break;
                case PlcBlockGroup typeGroup:
                    plcBlockGroup = typeGroup;
                    break;
                default:
                    Log.Debug("Create Object: Parent is not from type PlcBlockGroup"); return;
            }

            if (string.IsNullOrEmpty(this.BlockData))
            {
                Log.DebugFormat("Create Object: Data is empty in Object: {0}!", this.Name); 
                return;
            }

            switch (this.ObjType)
            {
                case nameof(InstanceDB):
                case nameof(GlobalDB):
                    Log.DebugFormat("Creating new Datablock (Name: {0})", this.Name);
                    CreateBlockAsDataBlock(plcBlockGroup, software);
                    break;
                case nameof(FC):
                case nameof(FB):
                case nameof(OB):
                    Log.DebugFormat("Creating new CodeBlock (Name: {0})", this.Name);
                    CreateBlockAsCodeBlock(plcBlockGroup, software);
                    break;
            }
        }

        #endregion
    }
}