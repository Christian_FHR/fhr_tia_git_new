﻿// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT_2\TiaInstance\TiaObject.cs </file>
///
/// <copyright file="TiaObject.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the tia object class. </summary>

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using FHR_TIA_GIT_2.Properties;
using FHR_TIA_GIT_2.TIA.TiaObjects;
using FHR_TIA_GIT_2.TIA.XMLParser.Models;
using FHR_TIA_GIT_2.Utillities;
using FHR_TIA_GIT_2.ViewsAndModels.Models;
using MsgboxModernStyle;
using Siemens.Engineering;
using Siemens.Engineering.Compiler;
using Siemens.Engineering.HW;
using Siemens.Engineering.HW.CustomDataTypes;
using Siemens.Engineering.Security;
using Siemens.Engineering.SW;
using Siemens.Engineering.SW.Blocks;
using Siemens.Engineering.SW.ExternalSources;
using Siemens.Engineering.SW.Types;
using static FHR_TIA_GIT_2.Utillities.Logger.Log4Net;
using static FHR_TIA_GIT_2.Utillities.Constants;
using DispatcherPriority = System.Windows.Threading.DispatcherPriority;

namespace FHR_TIA_GIT_2.TIA
{
    /// <summary>   A bit-field of flags for specifying enm attribute access modes. </summary>
    [Flags]
    public enum EnmAttributeAccessMode
    {
        /// <summary>   A binary constant representing the none flag. </summary>
        None = 0,
        /// <summary>   A binary constant representing the read flag. </summary>
        Read = 1,
        /// <summary>   A binary constant representing the write flag. </summary>
        Write = 2,
        /// <summary>   A binary constant representing the read write flag. </summary>
        ReadWrite = Read | Write,
    }

    /// <summary>   Values that represent enm attribute create relevancy. </summary>
    public enum EnmAttributeCreateRelevance
    {
        /// <summary>   An enum constant representing the none option. </summary>
        None = 0,
        /// <summary>   An enum constant representing the relevant option. </summary>
        Relevant = 1,
        /// <summary>   An enum constant representing the mandatory option. </summary>
        Mandatory = 2,
    }

    /// <summary>   A bit-field of flags for specifying enm read attribut do toes. </summary>
    [Flags]
    public enum EnmReadAttributDoTo
    {
        /// <summary>   A binary constant representing the synchron flag. </summary>
        Synchron = 1,
        /// <summary>   A binary constant representing the synchron flag. </summary>
        ASynchron = 2,
        /// <summary>   A binary constant representing the software flag. </summary>
        Software = 4,
        /// <summary>   A binary constant representing the hardware flag. </summary>
        Hardware = 8,
        /// <summary>   A binary constant representing the software Synchronize flag. </summary>
        SoftwareSync = Synchron | Software,
        /// <summary>   A binary constant representing the software a Synchronize flag. </summary>
        SoftwareASync = ASynchron | Software,
        /// <summary>   A binary constant representing the hardware Synchronize flag. </summary>
        HardwareSync = Synchron | Hardware,
        /// <summary>   A binary constant representing the hardware a Synchronize flag. </summary>
        HardwareASync = ASynchron | Hardware,
        /// <summary>   A binary constant representing all Synchronize flag. </summary>
        AllSync = Synchron | Software | Hardware,
        /// <summary>   A binary constant representing all a Synchronize flag. </summary>
        AllASync = ASynchron | Software | Hardware,
    }
    
    /// <summary>   Interface for tia object. </summary>
    public interface ITiaObject : IDisposable
    {
        /// <summary>   Gets or sets the name. </summary>
        ///
        /// <value> The name. </value>

        string Name { get; set; }

        /// <summary>   Gets or sets the object. </summary>
        ///
        /// <value> The object. </value>

        object Obj { get; set; }

        /// <summary>   Gets or sets the type of the object. </summary>
        ///
        /// <value> The type of the object. </value>

        string ObjType { get; set; }

        /// <summary>   Gets or sets the subitems. </summary>
        ///
        /// <value> The subitems. </value>

        ObservableCollection<TiaObject> Subitems { get; set; }

        /// <summary>   Gets or sets the attributes. </summary>
        ///
        /// <value> The attributes. </value>

        ObservableCollection<TiaObjectAttribute> Attributes { get; set; }

        /// <summary>   Reads all attributes. </summary>
        ///
        /// <param name="readAttributDoTo"> (Optional) </param>

        void ReadAllAttributes(EnmReadAttributDoTo readAttributDoTo = EnmReadAttributDoTo.AllSync);

        /// <summary>   Reads all attributes recursive. </summary>
        ///
        /// <param name="readAttributDoTo"> (Optional) </param>

        void ReadAllAttributesRecursive(EnmReadAttributDoTo readAttributDoTo = EnmReadAttributDoTo.AllSync);

        /// <summary>   Gets attribute value. </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="attName">  Name of the att. </param>
        /// <param name="fromObj">  (Optional) True to from object. </param>
        ///
        /// <returns>   The attribute value. </returns>

        T GetAttributeValue<T>(string attName, bool fromObj = false);
        /// <summary>   Sets all attributes. </summary>
        void SetAllAttributes();

        /// <summary>   Gets the parent of this item. </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="obj">  (Optional) The object. </param>
        ///
        /// <returns>   The type from object parent. </returns>

        T GetTypeFromObjectParent<T>(object obj = null);

        /// <summary>   Creates an object. </summary>
        ///
        /// <param name="parent">   The parent. </param>

        void CreateObject(IEngineeringObject parent);

        /// <summary>   Creates an object. </summary>
        ///
        /// <param name="parent">       The parent. </param>
        /// <param name="plcSoftware">  (Optional) The PLC software. </param>
        // ReSharper disable once MethodOverloadWithOptionalParameter

        void CreateObject(IEngineeringObject parent, PlcSoftware plcSoftware = null);
    }

#pragma warning disable 1587
    /// <summary>   (Serializable) a tia object. </summary>
#pragma warning restore 1587
    [XmlInclude(typeof(TiaInstance))]
    [XmlInclude(typeof(TiaProject))]
    [XmlInclude(typeof(TiaDeviceGroup))]
    [XmlInclude(typeof(TiaDevice))]
    [XmlInclude(typeof(TiaDeviceItem))]
    
    [XmlInclude(typeof(TiaPlcSoftware))]
    [XmlInclude(typeof(TiaTypeGroup))]
    [XmlInclude(typeof(TiaType))]
    
    [XmlInclude(typeof(TiaTagTableGroup))]
    [XmlInclude(typeof(TiaTagTable))]
    [XmlInclude(typeof(TiaTag))]
    
    [XmlInclude(typeof(TiaBlockGroup))]
    [XmlInclude(typeof(TiaBlock))]
    
    [XmlInclude(typeof(TiaAddress))]
    [XmlInclude(typeof(TiaChannel))]
    [XmlInclude(typeof(TiaNetworkInterface))]
    [XmlInclude(typeof(TiaNode))]
    [XmlInclude(typeof(TiaSubnet))]
    [XmlInclude(typeof(TiaIoController))]
    [XmlInclude(typeof(TiaIoSystem))]
    [XmlInclude(typeof(TiaIoConnector))]
    [XmlInclude(typeof(TiaOpcUa))]
    [Serializable]
    public class TiaObject : ViewModelBase, ITiaObject
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public TiaObject()
        {
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="obj">      The object. </param>
        /// <param name="parent">   The parent. </param>

        public TiaObject(object obj, TiaObject parent)
        {
            Obj = obj;
            Name = GetAttributeValue<string>(nameof(Name));
            Parent = parent;

            Log.InfoFormat("Founded object with name: {0} of type {1}", _name, obj.GetType().Name);

            CmdRefresh = new CommandBase(OnCmdRefresh);
            CmdExport = new CommandBase(OnCmdExport);
            CmdImport = new CommandBase(OnCmdImport);
            CmdDelete = new CommandBase(OnCmdDelete);
            CmdCreate = new CommandBase(OnCmdCreate);
            CmdParseFromXml = new CommandBase(OnCmdParseFromXml);
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        /// <summary>   The name. </summary>
        private string _name;
        /// <summary>   The object. </summary>
        private object _obj;
        /// <summary>   Type of the object. </summary>
        private string _objType;
        /// <summary>   The parent. </summary>
        private TiaObject _parent;
        /// <summary>   The subitems. </summary>
        private ObservableCollection<TiaObject> _subitems;
        /// <summary>   The attributes. </summary>
        private ObservableCollection<TiaObjectAttribute> _attributes;

        /// <summary>   Delegate for handling DelgRefresh events. </summary>
        ///
        /// <param name="sender">   Source of the event. </param>
        /// <param name="state">    Event information. </param>

        public delegate void DelgRefreshEventHandler(object sender, EventArgs state);
        /// <summary>   Event queue for all listeners interested in EvtRefresh events. </summary>
        public event DelgRefreshEventHandler EvtRefresh;

        /// <summary>   Delegate for handling DelgExport events. </summary>
        ///
        /// <param name="sender">   Source of the event. </param>
        /// <param name="state">    Event information. </param>

        public delegate void DelgExportEventHandler(object sender, EventArgs state);
        /// <summary>   Event queue for all listeners interested in EvtExport events. </summary>
        public event DelgExportEventHandler EvtExport;

        /// <summary>   Delegate for handling delgImport events. </summary>
        ///
        /// <param name="sender">   Source of the event. </param>
        /// <param name="state">    Event information. </param>

        public delegate void delgImportEventHandler(object sender, EventArgs state);
        /// <summary>   Event queue for all listeners interested in EvtImport events. </summary>
        public event delgImportEventHandler EvtImport;

        /// <summary>   Delegate for handling delgDelete events. </summary>
        ///
        /// <param name="sender">   Source of the event. </param>
        /// <param name="state">    Event information. </param>

        public delegate void delgDeleteEventHandler(object sender, EventArgs state);
        /// <summary>   Event queue for all listeners interested in EvtDelete events. </summary>
        public event delgDeleteEventHandler EvtDelete;

        /// <summary>   Delegate for handling delgCreate events. </summary>
        ///
        /// <param name="sender">   Source of the event. </param>
        /// <param name="state">    Event information. </param>

        public delegate void delgCreateEventHandler(object sender, EventArgs state);

        /// <summary>   Event queue for all listeners interested in EvtCreate events. </summary>
        public event delgCreateEventHandler EvtCreate;

        /// <summary>   Delegate for handling delgParseFromXml events. </summary>
        ///
        /// <param name="sender">   Source of the event. </param>
        /// <param name="state">    Event information. </param>

        public delegate void delgParseFromXmlEventHandler(object sender, EventArgs state);

        /// <summary>   Event queue for all listeners interested in EvtParseFromXml events. </summary>
        public event delgParseFromXmlEventHandler EvtParseFromXml;
        
        #endregion

        #region Properties

        /// <summary>   Gets or sets the name. </summary>
        ///
        /// <value> The name. </value>

        [XmlAttribute]
        public string Name
        {
            get { return _name; }
            set { _name = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets the object. </summary>
        ///
        /// <value> The object. </value>

        [XmlIgnore]
        public object Obj
        {
            get { return _obj; }
            set
            {
                _obj = value;
                if (_obj != null) ObjType = _obj.GetType().Name;
                RaisePropertyChanged();
            }
        }

        /// <summary>   Gets or sets the type of the object. </summary>
        ///
        /// <value> The type of the object. </value>

        [XmlAttribute("Type")]
        public string ObjType
        {
            get { return _objType; }
            set {
                _objType = value;
                if (_objType.EndsWith("Impl")) _objType = _objType.Remove(_objType.LastIndexOf("Impl", StringComparison.Ordinal));
                RaisePropertyChanged(); 
            }
        }

        /// <summary>   Gets or sets the parent. </summary>
        ///
        /// <value> The parent. </value>

        [XmlIgnore]
        public TiaObject Parent
        {
            get { return _parent; }
            set { _parent = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets the attributes. </summary>
        ///
        /// <value> The attributes. </value>

        [XmlArray ("Attributes")]
        [XmlArrayItem("Attribute")]
        public ObservableCollection<TiaObjectAttribute> Attributes
        {
            get { return _attributes; }
            set { _attributes = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets the subitems. </summary>
        ///
        /// <value> The subitems. </value>

        [XmlArray("Subitems")]
        [XmlArrayItem("Subitem")]
        public ObservableCollection<TiaObject> Subitems
        {
            get { return _subitems; }
            set { _subitems = value; RaisePropertyChanged(); }
        }

        #region Commands

        /// <summary>   Gets or sets the command refresh. </summary>
        ///
        /// <value> The command refresh. </value>

        [XmlIgnore]
        public CommandBase CmdRefresh { get; set; }

        /// <summary>   Gets or sets the command export. </summary>
        ///
        /// <value> The command export. </value>

        [XmlIgnore]
        public CommandBase CmdExport { get; set; }

        /// <summary>   Gets or sets the command import. </summary>
        ///
        /// <value> The command import. </value>

        [XmlIgnore]
        public CommandBase CmdImport { get; set; }

        /// <summary>   Gets or sets the command delete. </summary>
        ///
        /// <value> The command delete. </value>

        [XmlIgnore]
        public CommandBase CmdDelete { get; set; }

        /// <summary>   Gets or sets the command create. </summary>
        ///
        /// <value> The command create. </value>

        [XmlIgnore]
        public CommandBase CmdCreate { get; set; }

        /// <summary>   Gets or sets the command parse from XML. </summary>
        ///
        /// <value> The command parse from XML. </value>

        [XmlIgnore]
        public CommandBase CmdParseFromXml { get; set; }

        #endregion
        
        #endregion

        #region Methods

        #region Public Methods

        /// <summary>   Reads all attributes. </summary>
        ///
        /// <param name="readAttributDoTo"> (Optional) </param>

        public void ReadAllAttributes(EnmReadAttributDoTo readAttributDoTo = EnmReadAttributDoTo.AllSync)
        {
            Log.DebugFormat("Start reading all Attributes from Object: {0}. (Mode {1})", this.Name, readAttributDoTo.ToString());
            
            if(readAttributDoTo.HasFlag(EnmReadAttributDoTo.Synchron))
                GetAllAttributes();
            else if(readAttributDoTo.HasFlag(EnmReadAttributDoTo.ASynchron))
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(GetAllAttributes));
        }

        /// <summary>   Reads all attributes recursive. </summary>
        ///
        /// <param name="readAttributDoTo"> (Optional) True to synchronize. </param>

        public virtual void ReadAllAttributesRecursive(EnmReadAttributDoTo readAttributDoTo = EnmReadAttributDoTo.AllSync)
        {
            this.ReadAllAttributes(readAttributDoTo);
            if (this.Subitems.Count > 0) Subitems.ToList().ForEach(x => x.ReadAllAttributesRecursive(readAttributDoTo));
        }

        /// <summary>   Gets simatic miles instance by name. </summary>
        ///
        /// <param name="instanceName"> Name of the instance. </param>
        ///
        /// <returns>   The simatic miles instance by name. </returns>

        public XmlInformation GetSimaticMlInstanceByName(string instanceName)
        {
            if (this.Subitems == null) return null;
            
            foreach (TiaObject subitem in this.Subitems)
            {
                Log.DebugFormat("Search for Type/Block with name {0} in {1}", instanceName, this.Name);

                if ((subitem is TiaBlock or TiaType) && subitem.Name.ToLower().Equals(instanceName.ToLower().Trim(new[] {' ', '\"'})))
                {
                    switch (subitem)
                    {
                        case TiaBlock tiaBlock:
                            tiaBlock.ParseSimaticMLData();
                            if (tiaBlock.SimaticMlInformation?.XmlInformations != null)
                            {
                                Log.DebugFormat("Found TiaBlock with name {0}", tiaBlock.Name);
                                return tiaBlock.SimaticMlInformation.XmlInformations;
                            }
                            break;
                        case TiaType tiaType:
                            tiaType.ParseSimaticMLData();
                            if (tiaType.SimaticMlInformation?.XmlInformations != null)
                            {
                                Log.DebugFormat("Found TiaType with name {0}", tiaType.Name);
                                return tiaType.SimaticMlInformation.XmlInformations;
                            }
                            break;
                    }
                }
                else
                {
                    var founded = subitem?.GetSimaticMlInstanceByName(instanceName);
                    if (founded != null) return founded;
                }
            }
            return null;
        }

        /// <summary>   Gets attribute value. </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="attName">  Name of the att. </param>
        /// <param name="fromObj">  (Optional) True to from object. </param>
        ///
        /// <returns>   The attribute value. </returns>
        // ReSharper disable once MethodOverloadWithOptionalParameter

        public T GetAttributeValue<T>(string attName, bool fromObj = false)
        {
            if (fromObj && _obj != null) return GetAttributeValue<T>(attName);

            var att = Attributes.ToList().Find(x => x.Name == attName);
            if (att != null) return (T) att.Value;

            return GetAttributeValue<T>(attName);
        }
        
        /// <summary>   Sets all attributes. </summary>
        public void SetAllAttributes()
        {
            try
            {
                Attributes.Where(x => x.AccessMode.HasFlag(EnmAttributeAccessMode.Write)).ToList()
                    .ForEach(x => x.SetAttribute((IEngineeringObject) _obj));
            }
            catch (Exception e)
            {
                Log.Error("Error by setting the all attributes.", e);
            }
        }

        /// <summary>   Gets the parent of this item. </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="obj">  (Optional) The object. </param>
        ///
        /// <returns>   The type from object parent. </returns>

        public T GetTypeFromObjectParent<T>(object obj = null)
        {
            var tmpObj = (IEngineeringObject)obj ?? (IEngineeringObject) _obj;
            while (tmpObj is not T)
            {
                if (tmpObj.Parent == null || (tmpObj.Parent is Project && typeof(T) != typeof(Project))) return default(T);
                tmpObj = tmpObj.Parent;
            }

            return (T) tmpObj;
        }

        public T GetTypeFromParent<T>(TiaObject tiaObject = null) where T : TiaObject
        {
            var tmpObj = tiaObject ?? this;
            while (tmpObj is not T)
            {
                if (tmpObj.Parent == null || (tmpObj.Parent is TiaInstance && typeof(T) != typeof(TiaInstance))) return default(T);
                tmpObj = tmpObj.Parent;
            }

            return (T)tmpObj;
        }

        /// <summary>   Gets service from object parent with service. </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="obj">  (Optional) The object. </param>
        ///
        /// <returns>   The service from object parent with service. </returns>

        public T GetServiceFromObjectParentWithService<T>(object obj = null) where T : class, IEngineeringService
        {
            var serObj = (IEngineeringServiceProvider) obj ?? (IEngineeringServiceProvider) _obj;
            var tmpObj = (IEngineeringObject)obj ?? (IEngineeringObject) _obj;
            var service = serObj.GetService<T>();

            while (service == null)
            {
                if (tmpObj.Parent == null || (tmpObj.Parent is Project && typeof(T) != typeof(Project))) return default(T);
                tmpObj = tmpObj.Parent;

                serObj = (IEngineeringServiceProvider) tmpObj;
                service = serObj.GetService<T>();
            }
            
            return service;
        }

        #endregion
        
        #region Private Methods

        /// <summary>   Gets all attributes. </summary>
        private void GetAllAttributes()
        {
            try
            {
                if (_obj is null) return;
                
                this.Attributes ??= new ObservableCollection<TiaObjectAttribute>();
                this.Attributes.Clear();
                
                ((IEngineeringObject) _obj)?.GetAttributeInfos()?.ToList().ForEach(x =>
                    this.Attributes.Add(new TiaObjectAttribute(x, ((IEngineeringObject) _obj)?.GetAttribute(x.Name))));
            }
            catch (Exception e) { Log.FatalFormat("{0}", e.Message); }
        }

        /// <summary>   Gets attribute value. </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="attName">  Name of the att. </param>
        ///
        /// <returns>   The attribute value. </returns>

        private T GetAttributeValue<T>(string attName)
        {
            try
            {
                return (T) ((IEngineeringObject) _obj)?.GetAttribute(attName);
            }
            catch (Exception e)
            {
                Log.FatalFormat("Error by reading Attribute. Message {0}", e.Message);
            }

            return default(T);
        }

        /// <summary>   Gets data from PLC block. </summary>
        ///
        /// <param name="obj">  The object. </param>
        ///
        /// <returns>   The data from PLC block. </returns>

        internal string GetDataFromPlcBlock(object obj)
        {
            if (obj is not CodeBlock && obj is not DataBlock && obj is not PlcType) { return string.Empty; }
            
            var fileName = "";
            if (!Directory.Exists(ExportTargetPath)) Directory.CreateDirectory(ExportTargetPath);

            if (obj is PlcBlock {IsConsistent: false} or PlcType {IsConsistent: false})
                ((IEngineeringServiceProvider) obj).GetService<ICompilable>()?.Compile();

            ProgrammingLanguage progLang;
            
            switch (obj)
            {
                case CodeBlock codeBlock:
                    if (!codeBlock.IsConsistent || codeBlock.IsKnowHowProtected) return "";
                    
                    progLang = GetAttributeValue<ProgrammingLanguage>(nameof(ProgrammingLanguage));
                    if (progLang == ProgrammingLanguage.SCL)
                    {
                        fileName = ExportTargetPath + "\\" + this.Name + FileExtensionScl;
                        GetTypeFromObjectParent<PlcSoftware>().ExternalSourceGroup.GenerateSource(
                            new[] {codeBlock}, new FileInfo(fileName), GenerateOptions.None);
                    }
                    else
                    {
                        fileName = ExportTargetPath + "\\" + this.Name + FileExtensionAwl;
                        codeBlock.Export(new FileInfo(fileName), ExportOptions.WithDefaults | ExportOptions.WithReadOnly);
                    }
                    
                    break;
                case DataBlock dataBlock:
                    if (!dataBlock.IsConsistent || dataBlock.IsKnowHowProtected) return "";
                    
                    fileName = ExportTargetPath + "\\" + this.Name + FileExtensionDataBlock;
                    progLang = GetAttributeValue<ProgrammingLanguage>(nameof(ProgrammingLanguage));

                    if (progLang != ProgrammingLanguage.F_DB)
                    {
                        GetTypeFromObjectParent<PlcSoftware>().ExternalSourceGroup.GenerateSource(
                            new[] {dataBlock}, new FileInfo(fileName), GenerateOptions.None);
                    }
                    else
                    {
                        dataBlock.Export(new FileInfo(fileName), ExportOptions.WithDefaults | ExportOptions.WithReadOnly);
                    }
                    break;
                
                case PlcType plcType:
                    if (!plcType.IsConsistent || plcType.IsKnowHowProtected) return "";
                    
                    fileName = ExportTargetPath + "\\" + this.Name + FileExtensionUdt;
                    GetTypeFromObjectParent<PlcSoftware>().ExternalSourceGroup.GenerateSource(
                        new[] {plcType}, new FileInfo(fileName), GenerateOptions.None);
                    break;
            }

            string data = File.ReadAllText(fileName);
            if(File.Exists(fileName)) File.Delete(fileName);

            return data;
        }

        /// <summary>   Gets simatic miles data. </summary>
        ///
        /// <param name="obj">  The object. </param>
        ///
        /// <returns>   The simatic miles data. </returns>

        internal string GetSimaticMlData(object obj)
        {
            if (obj is not CodeBlock && obj is not DataBlock && obj is not PlcType) { return string.Empty; }

            if (!Directory.Exists(ExportTargetPath)) Directory.CreateDirectory(ExportTargetPath);

            if (obj is PlcBlock {IsConsistent: false} or PlcType {IsConsistent: false})
                ((IEngineeringServiceProvider) obj).GetService<ICompilable>()?.Compile();
            
            var fileName = ExportTargetPath + "\\" + this.Name + FileExtensionXml;
            Log.DebugFormat("Reading the data with SimaticML export function (Obj: {0})", this.Name);
            
            switch (obj)
            {
                case PlcBlock plcBlock:
                    if (!plcBlock.IsConsistent || plcBlock.IsKnowHowProtected) return "";

                    plcBlock.Export(new FileInfo(fileName), ExportOptions.WithDefaults | ExportOptions.WithReadOnly);
                    break;
                case PlcType plcType:
                    if (!plcType.IsConsistent || plcType.IsKnowHowProtected) return "";
                    
                    plcType.Export(new FileInfo(fileName), ExportOptions.WithDefaults | ExportOptions.WithReadOnly);
                    break;
            }
            
            string simaticMlData = File.ReadAllText(fileName);
            if(File.Exists(fileName)) File.Delete(fileName);

            return simaticMlData;
        }

        /// <summary>   Gets source data. </summary>
        ///
        /// <param name="obj">  The object. </param>
        ///
        /// <returns>   The source data. </returns>

        internal string GetSourceData(object obj)
        {
            if (obj is not CodeBlock && obj is not DataBlock && obj is not PlcType) { return string.Empty; }
            
            var fileName = "";
            if (!Directory.Exists(ExportTargetPath)) Directory.CreateDirectory(ExportTargetPath);

            if (obj is PlcBlock {IsConsistent: false} or PlcType {IsConsistent: false})
                ((IEngineeringServiceProvider) obj).GetService<ICompilable>()?.Compile();

            ProgrammingLanguage progLang;
            
            Log.DebugFormat("Reading the data with external source export function (Obj: {0})", this.Name);
            
            switch (obj)
            {
                case CodeBlock codeBlock:
                    if (!codeBlock.IsConsistent || codeBlock.IsKnowHowProtected) return "";
                    
                    progLang = GetAttributeValue<ProgrammingLanguage>(nameof(ProgrammingLanguage));
                    if (progLang != ProgrammingLanguage.SCL) return "";
                    
                    fileName = ExportTargetPath + "\\" + this.Name + FileExtensionScl;
                    GetTypeFromObjectParent<PlcSoftware>().ExternalSourceGroup.
                        GenerateSource(new[] {codeBlock}, new FileInfo(fileName), GenerateOptions.None);
                    break;
                
                case DataBlock dataBlock:
                    if (!dataBlock.IsConsistent || dataBlock.IsKnowHowProtected) return "";
                    
                    fileName = ExportTargetPath + "\\" + this.Name + FileExtensionDataBlock;
                    progLang = GetAttributeValue<ProgrammingLanguage>(nameof(ProgrammingLanguage));

                    if (progLang == ProgrammingLanguage.F_DB) return "";
                    
                    GetTypeFromObjectParent<PlcSoftware>().ExternalSourceGroup.
                        GenerateSource(new[] {dataBlock}, new FileInfo(fileName), GenerateOptions.None);
                    break;
                
                case PlcType plcType:
                    if (!plcType.IsConsistent || plcType.IsKnowHowProtected) return "";
                    
                    fileName = ExportTargetPath + "\\" + this.Name + FileExtensionUdt;
                    GetTypeFromObjectParent<PlcSoftware>().ExternalSourceGroup.
                        GenerateSource(new[] {plcType}, new FileInfo(fileName), GenerateOptions.None);
                    break;
            }

            string sourceData = File.ReadAllText(fileName);
            if(File.Exists(fileName)) File.Delete(fileName);

            return sourceData;
        }

        /// <summary>   Gets the parent of this item. </summary>
        ///
        /// <param name="fromObject">   from object. </param>
        ///
        /// <returns>   The git path from parent. </returns>

        internal string GetGitPathFromParent(TiaObject fromObject)
        {
            string path;
            if (Settings.Default.UseProjectPathAsGitPath) path = this.GetTypeFromObjectParent<Project>().Path.Directory + "\\GIT";
            else path = Settings.Default.LibraryGitPath + "\\" + this.GetTypeFromObjectParent<Project>().Name;

            if (!Directory.Exists(path)) Directory.CreateDirectory(path);
            
            List<TiaObject> parents = new List<TiaObject>();
            var tmp = fromObject.Parent;

            while (tmp is not TiaInstance)
            {
                if (tmp is null ) break;
                parents.Add(tmp);
                tmp = tmp.Parent;
            }

            parents.Reverse();
            foreach (var parent  in parents)
            {
                path = path.EndsWith("\\") ? path + parent.Name + "\\" : path + "\\" + parent.Name  + "\\";
                if (!Directory.Exists(path)) Directory.CreateDirectory(path);
            }

            return path;
        }

        #endregion

        #region Command methods

        /// <summary>   Executes the 'command refresh' action. </summary>
        ///
        /// <param name="obj">  The object. </param>

        private void OnCmdRefresh(object obj)
        {
            EvtRefresh?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>   Executes the 'command export' action. </summary>
        ///
        /// <param name="obj">  The object. </param>

        private void OnCmdExport(object obj)
        {
            EvtExport?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>   Executes the 'command import' action. </summary>
        ///
        /// <param name="obj">  The object. </param>

        private void OnCmdImport(object obj)
        {
            EvtImport?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>   Executes the 'command delete' action. </summary>
        ///
        /// <param name="obj">  The object. </param>

        private void OnCmdDelete(object obj)
        {
            EvtDelete?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>   Executes the 'command create' action. </summary>
        ///
        /// <param name="obj">  The object. </param>

        private void OnCmdCreate(object obj)
        {
            EvtCreate?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>   Executes the 'command parse from xml' action. </summary>
        ///
        /// <param name="obj">  The object. </param>

        private void OnCmdParseFromXml(object obj)
        {
            EvtParseFromXml?.Invoke(this, EventArgs.Empty);
        }

        #endregion
        
        #endregion

        #region Override metods

        public virtual void ParseSimaticMLData() { }
        
        /// <summary>   Reads git directory. </summary>
        ///
        /// <param name="directoryPath">    Full pathname of the directory file. </param>

        public virtual void ReadGitDirectory(string directoryPath)
        {
            this.Subitems ??= new ObservableCollection<TiaObject>();

            foreach (string file in Directory.GetFiles(directoryPath, "*.xml"))
            {
                TiaObject impObj = file.TryDeserialize<TiaObject>();
                
                if (impObj == null) continue;
                impObj.Parent = this;
                
                Directory.GetDirectories(directoryPath, impObj.Name).ToList().ForEach(x => impObj.ReadGitDirectory(x));
                this.Subitems.Add(impObj);
            }
        }
        
        /// <summary>   Creates an object. </summary>
        public virtual void CreateObject() { }

        /// <summary>   Creates an object. </summary>
        ///
        /// <param name="parent">   The parent. </param>

        public virtual void CreateObject(IEngineeringObject parent)
        {
            this.CreateObject(parent, null);
        }

        /// <summary>   Creates an object. </summary>
        ///
        /// <param name="parent">       The parent. </param>
        /// <param name="plcSoftware">  The PLC software. </param>

        public virtual void CreateObject(IEngineeringObject parent, PlcSoftware plcSoftware)
        {
            switch (ObjType)
            {
                case nameof(Project):
                    Obj = parent;
                    Subitems.SortObjects().ToList().ForEach(x => x.CreateObject((IEngineeringObject)Obj));
                    break;
            }
        }
        
        /// <summary>   Reads main components. </summary>
        public virtual void ReadMainComponents() {}
        
        /// <summary>   Reads additional components. </summary>
        public virtual void ReadAdditionalComponents() {}

        /// <summary>   Export objects. </summary>
        public virtual void ExportObjects() { }

        /// <summary>   Export object complete to one file. </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="toSer">    The object to serialize. </param>

        public virtual void ExportObject<T>(T toSer) where T : TiaObject, new()
        {
            const string messageText = "Do you want to export everything to one file or each object to a separate file? \r\n \r\n Cancel with Key escape.";
            const string btnRightText = "Only This Object";
            const string btnMiddleText = "All in separate files";
            const string btnLeftText = "All in one file";
            var msgBox = MsgBox.Custom(messageText, "Export", MsgboxIcons.Question, MsgboxButtons.Ok,
                btnRightText ,btnMiddleText, btnLeftText);

            if (msgBox.Button == MsgboxButton.Close) return;
            switch (msgBox.ButtonText)
            {
                case btnLeftText: ExportOneFile(toSer); break;
                case btnMiddleText: ExportSeparateFile(toSer); break;
                case btnRightText: ExportSeparateFile(toSer, true); break;
            }
        }

        /// <summary>   Export one file. </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="toSer">    The object to serialize. </param>

        private void ExportOneFile<T>(T toSer) where T : TiaObject
        {
            var path = GetGitPathFromParent(toSer);
            var fileName = path.EndsWith("\\") ? path + toSer.Name + ".xml" : path + "\\" + toSer.Name + ".xml";
            toSer.ReadAllAttributesRecursive();
            
            Stream stream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.Write);
            try
            {
                var xmlSerializer = new XmlSerializer(typeof(T));
                xmlSerializer.Serialize(stream, toSer);
            }
            catch (Exception e) { Log.FatalFormat("Exception by serialize Object: {0}", e.Message); }
            finally { stream.Close(); }
        }

        /// <summary>   Export separate file. </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="toSer">    The object to serialize. </param>
        /// <param name="onlyThis"> (Optional) True to only this. </param>

        private void ExportSeparateFile<T>(T toSer, bool onlyThis = false) where T : TiaObject, new()
        {
            T copy = (T) toSer.MemberwiseClone();

            switch (copy)
            {
                case TiaProject:
                case TiaPlcSoftware:
                case TiaDeviceGroup:
                case TiaTypeGroup:
                case TiaTagTableGroup:
                case TiaBlockGroup:
                case TiaType:
                case TiaTagTable:
                case TiaTag:
                case TiaBlock:
                    copy.Subitems = null;
                    ExportOneFile(copy);
                    if (toSer.Subitems?.Count > 0 && !onlyThis) 
                        toSer.Subitems?.ToList().ForEach(x => ExportSeparateFile(x));
                    break;
                case TiaDevice:
                    var software = toSer.Subitems?.ToList().Find(x => x is TiaPlcSoftware);
                    if (software != null) copy.Subitems?.Remove(software);
                    ExportOneFile(copy);
                    if (software != null && !onlyThis) ExportSeparateFile(software);
                    break;
            }
        }

        /// <summary>   Import object. </summary>
        public virtual void ImportObject() { }
        
        /// <summary>   Deletes the object. </summary>
        public virtual void DeleteObject() { }

        /// <summary>
        ///     Führt anwendungsspezifische Aufgaben durch, die mit der Freigabe, der Zurückgabe oder dem
        ///     Zurücksetzen von nicht verwalteten Ressourcen zusammenhängen.
        /// </summary>

        public virtual void Dispose()
        {
            _subitems?.ToList().ForEach(x => x.Dispose());
            _subitems?.Clear();
            _subitems = null;
            _attributes?.ToList().ForEach(x => x.Dispose());
            _attributes?.Clear();
            _attributes = null;
        }

        #endregion
    }
    
    /// <summary>   Attribute for tia object. </summary>
    [Serializable]
    public class TiaObjectAttribute : ViewModelBase, IDisposable, IXmlSerializable
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public TiaObjectAttribute()
        {
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="name">             Name of the attribute. </param>
        /// <param name="accessMode">       The access mode. </param>
        /// <param name="createRelevance">  The create relevance. </param>
        /// <param name="attributeValue">   The attribute value. </param>

        public TiaObjectAttribute(string name, EnmAttributeAccessMode accessMode, EnmAttributeCreateRelevance createRelevance, object attributeValue)
        {
            Name = name;
            AccessMode = accessMode;
            CreateRelevance = createRelevance;
            Value = attributeValue ?? string.Empty;
            ValueType = Value.GetType();
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="name">                 Name of the attribute. </param>
        /// <param name="accessMode">           The access mode. </param>
        /// <param name="createRelevance">      The create relevance. </param>
        /// <param name="attributeValue">       The attribute value. </param>
        /// <param name="attributeValueType">   Type of the attribute value. </param>

        public TiaObjectAttribute(string name, string accessMode, string createRelevance, string attributeValue, string attributeValueType)
        {
            Name = name;
            AccessMode = (EnmAttributeAccessMode) Enum.Parse(typeof(EnmAttributeAccessMode), accessMode);
            CreateRelevance = (EnmAttributeCreateRelevance) Enum.Parse(typeof(EnmAttributeCreateRelevance), createRelevance);
            Value = TryParse(attributeValue, attributeValueType);
            ValueType = Value.GetType();
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="attribute">        The attribute. </param>
        /// <param name="attributeValue">   The attribute value. </param>

        public TiaObjectAttribute(EngineeringAttributeInfo attribute, object attributeValue)
        {
            Name = attribute.Name;
            AccessMode = (EnmAttributeAccessMode)Enum.Parse(typeof(EnmAttributeAccessMode), attribute.AccessMode.ToString());
            CreateRelevance = (EnmAttributeCreateRelevance)Enum.Parse(typeof(EnmAttributeCreateRelevance), attribute.CreateRelevance.ToString());
            
            switch (attributeValue)
            {
                case MultilingualText text:
                    Value = new TiaMultiLanguage(text);
                    ValueType = Value == null ? typeof(string) : Value.GetType();
                    break;
                case TableData:
                    Value = AttributeNotSupported;
                    ValueType = Value == null ? typeof(string) : Value.GetType();
                    break;
                // TODO: Save the certificate on export and write in the XML where the certificate is contained.
                case Certificate:
                    Value = AttributeNotSupported;
                    ValueType = typeof(Certificate);
                    break;
                case string[] value:
                    Value = String.Join(";", value);
                    ValueType = typeof(string[]);
                    break;
                default:
                    Value = attributeValue ?? "";
                    ValueType = Value == null ? typeof(string) : Value.GetType();
                    break;
            }
            
            Log.InfoFormat("Attribute {0} readed (Value: {1}, Valuetype: {2})", Name, Value, ValueType.Name);
        }
        
        #endregion

        #region Types
        
        #endregion

        #region Fields

        /// <summary>   Name of the attribute. </summary>
        private string _name;
        /// <summary>   The access mode. </summary>
        private EnmAttributeAccessMode _accessMode;
        /// <summary>   The create relevance. </summary>
        private EnmAttributeCreateRelevance _createRelevance;
        /// <summary>   The attribute value. </summary>
        private object _value;
        /// <summary>   Type of the attribute value. </summary>
        private Type _valueType;
        /// <summary>   Name of the attribute value type. </summary>
        private string _attributeValueTypeName;
        
        #endregion

        #region Properties

        /// <summary>   Gets or sets the name of the attribute. </summary>
        ///
        /// <value> The name of the attribute. </value>

        [XmlAttribute]
        public string Name
        {
            get { return _name; }
            set { _name = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets the access mode. </summary>
        ///
        /// <value> The access mode. </value>

        [XmlAttribute]
        public EnmAttributeAccessMode AccessMode
        {
            get { return _accessMode; }
            set { _accessMode = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets the create relevance. </summary>
        ///
        /// <value> The create relevance. </value>

        [XmlAttribute]
        public EnmAttributeCreateRelevance CreateRelevance
        {
            get { return _createRelevance; }
            set { _createRelevance = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets the value. </summary>
        ///
        /// <value> The value. </value>

        [XmlIgnore]
        public object Value
        {
            get { return _value; }
            set { _value = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets the type of the attribute value. </summary>
        ///
        /// <value> The type of the attribute value. </value>

        [XmlIgnore]
        public Type ValueType
        {
            get { return _valueType; }
            set
            {
                _valueType = value;
                ValueTypeName = value.ToString();
            }
        }

        /// <summary>   Gets or sets the name of the value type. </summary>
        ///
        /// <value> The name of the value type. </value>

        [XmlIgnore]
        public string ValueTypeName
        {
            get { return _valueType.ToString(); }
            set { _attributeValueTypeName = value; }
        }

        #endregion

        #region Methods

        /// <summary>   Try parse. </summary>
        ///
        /// <param name="value">        The value. </param>
        /// <param name="valueType">    Type of the value. </param>
        ///
        /// <returns>   An object. </returns>

        private object TryParse(string value, string valueType)
        {
            if (valueType == typeof(string[]).Name)
                return value.Split(new [] {';'}, StringSplitOptions.RemoveEmptyEntries);

            return valueType switch
            {
                nameof(Boolean) => Convert.ToBoolean(value),
                nameof(String) => value,
                nameof(Int32) => Convert.ToInt32(value),
                nameof(Int64) => Convert.ToInt64(value),
                nameof(UInt32) => Convert.ToUInt32(value),
                nameof(UInt64) => Convert.ToUInt64(value),
                nameof(DateTime) => Convert.ToDateTime(value),
                nameof(TiaMultiLanguage) => new TiaMultiLanguage(value),
                nameof(PnDnsConfigNameResolve) => value.ParseToEnum<PnDnsConfigNameResolve>(),
                nameof(InterfaceOperatingModes) => value.ParseToEnum<InterfaceOperatingModes>(),
                nameof(MediaRedundancyRole) => value.ParseToEnum<MediaRedundancyRole>(),
                nameof(TransmissionRateAndDuplex) => value.ParseToEnum<TransmissionRateAndDuplex>(),
                nameof(ProgrammingLanguage) => value.ParseToEnum<ProgrammingLanguage>(),
                nameof(MemoryLayout) => value.ParseToEnum<MemoryLayout>(),
                nameof(BusProfile) => value.ParseToEnum<BusProfile>(),
                nameof(BaudRate) => value.ParseToEnum<BaudRate>(),
                nameof(IpProtocolSelection) => value.ParseToEnum<IpProtocolSelection>(),
                nameof(SyncRole) => value.ParseToEnum<SyncRole>(),
                _ => ""
            };
        }

        /// <summary>   Sets an attribute. </summary>
        ///
        /// <param name="obj">  The object. </param>

        internal void SetAttribute(IEngineeringObject obj)
        {
            try
            {
                if (obj is null) return;
                if (_value is AttributeNotSupported) return;
                
                if (_valueType == typeof(TiaMultiLanguage))
                    ((TiaMultiLanguage) _value).SetMultiLanguage(obj.GetAttribute(_name));
                else
                {
                    Log.DebugFormat("Setting Attribute Name: {0} with value:{1} of type: {2}", Name, Value,
                        ValueType.Name);
                    obj.SetAttribute(Name, Value);
                }
            }
            catch (EngineeringNotSupportedException)
            {
                Log.InfoFormat("Setting of attribute not Supported (Name: {0}, Value: {1})", _name, _value);
            }
            catch (EngineeringTargetInvocationException)
            {
                Log.ErrorFormat("Setting of attribute with wrong Valuetype (Name: {0}, Value: {1}, my type: {2}, expected type: {3}",
                    _name, _value, _valueType, obj?.GetAttribute(_name).GetType());
            }
            catch (Exception e)
            {
                Log.ErrorFormat("Error by setting the attribute (Name: {0}, Value: {1}) with Exception:{2}{2}{3}",
                    _name, _value, Environment.NewLine , e.Message);
            }
        }
        
        #endregion

        #region Override metods

        /// <summary>
        ///     Führt anwendungsspezifische Aufgaben durch, die mit der Freigabe, der Zurückgabe oder dem
        ///     Zurücksetzen von nicht verwalteten Ressourcen zusammenhängen.
        /// </summary>

        public void Dispose()
        {
            Name = string.Empty;
            Value = null;
        }

        /// <summary>
        ///     Diese Methode ist reserviert und sollte nicht verwendet werden.
        ///      Bei der Implementierung der <see langword="IXmlSerializable" />-Schnittstelle sollte von
        ///      dieser Methode <see langword="null" /> (<see langword="Nothing" /> in Visual Basic)
        ///      zurückgegeben werden. Wenn die Angabe eines benutzerdefinierten Schemas erforderlich ist,
        ///      sollten Sie stattdessen das das
        ///      <see cref="T:System.Xml.Serialization.XmlSchemaProviderAttribute" /> auf die Klasse
        ///      anwenden.
        /// </summary>
        ///
        /// <returns>
        ///     Ein <see cref="T:System.Xml.Schema.XmlSchema" /> zur Beschreibung der XML-Darstellung des
        ///     Objekts, das von der
        ///     <see cref="M:System.Xml.Serialization.IXmlSerializable.WriteXml(System.Xml.XmlWriter)" />-
        ///     Methode erstellt und von der
        ///     <see cref="M:System.Xml.Serialization.IXmlSerializable.ReadXml(System.Xml.XmlReader)" />-
        ///     Methode verwendet wird.
        /// </returns>

        public XmlSchema GetSchema()
        {
            return null;
        }

        /// <summary>   Generiert ein Objekt aus seiner XML-Darstellung. </summary>
        ///
        /// <param name="reader">   Der <see cref="T:System.Xml.XmlReader" />-Stream, aus dem das Objekt
        ///                         deserialisiert wird. </param>

        public void ReadXml(XmlReader reader)
        {
            reader.MoveToAttribute(nameof(Name));
            this.Name = reader.Value;
            
            reader.MoveToAttribute(nameof(AccessMode));
            this.AccessMode = (EnmAttributeAccessMode) Enum.Parse(typeof(EnmAttributeAccessMode), reader.Value);
            
            reader.MoveToAttribute(nameof(CreateRelevance));
            this.CreateRelevance = (EnmAttributeCreateRelevance) Enum.Parse(typeof(EnmAttributeCreateRelevance), reader.Value);
            
            reader.MoveToAttribute(nameof(ValueType));
            string valueType = reader.Value;
            
            reader.MoveToAttribute(nameof(Value));
            string attValue = reader.Value;

            this.Value = this.TryParse(attValue, valueType);
            this.ValueType = this.Value == null ? typeof(string) : this.Value.GetType();

            reader.MoveToElement();
            reader.Read();
        }

        /// <summary>   Konvertiert ein Objekt in seine XML-Darstellung. </summary>
        ///
        /// <param name="writer">   Der <see cref="T:System.Xml.XmlWriter" />-Stream, in den das Objekt
        ///                         serialisiert wird. </param>

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString(nameof(Name), Name);
            writer.WriteAttributeString(nameof(Value), Value.ToString());
            writer.WriteAttributeString(nameof(AccessMode), AccessMode.ToString());
            writer.WriteAttributeString(nameof(CreateRelevance), CreateRelevance.ToString());
            writer.WriteAttributeString(nameof(ValueType), ValueType == null ? String.Empty : ValueType.Name);
        }
        
        #endregion
    }

    /// <summary>   A tia multi language. </summary>
    public class TiaMultiLanguage : ViewModelBase, IDisposable
    {
        #region Constructors

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="attributeValue">   The attribute value. </param>

        public TiaMultiLanguage(MultilingualText attributeValue)
        {
            if (attributeValue != null)
            {
                if (MultiLanguages == null) MultiLanguages = new Dictionary<CultureInfo, string>();
                else MultiLanguages.Clear();
                
                foreach (MultilingualTextItem multilingualTextItem in attributeValue.Items)
                {
                    MultiLanguages.Add(multilingualTextItem.Language.Culture, multilingualTextItem.Text);    
                }
            }
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="attributeValue">   The attribute value. </param>

        public TiaMultiLanguage(string attributeValue)
        {
            if (MultiLanguages == null) MultiLanguages = new Dictionary<CultureInfo, string>();
            else MultiLanguages.Clear();
            
            if (!string.IsNullOrEmpty(attributeValue))
            {
                foreach (string language in attributeValue.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                {
                    string langCode = language.Split(':')[0];
                    string langText = language.Split(':')[1];

                    if (!string.IsNullOrEmpty(langCode))
                    {
                        MultiLanguages.Add(new CultureInfo(langCode), langText);
                    }
                }
            }
        }
        
        #endregion

        #region Types

        #endregion

        #region Fields

        /// <summary>   The multi languages. </summary>
        private Dictionary<CultureInfo, string> _multiLanguages;

        #endregion

        #region Properties

        /// <summary>   Gets or sets the multi languages. </summary>
        ///
        /// <value> The multi languages. </value>

        public Dictionary<CultureInfo, string> MultiLanguages
        {
            get { return _multiLanguages; }
            set { _multiLanguages = value; RaisePropertyChanged(); }
        }

        #endregion

        #region Methods

        /// <summary>   Sets multi language. </summary>
        ///
        /// <param name="attribute">    The attribute. </param>

        public void SetMultiLanguage(object attribute)
        {
            if (attribute.GetType() != typeof(MultilingualText)) return;
           
            ((MultilingualText)attribute).Items.ToList().ForEach(y =>
            {
                if (MultiLanguages.ContainsKey(y.Language.Culture))
                {
                    string langText = MultiLanguages.ToList().Find(x => x.Key.Equals(y.Language.Culture)).Value;
                    y.Text = langText;
                    Log.InfoFormat("Setting Language: {0} with Text: {1}", y.Language.Culture.Name, langText);
                }
            });
        }
        
        #endregion

        #region Override metods

        /// <summary>   Gibt eine Zeichenfolge zurück, die das aktuelle Objekt darstellt. </summary>
        ///
        /// <returns>   Eine Zeichenfolge, die das aktuelle Objekt darstellt. </returns>

        public override string ToString()
        {
            string back = "";
            this.MultiLanguages.ToList()
                .ForEach(x => back += x.Key.Name + ": " + x.Value + Environment.NewLine);
            return back.TrimEnd('\r', '\n');
        }

        /// <summary>
        ///     Führt anwendungsspezifische Aufgaben durch, die mit der Freigabe, der Zurückgabe oder dem
        ///     Zurücksetzen von nicht verwalteten Ressourcen zusammenhängen.
        /// </summary>

        public void Dispose()
        {
        }
        
        #endregion
    }
}