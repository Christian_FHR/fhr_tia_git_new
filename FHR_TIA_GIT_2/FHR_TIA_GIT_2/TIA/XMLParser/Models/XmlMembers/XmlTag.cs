﻿using System.Linq;
using System.Runtime.CompilerServices;
using System.Xml;
using JetBrains.Annotations;

namespace FHR_TIA_GIT_2.TIA.XMLParser.Models.XmlMembers
{
    public class XmlTag : XmlMember
    {
        #region Constructors

        public XmlTag()
        {
        }

        public XmlTag(string name, string dataType, string defaultValue = "") : base(name, dataType, defaultValue)
        {
        }

        public XmlTag(XmlNode node) : base(node)
        {
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        private string _address;

        #endregion

        #region Properties

        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }

        #endregion

        #region Methods

        public static XmlTag GetXmlTag([NotNull]XmlNode member, [CanBeNull]XmlNamespaceManager ns)
        {
            XmlTag tag = null;
            member.ChildNodes?.Cast<XmlNode>().ToList().ForEach(child =>
            {
                tag = new XmlTag();
                switch (child.Name)
                {
                    case "AttributeList":
                        child.ChildNodes.Cast<XmlNode>().ToList().ForEach(attribute =>
                        {
                            switch (attribute.Name)
                            {
                                case "Name":
                                    tag.Name = attribute.InnerText;
                                    break;
                                case "LogicalAddress":
                                    tag.Address = attribute.InnerText;
                                    break;
                            }
                        });
                        break;
                    case "LinkList":
                        child.ChildNodes.Cast<XmlNode>().ToList().ForEach(link => tag.DataType = link.InnerText);
                        break;
                    case "ObjectList":
                    {
                        var comment = child.SelectSingleNode("MultilingualText");
                        if (comment != null)
                        {
                            tag.Comment.CompositionNameInXml = comment.Attributes?["CompositionName"].Value;
                            tag.Comment.GetComment(comment);
                        }
                        break;
                    }
                }
            });

            return tag;
        }
        
        #endregion

        #region Override metods

        #endregion
    }
}