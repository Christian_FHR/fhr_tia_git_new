﻿// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT_2\TIA\XMLParser\Models\XmlMembers\XmlStruct.cs </file>
///
/// <copyright file="XmlStruct.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the XML structure class. </summary>

using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace FHR_TIA_GIT_2.TIA.XMLParser.Models.XmlMembers
{
    /// <summary>   An XML structure. </summary>
    public class XmlStruct : XmlMember
    {
        #region Constructors

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="name">         The name. </param>
        /// <param name="dataType">     (Optional) Type of the data. </param>
        /// <param name="defaultValue"> (Optional) The default value. </param>

        public XmlStruct(string name, string dataType = "Struct", string defaultValue = "") : base(name, dataType, defaultValue)
        {
            Members = new List<XmlMember>();
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="node"> The node. </param>
        /// <param name="ns">The namespacemanager</param>

        public XmlStruct(XmlNode node, XmlNamespaceManager ns) : base(node)
        {
            Members = new List<XmlMember>();
            node.ChildNodes?.Cast<XmlNode>().ToList().ForEach(x =>
            {
                var xmlMember = XmlMember.GetXmlMember(x, ns);
                if(xmlMember != null) this.Members.Add(xmlMember);
            });
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        #endregion

        #region Properties

        /// <summary>   Gets or sets the members. </summary>
        ///
        /// <value> The members. </value>

        public List<XmlMember> Members { get; set; }
        
        #endregion

        #region Methods

        #endregion

        #region Override metods

        #endregion
    }
}