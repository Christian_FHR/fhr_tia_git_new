﻿using System.Linq;
using System.Xml;
using JetBrains.Annotations;

namespace FHR_TIA_GIT_2.TIA.XMLParser.Models.XmlMembers
{
    public class XmlConstantTag : XmlMember
    {
        #region Constructors

        public XmlConstantTag()
        {
        }

        public XmlConstantTag(string name, string dataType, string defaultValue = "") : base(name, dataType, defaultValue)
        {
        }

        public XmlConstantTag(XmlNode node) : base(node)
        {
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        private string _constantValue;

        #endregion

        #region Properties

        public string ConstantValue
        {
            get { return _constantValue; }
            set { _constantValue = value; }
        }

        #endregion

        #region Methods

        public static XmlConstantTag GetXmlConstantTag([NotNull]XmlNode member, [CanBeNull]XmlNamespaceManager ns)
        {
            XmlConstantTag tag = null;
            member.ChildNodes?.Cast<XmlNode>().ToList().ForEach(child =>
            {
                tag = new XmlConstantTag();
                switch (child.Name)
                {
                    case "AttributeList":
                        child.ChildNodes.Cast<XmlNode>().ToList().ForEach(attribute =>
                        {
                            switch (attribute.Name)
                            {
                                case "Name":
                                    tag.Name = attribute.InnerText;
                                    break;
                                case "LogicalAddress":
                                    tag.ConstantValue = attribute.InnerText;
                                    break;
                            }
                        });
                        break;
                    case "LinkList":
                        child.ChildNodes.Cast<XmlNode>().ToList().ForEach(link => tag.DataType = link.InnerText);
                        break;
                    case "ObjectList":
                    {
                        var comment = child.SelectSingleNode("MultilingualText");
                        if (comment != null)
                        {
                            tag.Comment.CompositionNameInXml = comment.Attributes?["CompositionName"].Value;
                            tag.Comment.GetComment(comment);
                        }
                        break;
                    }
                }
            });

            return tag;
        }
        
        #endregion

        #region Override metods

        #endregion
    }
}