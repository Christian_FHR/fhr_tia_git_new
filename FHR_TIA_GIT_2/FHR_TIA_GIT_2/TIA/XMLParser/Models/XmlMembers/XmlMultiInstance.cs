﻿// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT_2\TIA\XMLParser\Models\XmlMembers\XmlMultiInstance.cs </file>
///
/// <copyright file="XmlMultiInstance.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the XML multi instance class. </summary>

using System.Collections.Generic;
using System.Xml;
using FHR_TIA_GIT_2.TIA.XMLParser.Models.Block;

namespace FHR_TIA_GIT_2.TIA.XMLParser.Models.XmlMembers
{
    /// <summary>   An XML multi instance. </summary>
    public class XmlMultiInstance : XmlMember
    {
        #region Constructors

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="name">         The name. </param>
        /// <param name="dataType">     Type of the data. </param>
        /// <param name="defaultValue"> (Optional) The default value. </param>

        public XmlMultiInstance(string name, string dataType, string defaultValue = "") : base(name, dataType, defaultValue)
        {
            InterfaceSections = new List<BlockInterfaceSection>();
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="node"> The node. </param>

        public XmlMultiInstance(XmlNode node) : base(node)
        {
            InterfaceSections = new List<BlockInterfaceSection>();
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        #endregion

        #region Properties

        /// <summary>   Gets or sets the interface sections. </summary>
        ///
        /// <value> The interface sections. </value>

        public List<BlockInterfaceSection> InterfaceSections { get; set; }
        
        #endregion

        #region Methods

        #endregion

        #region Override metods

        #endregion

        
    }
}