﻿// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT_2\TIA\XMLParser\Models\XmlMembers\XmlMember.cs </file>
///
/// <copyright file="XmlMember.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the XML member class. </summary>

using System;
using System.Globalization;
using System.Linq;
using System.Xml;
using FHR_TIA_GIT_2.TIA.TiaObjects;
using FHR_TIA_GIT_2.TIA.XMLParser.Models.Block;
using FHR_TIA_GIT_2.TIA.XMLParser.Models.Type;
using JetBrains.Annotations;

namespace FHR_TIA_GIT_2.TIA.XMLParser.Models.XmlMembers
{
    /// <summary>   An XML member. </summary>
    public class XmlMember
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public XmlMember()
        {
            Comment = new MultiLanguage();
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="name">         The name. </param>
        /// <param name="dataType">     The type of the data. </param>
        /// <param name="defaultValue"> (Optional) The default value. </param>

        public XmlMember(string name, string dataType, string defaultValue = "") : this()
        {
            Name = name;
            DataType = dataType;
            DefaultValue = defaultValue;
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="node"> The node. </param>

        public XmlMember(XmlNode node) : this()
        {
            Name = node.Attributes?["Name"].Value;
            DataType = node.Attributes?["Datatype"].Value;
            DefaultValue = "";
        }

        #endregion

        #region Types
        
        #endregion

        #region Fields

        #endregion

        #region Properties

        /// <summary>   Gets or sets the name. </summary>
        ///
        /// <value> The name. </value>

        public string Name { get; set; }

        /// <summary>   Gets or sets the type of the data. </summary>
        ///
        /// <value> The type of the data. </value>

        public string DataType { get; set; }

        /// <summary>   Gets or sets the default value. </summary>
        ///
        /// <value> The default value. </value>

        public string DefaultValue { get; set; }

        /// <summary>   Gets or sets the comment. </summary>
        ///
        /// <value> The comment. </value>

        public MultiLanguage Comment { get; set; }
        
        public bool ExternalAccessible { get; set; }
        public bool ExternalVisible { get; set; }
        public bool ExternalWritable { get; set; }
        
        #endregion

        #region Methods

        /// <summary>   Sets member multi instance. </summary>
        ///
        /// <param name="xmlMember">        The XML member. </param>
        /// <param name="softwareComplete"> The software complete. </param>

        public static void SetMemberMultiInstance(XmlMember xmlMember, TiaPlcSoftware softwareComplete)
        {
            switch (xmlMember)
            {
                case XmlStruct xmlStruct:
                    xmlStruct.Members?.ForEach(x => XmlMember.SetMemberMultiInstance(x, softwareComplete));
                    break;
                case XmlMultiInstance xmlMultiInstance:
                    var instance = softwareComplete.GetSimaticMlInstanceByName(xmlMultiInstance.DataType);
                    switch (instance)
                    {
                        case TypeInformation typeInformation:
                            // ReSharper disable once UseObjectOrCollectionInitializer
                            BlockInterfaceSection typeStruct = new BlockInterfaceSection("None");
                            typeStruct.InterfaceMembers = typeInformation.Members;
                            xmlMultiInstance.InterfaceSections.Add(typeStruct);
                            break;
                        case BlockInformation blockInformation:
                            xmlMultiInstance.InterfaceSections = blockInformation.BlockInterface.InterfaceSections;
                            break;
                    }
                    
                    break;
            }
        }

        /// <summary>   Gets XML member. </summary>
        ///
        /// <param name="member">   The member. </param>
        /// <param name="ns">The namespacemanager</param>
        ///
        /// <returns>   The XML member. </returns>

        public static XmlMember GetXmlMember(XmlNode member, XmlNamespaceManager ns)
        {
            if (!member.Name.Equals("Member")) return null;
            
            XmlMember xmlMember = null;
            if (member.Attributes != null && member.Attributes["Datatype"].Value.Equals("Struct"))
            {
                xmlMember = new XmlStruct(member, ns);
            }
            else if (member.Attributes != null && (member.LocalName.Equals("Member") && member.HasChildNodes &&
                                                   member.Attributes["Datatype"].Value.Contains("\"") &&
                                                   !member.Attributes["Datatype"].Value.Contains("Array of")))
            {
                foreach (XmlNode childNode in member.ChildNodes)
                {
                    if(!childNode.Name.Equals("Sections")) continue;
                    xmlMember = new XmlMultiInstance(member);
                }
            }
            else
            {
                xmlMember = new XmlMember(member);
            }

            if (xmlMember != null)
            {
                SetMemberStartValue(member, xmlMember, ns);
                SetMemberComment(member, xmlMember, ns);
                SetMemberAttributes(member, xmlMember, ns);
            }
            
            return xmlMember;
        }

        private static void SetMemberStartValue([NotNull] XmlNode member, [NotNull] XmlMember xmlMember, [NotNull] XmlNamespaceManager ns)
        {
            member.SelectNodes("SI:StartValue", ns)?.Cast<XmlNode>().ToList().ForEach(startValue => 
                xmlMember.DefaultValue = startValue != null ? startValue.InnerText : "");
        }
        
        private static void SetMemberComment([NotNull] XmlNode member, [NotNull] XmlMember xmlMember, [NotNull] XmlNamespaceManager ns)
        {
            member.SelectNodes("SI:Comment", ns)?.Cast<XmlNode>().ToList().
                ForEach(comment =>
                {
                    xmlMember.Comment.CompositionNameInXml = string.Empty;
                    comment.SelectNodes("SI:MultiLanguageText", ns)?.Cast<XmlNode>().ToList().
                        ForEach(mulitLangText =>
                        {
                            if(mulitLangText.Attributes is {Count: > 0})
                                xmlMember.Comment.TextItems.Add(mulitLangText.Attributes["Lang"].Value, mulitLangText.InnerText);
                        });
                });
        }

        private static void SetMemberAttributes([NotNull] XmlNode member, [NotNull] XmlMember xmlMember, [NotNull] XmlNamespaceManager ns)
        {
            member.SelectNodes("SI:AttributeList", ns)?.Cast<XmlNode>().ToList().ForEach(attribute =>
            {
                attribute.ChildNodes?.Cast<XmlNode>().ToList().ForEach(childNode =>
                {
                    if (childNode.Attributes != null && childNode.Attributes["Name"].Value == nameof(ExternalAccessible))
                        xmlMember.ExternalAccessible = Convert.ToBoolean(childNode.InnerText);
                    else if (childNode.Attributes != null && childNode.Attributes["Name"].Value == nameof(ExternalVisible))
                        xmlMember.ExternalVisible = Convert.ToBoolean(childNode.InnerText);
                    else if (childNode.Attributes != null && childNode.Attributes["Name"].Value == nameof(ExternalWritable))
                        xmlMember.ExternalWritable = Convert.ToBoolean(childNode.InnerText);
                });
            });
        }
        
        public string GetMemberCommentWhitActualCulture()
        {
            return this.Comment.TextItems.ContainsKey(CultureInfo.CurrentCulture.Name)
                ? this.Comment.TextItems[CultureInfo.CurrentCulture.Name]
                : "";
        }

        public string GetMemberCommentWithCulture(CultureInfo cultureInfo)
        {
            return this.Comment.TextItems.ContainsKey(cultureInfo.Name) ? this.Comment.TextItems[cultureInfo.Name] : "";
        }
        
        #endregion

        #region Override metods
        
        #endregion
    }
}