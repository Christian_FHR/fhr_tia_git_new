﻿// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT_2\TIA\XMLParser\Models\Block\BlockInterface.cs </file>
///
/// <copyright file="BlockInterface.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the block interface class. </summary>

using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace FHR_TIA_GIT_2.TIA.XMLParser.Models.Block
{
    /// <summary>   A block interface. </summary>
    public class BlockInterface
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public BlockInterface()
        {
            InterfaceSections = new List<BlockInterfaceSection>();
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        #endregion

        #region Properties

        /// <summary>   Gets or sets the interface sections. </summary>
        ///
        /// <value> The interface sections. </value>

        public List<BlockInterfaceSection> InterfaceSections { get; set; }
        
        #endregion

        #region Methods

        /// <summary>   Adds an interface section. </summary>
        /// 
        /// <param name="section">  The section. </param>
        /// <param name="ns">The namespacemanager</param>
        
        internal void AddInterfaceSection(XmlNode section, XmlNamespaceManager ns)
        {
            var blockInterfaceSection = new BlockInterfaceSection(section.Attributes?["Name"].Value);
            section.ChildNodes.Cast<XmlNode>().ToList().ForEach(x => blockInterfaceSection.AddInterfaceMember(x, ns));
            
            InterfaceSections?.Add(blockInterfaceSection);
        }
        
        #endregion

        #region Override metods

        #endregion
    }
}