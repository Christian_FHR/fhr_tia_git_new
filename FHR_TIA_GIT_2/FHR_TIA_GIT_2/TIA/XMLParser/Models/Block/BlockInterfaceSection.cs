﻿// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT_2\TIA\XMLParser\Models\Block\BlockInterfaceSection.cs </file>
///
/// <copyright file="BlockInterfaceSection.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the block interface section class. </summary>

using System.Collections.Generic;
using System.Xml;
using FHR_TIA_GIT_2.TIA.XMLParser.Models.XmlMembers;

namespace FHR_TIA_GIT_2.TIA.XMLParser.Models.Block
{
    /// <summary>   A block interface section. </summary>
    public class BlockInterfaceSection
    {
        #region Constructors

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="interfaceSectionName"> Name of the interface section. </param>

        public BlockInterfaceSection(string interfaceSectionName)
        {
            InterfaceMembers = new List<XmlMember>();
            InterfaceSectionName = interfaceSectionName;
        }
        
        #endregion

        #region Types

        #endregion

        #region Fields

        #endregion

        #region Properties

        /// <summary>   Gets or sets the interface members. </summary>
        ///
        /// <value> The interface members. </value>

        public List<XmlMember> InterfaceMembers { get; set; }

        /// <summary>   Gets or sets the name of the interface section. </summary>
        ///
        /// <value> The name of the interface section. </value>

        public string InterfaceSectionName { get; set; }
        
        #endregion

        #region Methods

        /// <summary>   Adds an interface member. </summary>
        ///
        /// <param name="member">   The member. </param>
        /// /// <param name="ns">The namespacemanager</param>

        public void AddInterfaceMember(XmlNode member, XmlNamespaceManager ns)
        {
            var xmlMember = XmlMember.GetXmlMember(member, ns);
            if(xmlMember != null) this.InterfaceMembers.Add(xmlMember);
        }
        
        #endregion

        #region Override metods

        #endregion
    }
}