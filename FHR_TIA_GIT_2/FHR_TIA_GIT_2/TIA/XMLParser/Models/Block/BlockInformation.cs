﻿// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT_2\TIA\XMLParser\Models\Block\BlockInformation.cs </file>
///
/// <copyright file="BlockInformation.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the block information class. </summary>

using System.Xml;
using FHR_TIA_GIT_2.TIA.TiaObjects;
using FHR_TIA_GIT_2.TIA.XMLParser.Models.XmlMembers;

namespace FHR_TIA_GIT_2.TIA.XMLParser.Models.Block
{
    /// <summary>   Information about the block. </summary>
    public class BlockInformation : XmlInformation
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public BlockInformation()
        {
            TiaXmlType = TiaXmlType.Block;
            BlockInterface = new BlockInterface();
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        #endregion

        #region Properties

        /// <summary>   Gets or sets the block interface. </summary>
        ///
        /// <value> The block interface. </value>

        public BlockInterface BlockInterface { get; set; }
        
        #endregion

        #region Methods

        #endregion

        #region Override metods

        /// <summary>   Gets an interface. </summary>
        ///
        /// <param name="rootNode">         The root node. </param>
        /// <param name="namespaceManager"> Manager for namespace. </param>

        public override void GetInterface(XmlNode rootNode, XmlNamespaceManager namespaceManager)
        {
            var listOfSections = rootNode.SelectNodes("//Interface/SI:Sections/SI:Section", namespaceManager);
            if (listOfSections == null) return;
            
            foreach (XmlNode section in listOfSections)
            {
                if (section.Attributes != null && section.Attributes["Name"].Value.Equals("Base")) continue;
                BlockInterface.AddInterfaceSection(section, namespaceManager);
            }
        }

        /// <summary>   Sort instances. </summary>
        ///
        /// <param name="softwareComplete"> The software complete. </param>

        public override void SortInstances(TiaPlcSoftware softwareComplete)
        {
            BlockInterface?.InterfaceSections?.ForEach(section => 
                section.InterfaceMembers?.ForEach(member => 
                    XmlMember.SetMemberMultiInstance(member, softwareComplete)));    
        }

        #endregion
    }
}