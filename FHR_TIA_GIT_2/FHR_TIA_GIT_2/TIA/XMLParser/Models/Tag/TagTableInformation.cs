﻿using System.Collections.Generic;
using System.Linq;
using System.Xml;
using FHR_TIA_GIT_2.TIA.TiaObjects;
using FHR_TIA_GIT_2.TIA.XMLParser.Models.XmlMembers;
using static FHR_TIA_GIT_2.TIA.XMLParser.Models.XPathConstants;

namespace FHR_TIA_GIT_2.TIA.XMLParser.Models.Tag
{
    public class TagTableInformation : XmlInformation
    {
        #region Constructors

        public TagTableInformation()
        {
            TiaXmlType = TiaXmlType.PlcTagTable;
            XmlTags = new List<XmlTag>();
            XmlConstantTags = new List<XmlConstantTag>();
        }
        
        #endregion

        #region Types

        #endregion

        #region Fields

        private List<XmlTag> _xmlTags;
        private List<XmlConstantTag> _xmlConstantTags;

        #endregion

        #region Properties

        public List<XmlTag> XmlTags
        {
            get { return _xmlTags; }
            set { _xmlTags = value; }
        }

        public List<XmlConstantTag> XmlConstantTags
        {
            get { return _xmlConstantTags; }
            set { _xmlConstantTags = value; }
        }

        #endregion

        #region Methods

        #endregion

        #region Override metods

        public override void GetInterface(XmlNode rootNode, XmlNamespaceManager namespaceManager)
        {
            this.Name = this.GetInformations(rootNode, XPATHNAME);

            rootNode.SelectNodes("//ObjectList/SW.Tags.PlcTag")?.Cast<XmlNode>().ToList().ForEach(tag =>
            {
                var xmlTag = XmlTag.GetXmlTag(tag, namespaceManager);
                if (xmlTag != null) this.XmlTags?.Add(xmlTag);
            });
            
            rootNode.SelectNodes("//ObjectList/SW.Tags.PlcConstant")?.Cast<XmlNode>().ToList().ForEach(constant =>
            {
                var xmlConstantTag = XmlConstantTag.GetXmlConstantTag(constant, namespaceManager);
                if (xmlConstantTag != null) this.XmlConstantTags?.Add(xmlConstantTag);
            });
        }

        public override void SortInstances(TiaPlcSoftware softwareComplete)
        {
            
        }

        #endregion
    }
}