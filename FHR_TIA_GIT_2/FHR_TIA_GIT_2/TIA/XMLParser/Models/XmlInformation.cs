﻿// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT_2\TIA\XMLParser\Models\XmlInformation.cs </file>
///
/// <copyright file="XmlInformation.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the XML information class. </summary>

using System;
using System.Xml;
using FHR_TIA_GIT_2.TIA.TiaObjects;

namespace FHR_TIA_GIT_2.TIA.XMLParser.Models
{
    /// <summary>   Information about the xml. </summary>
    public abstract class XmlInformation : IComparable
    {
        #region Constructors

        #endregion

        #region Types

        #endregion

        #region Fields

        /// <summary>   The name. </summary>
        private string _name;
        /// <summary>   Type of the tia XML. </summary>
        private TiaXmlType _tiaXmlType;

        #endregion

        #region Properties

        /// <summary>   Gets or sets the name. </summary>
        ///
        /// <value> The name. </value>

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        /// <summary>   Gets or sets the type of the tia XML. </summary>
        ///
        /// <value> The type of the tia XML. </value>

        public TiaXmlType TiaXmlType
        {
            get { return _tiaXmlType; }
            set { _tiaXmlType = value; }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Vergleicht die aktuelle Instanz mit einem anderen Objekt vom selben Typ und gibt eine
        ///     ganze Zahl zurück, die angibt, ob die aktuelle Instanz in der Sortierreihenfolge vor oder
        ///     nach dem anderen Objekt oder an derselben Position auftritt.
        /// </summary>
        ///
        /// <exception cref="ArgumentException">            Thrown when one or more arguments have
        ///                                                 unsupported or illegal values. </exception>
        /// <exception cref="T:System.ArgumentException">   . </exception>
        ///
        /// <param name="obj">  Ein Objekt, das mit dieser Instanz verglichen werden soll. </param>
        ///
        /// <returns>
        ///     Ein Wert, der die relative Reihenfolge der verglichenen Objekte angibt. Der Rückgabewert
        ///     hat folgende Bedeutung: Wert Bedeutung Kleiner als 0 (null)
        ///     Diese Instanz befindet sich in der Sortierreihenfolge vor <paramref name="obj" />.  
        ///     Zero Diese Instanz tritt in der Sortierreihenfolge an der gleichen Position wie
        ///     <paramref name="obj" /> auf.  
        ///     Größer als 0 (null)
        ///     Diese Instanz folgt in der Sortierreihenfolge auf <paramref name="obj" />.
        /// </returns>

        public int CompareTo(object obj)
        {
            if (obj is not XmlInformation xmlInfo) throw new ArgumentException("Object is not an XMLInformation");
            return TiaXmlType.CompareTo(xmlInfo.TiaXmlType);
        }

        public string GetInformations(XmlNode rootNode, string xPath)
        {
            var node = rootNode.SelectSingleNode(xPath);
            return node != null ? node.InnerText : "";
        }
        
        #endregion

        #region Override metods

        /// <summary>   Gets an interface. </summary>
        ///
        /// <param name="rootNode">         The root node. </param>
        /// <param name="namespaceManager"> Manager for namespace. </param>

        public abstract void GetInterface(XmlNode rootNode, XmlNamespaceManager namespaceManager);

        /// <summary>   Sort instances. </summary>
        ///
        /// <param name="softwareComplete"> The software complete. </param>

        public abstract void SortInstances(TiaPlcSoftware softwareComplete);

        #endregion

    }
}