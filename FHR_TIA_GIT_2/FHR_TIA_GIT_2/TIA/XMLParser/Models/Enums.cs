﻿// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT_2\TIA\XMLParser\Models\Enums.cs </file>
///
/// <copyright file="Enums.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the enums class. </summary>

namespace FHR_TIA_GIT_2.TIA.XMLParser.Models
{
    /// <summary>   Values that represent tia XML types. </summary>
    public enum TiaXmlType
    {
        /// <summary>
        /// The block
        /// </summary>
        Block,
        /// <summary>
        /// The data block
        /// </summary>
        DataBlock,
        /// <summary>
        /// The controller tag table
        /// </summary>
        PlcTagTable,
        /// <summary>
        /// The controller data type
        /// </summary>
        PlcType
    }
}