﻿// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT_2\TIA\XMLParser\Models\MultiLanguage.cs </file>
///
/// <copyright file="MultiLanguage.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the multi language class. </summary>

using System.Collections.Generic;
using System.Linq;
using System.Xml;
using FHR_TIA_GIT_2.ViewsAndModels.Models;

namespace FHR_TIA_GIT_2.TIA.XMLParser.Models
{
    /// <summary>   A multi language. </summary>
    public class MultiLanguage : ViewModelBase
    {
        /* ##### MultiLanguageText Interface Comment #####
        * <Comment>
               <MultiLanguageText Lang="en-US">TEXT</MultiLanguageText>
               <MultiLanguageText Lang="de-DE">TEXT</MultiLanguageText>
               <MultiLanguageText Lang="fr-FR">TEXT</MultiLanguageText>
        * </Comment>
        */

        /* ##### MultilingualText #####
        <MultilingualText ID="5" CompositionName="Title">
           <AttributeList>
               <TextItems>
                   <Value lang="en-US">TEXT</Value>
                   <Value lang="de-DE">TEXT</Value>
                   <Value lang="sv-SE">TEXT</Value>
                   <Value lang="fr-FR">TEXT</Value>
               </TextItems>
           </AttributeList>
         </MultilingualText>         
        */

        #region Constructors

        /// <summary>   Default constructor. </summary>
        public MultiLanguage()
        {
            TextItems = new Dictionary<string, string>();
        }

        #endregion

        #region Types

        #endregion
        
        #region Fields

        /// <summary>   The text items. </summary>
        private Dictionary<string, string> _textItems;
        /// <summary>   The composition name in XML. </summary>
        private string _compositionNameInXml;

        #endregion

        #region Properties

        /// <summary>   Gets or sets the text items. </summary>
        ///
        /// <value> The text items. </value>

        public Dictionary<string, string> TextItems
        {
            get { return _textItems; }
            set { _textItems = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets the composition name in XML. </summary>
        ///
        /// <value> The composition name in XML. </value>

        public string CompositionNameInXml
        {
            get { return _compositionNameInXml; }
            set { _compositionNameInXml = value; RaisePropertyChanged(); }
        }

        #endregion

        #region Methods

        public void GetComment(XmlNode xmlNode)
        {
            xmlNode.SelectNodes(".//Value")?.Cast<XmlNode>().ToList().ForEach(nodeValue =>
            {
                if(nodeValue.Attributes != null)
                    this.TextItems.Add(nodeValue.Attributes["Lang"].Value, nodeValue.InnerText);
            });
        }
        
        #endregion

        #region Override metods

        #endregion
    }
}