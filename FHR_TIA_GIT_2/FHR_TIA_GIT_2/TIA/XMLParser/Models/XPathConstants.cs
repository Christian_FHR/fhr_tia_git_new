﻿namespace FHR_TIA_GIT_2.TIA.XMLParser.Models
{
    public static class XPathConstants
    {

        #region Constants

        public const string XPATHNAME = "//Name";
        public const string XPATHNUMBER = "//Number";
        public const string XPATHTYPE = "//Type";
        public const string XPATHPROGRAMMINGLANGUAGE = "//ProgrammingLanguage";
        public const string XPATHMEMORYLAYOUT = "//MemoryLayout";
        public const string XPATHAUTHOR = "//HeaderAuthor";
        public const string XPATHTITLE = "//ObjectList/MultilingualText[@CompositionName='Title']";
        public const string XPATHCOMMENT = "//ObjectList/MultilingualText[@CompositionName='Comment']";
        public const string XPATHFAMILY = "//HeaderFamily";
        public const string XPATHVERSION = "//HeaderVersion";
        public const string XPATHHEADERNAME = "//HeaderName";
        
        public const string Xpathcodeblock = "//SW.CodeBlock";
        public const string Xpathdatablock = "//SW.DataBlock";
        public const string Xpathplcdatatype = "//SW.PlcType";
        public const string Xpathtagtable = "//SW.PlcTagTable";
        public const string Xpatharraydatatype = "//ArrayDatatype";
        public const string Xpathinstanceofname = "//InstanceOfName";
        public const string Xpathdatablocktype = "//DatablockType";
        public const string XpathAutonumber = "//AutoNumber";
        public const string XpathEnabletagreadback = "//EnableTagReadback";
        public const string XpathEnabletagreadbackblockproperties = "//EnableTagReadbackBlockProperties";
        public const string XpathIsieccheckenabled = "//IsIECCheckEnabled";
        public const string XpathDownloadwithoutreinit = "//DownloadWithoutReinit";
        public const string XpathIsonlystoredinloadmemory = "//IsOnlyStoredInLoadMemory";
        public const string XpathIsretainmemresenabled = "//IsRetainMemResEnabled";
        public const string XpathIswriteprotectedinas = "//IsWriteProtectedInAS";
        public const string XpathMemoryreserve = "//MemoryReserve";

        #endregion
        
        #region Methods

        #endregion
    }
}