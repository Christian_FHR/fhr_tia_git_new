﻿// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT_2\TIA\XMLParser\Models\Type\TypeInformation.cs </file>
///
/// <copyright file="TypeInformation.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the type information class. </summary>

using System.Collections.Generic;
using System.Linq;
using System.Xml;
using FHR_TIA_GIT_2.TIA.TiaObjects;
using FHR_TIA_GIT_2.TIA.XMLParser.Models.XmlMembers;

namespace FHR_TIA_GIT_2.TIA.XMLParser.Models.Type
{
    /// <summary>   Information about the type. </summary>
    public class TypeInformation : XmlInformation
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public TypeInformation()
        {
            TiaXmlType = TiaXmlType.PlcType;
            Members = new List<XmlMember>();
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        #endregion

        #region Properties

        /// <summary>   Gets or sets the members. </summary>
        ///
        /// <value> The members. </value>

        public List<XmlMember> Members { get; set; }
        
        #endregion

        #region Methods

        #endregion

        #region Override metods

        /// <summary>   Gets an interface. </summary>
        ///
        /// <param name="rootNode">         The root node. </param>
        /// <param name="namespaceManager"> Manager for namespace. </param>

        public override void GetInterface(XmlNode rootNode, XmlNamespaceManager namespaceManager)
        {
            rootNode.SelectNodes("//Interface/SI:Sections/SI:Section", namespaceManager)?.Cast<XmlNode>().ToList().ForEach(section => 
                section.ChildNodes.Cast<XmlNode>().ToList().ForEach(sectionMember =>
                    {
                        var xmlMember = XmlMember.GetXmlMember(sectionMember, namespaceManager);
                        if(xmlMember != null) this.Members.Add(xmlMember);
                    }));
        }

        /// <summary>   Sort instances. </summary>
        ///
        /// <param name="softwareComplete"> The software complete. </param>

        public override void SortInstances(TiaPlcSoftware softwareComplete)
        {
            Members.ForEach(x => XmlMember.SetMemberMultiInstance(x, softwareComplete));
        }

        #endregion
    }
}