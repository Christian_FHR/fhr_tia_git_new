﻿// ReSharper disable once InvalidXmlDocComment
/// <file>  FHR_TIA_GIT_2\TIA\XMLParser\SimaticMlParser.cs </file>
///
/// <copyright file="SimaticMlParser.cs" company="FHR Anlagenbau GmbH">
///     Copyright (c) 2022 FHR Anlagenbau GmbH. All rights reserved.
/// </copyright>
///
/// <summary>   Implements the simatic miles parser class. </summary>

using System.Xml;
using FHR_TIA_GIT_2.TIA.TiaObjects;
using FHR_TIA_GIT_2.TIA.XMLParser.Models;
using FHR_TIA_GIT_2.TIA.XMLParser.Models.Block;
using FHR_TIA_GIT_2.TIA.XMLParser.Models.Datablock;
using FHR_TIA_GIT_2.TIA.XMLParser.Models.Tag;
using FHR_TIA_GIT_2.TIA.XMLParser.Models.Type;
using FHR_TIA_GIT_2.ViewsAndModels.Models;

namespace FHR_TIA_GIT_2.TIA.XMLParser
{
    /// <summary>   A simatic miles parser. </summary>
    public class SimaticMlParser : ViewModelBase
    {
        #region Constructors

        /// <summary>   Default constructor. </summary>
        public SimaticMlParser()
        {
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="obj">  The object. </param>

        public SimaticMlParser(TiaObject obj)
        {
            XmlDocument = new XmlDocument();
            _ns = new XmlNamespaceManager(XmlDocument.NameTable);
            _ns.AddNamespace("SI", "http://www.siemens.com/automation/Openness/SW/Interface/v5");

            switch (obj)
            {
                case TiaBlock tiaBlock when !string.IsNullOrEmpty(tiaBlock.SimaticMlData):
                    XmlDocument.LoadXml(tiaBlock.SimaticMlData);
                    RootNode = XmlDocument?.DocumentElement;
                    XmlInformations = this.Parse();
                    break;
                case TiaType tiaType when !string.IsNullOrEmpty(tiaType.SimaticMlData):
                    XmlDocument.LoadXml(tiaType.SimaticMlData);
                    RootNode = XmlDocument?.DocumentElement;
                    XmlInformations = this.Parse();
                    break;
            }
        }

        #endregion

        #region Types

        #endregion

        #region Fields

        /// <summary>   The ns. </summary>
        private readonly XmlNamespaceManager _ns;
        /// <summary>   The XML document. </summary>
        private XmlDocument _xmlDocument;
        /// <summary>   The XML informations. </summary>
        private XmlInformation _xmlInformations;

        /// <summary>   The root node. </summary>
        private XmlNode _rootNode;
        /// <summary>   The node. </summary>
        private XmlNode _node;
        
        #endregion

        #region Properties

        /// <summary>   Gets or sets the XML document. </summary>
        ///
        /// <value> The XML document. </value>

        public XmlDocument XmlDocument
        {
            get { return _xmlDocument; }
            set { _xmlDocument = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets the XML informations. </summary>
        ///
        /// <value> The XML informations. </value>

        public XmlInformation XmlInformations
        {
            get { return _xmlInformations; }
            set { _xmlInformations = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets the root node. </summary>
        ///
        /// <value> The root node. </value>

        public XmlNode RootNode
        {
            get { return _rootNode; }
            set { _rootNode = value; RaisePropertyChanged(); }
        }

        /// <summary>   Gets or sets the node. </summary>
        ///
        /// <value> The node. </value>

        public XmlNode Node
        {
            get { return _node; }
            set { _node = value; RaisePropertyChanged(); }
        }

        #endregion

        #region Methods

        /// <summary>   Gets the parse. </summary>
        ///
        /// <returns>   An XmlInformation. </returns>

        public XmlInformation Parse()
        {
            XmlInformation xmlInfo = null;
            var xmlTypeNode = RootNode.SelectSingleNode(@"//*[contains(name(),'SW.')]");

            switch (xmlTypeNode?.Name)
            {
                case "SW.Blocks.FB":
                case "SW.Blocks.FC":
                case "SW.Blocks.OB":
                    xmlInfo = ParseSoftwareBlock();
                    break;

                case "SW.Blocks.DB":
                    xmlInfo = ParseDataBlock();
                    break;

                case "SW.PlcType":
                case "SW.Types.PlcType":
                case "SW.Types.PlcStruct":
                    xmlInfo = ParsePlcType();
                    break;

                case "SW.Tags.PlcTagTable":
                    xmlInfo = ParsePlcTagTable();
                    break;
            }

            return xmlInfo;
        }

        /// <summary>   Parse software block. </summary>
        ///
        /// <returns>   An XmlInformation. </returns>

        private XmlInformation ParseSoftwareBlock()
        {
            var blockInfo = new BlockInformation();
            blockInfo.GetInterface(RootNode, _ns);
            return blockInfo;
        }

        private XmlInformation ParseDataBlock()
        {
            var dataInfo = new DataBlockInformation();
            dataInfo.GetInterface(RootNode, _ns);
            return dataInfo;
        }
        
        /// <summary>   Parse PLC type. </summary>
        ///
        /// <returns>   An XmlInformation. </returns>

        private XmlInformation ParsePlcType()
        {
            var typeInfo = new TypeInformation();
            typeInfo.GetInterface(RootNode, _ns);
            return typeInfo;
        }
        
        private XmlInformation ParsePlcTagTable()
        {
            var tagTableInfo = new TagTableInformation();
            tagTableInfo.GetInterface(RootNode, _ns);
            return tagTableInfo;
        }
        
        #endregion

        #region Override metods

        #endregion

    }
}